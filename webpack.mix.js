let mix = require('laravel-mix');
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(`${path.sep}`); // this changes the /public path to / (important to outputs bellow)

// mix.js('Modules/Admin/Resources/assets/js/app.js', 'Modules/Admin/Assets/app/vue');
// mix.js('Modules/Admin/Resources/assets/js/app.js', 'public/modules/core/app/vue');