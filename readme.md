# Dashboard Semeia v1.0

## Installation

This project uses Laravel Framework.

Once you clone the branch **master** to the hosting system and create a **database**, you will need to make sure your 
server meets the following requirements:

- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

All the following commands must to be executed on a shell terminal. Many hosting providers allows access to their hosting 
accounts using ssh.

If everything is ok, the first step is to install all vendor packages by running _composer_ command:

    php composer.phar install

Wait all packages to be installed and then, copy **.env.example** as **.env**:

    cp -f -R .env.example .env

Now, with an text editor, open and edit the **.env** file and configure your database connection and all the others settings.

Also, make sure the directory **storage/logs** have write permissions:

    chmod -R 777 storage/logs

Create a symbolic link from **public/storage** to **storage/app/public**:

    php artisan storage:link

Now, you're able to publish all the modules, run migrations and seed the database:

    php artisan migrate
    php artisan module:migrate-refresh --seed
