<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => [ 'web', 'auth', 'roles' ], 'as' => 'api.',
              'prefix' => 'api', 'namespace' => 'Modules\Api\Http\Controllers'], function()
{
	// -- Files
	Route::resource('/v1/files', 'FileController');

    // -- contributions
    Route::resource('/v1/contributions', 'ContributionController');

    // -- withdrawals
    Route::resource('/v1/withdrawals', 'WithdrawalController');

    // -- RECIPES
    Route::resource('/v1/recipes', 'RecipeController');

    // -- EXPENSES
    Route::resource('/v1/expenses', 'ExpenseController');

    // -- TAXATIONS
    Route::resource('/v1/taxations', 'TaxationController');

    // -- PROFITABILITY
    Route::resource('/v1/profitability', 'ProfitabilityController');

});