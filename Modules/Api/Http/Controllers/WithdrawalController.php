<?php

namespace Modules\Api\Http\Controllers;

use DBHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Core\Events\TransactionCreated;
use Modules\Core\Entities\Transaction;
use Modules\Core\Entities\Withdrawal;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Notifications\WithdrawalApproved;


class WithdrawalController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$dd = 1;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
        try
        {
            if(! $request->input('id_request'))
                throw new \Exception('Erro: Desculpe, parâmetros inválidos', 1);

            $validation = \Validator::make($request->all(),[
                'file_upload' => 'mimes:jpeg,jpg,png,gif|max:10000',
            ]);

            if($validation->fails()){
                throw new \Exception('Algo de errado ocorreu ao fazer o upload da imagem', 1);
            } else{

                return RequestHelper::doApiRequest(function() use($request)
                {

                    \DB::transaction(function() use($request)
                    {

                        $inputs = [
                            'status' => $request->input('approve') === 'true' ?
                                Withdrawal::WITHDRAWAL_APPROVED:Withdrawal::WITHDRAWAL_SCHEDULED,

                            'id' => $request->input('id_request'),
                        ];

                        if($request->hasFile('file_upload')){

                            $date = (\Carbon\Carbon::now())->toDateString();
                            $_id_name = kebab_case("receipt-{$date}-" . uniqid() );

                            $_extension = $request->file('file_upload')->extension();
                            $file_name =  "{$_id_name}.{$_extension}";

                            $_path = Withdrawal::find($request->input('id_request'))
                                    ->user->path . DIRECTORY_SEPARATOR . 'withdrawals_receipt';

                            $request->file('file_upload')->storeAs($_path,$file_name);

                            $inputs = array_merge(['file_name' => $file_name],$inputs);

                        }

                        /**
                         * @var Withdrawal $withdrawal
                         */
                        $withdrawal = DBHelper::update( Withdrawal::class, $inputs );

                        if($withdrawal->getOriginal('status') === Withdrawal::WITHDRAWAL_APPROVED &&
                            $withdrawal->transaction == null)
                        {
                            /**
                             * @var Transaction $transaction
                             */
                            $transaction = $withdrawal
                                ->transaction()->create(['value' => $withdrawal->getOriginal('value')]);

                            event(new TransactionCreated($transaction, $withdrawal->user));
                            $withdrawal->user->notify(new WithdrawalApproved($transaction));
                        }

                    });

                });

            }

        }
        catch (\Exception $ex)
        {
            $msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

            return response()->json([
                'status' => 'error',
                'data' => $msg,
            ], 500);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try
		{
			if(!$id)
				throw new \Exception('Erro: Desculpe, parâmetros inválidos', 1);

			/** @var $data Withdrawal */
			$data = Withdrawal::with(
			    ['user','user.account','user.capital','user.bank_account','user.bank_account.bank'])
                ->whereId($id)->get();

			if($data->isEmpty())
				throw new \Exception('Erro: Recurso não encontrado', 1);

            $data_response = $data->first()->toArray();
            $data_response['value'] = $data->first()->getOriginal('value');

            $data_response['user'] = $data->first()->user->toArray();

            return response()->json([
                'status' => 'success',
                'data' => $data_response,
            ]);
		}
		catch (\Exception $ex)
		{
			$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

			return response()->json([
				'status' => 'error',
				'data' => $msg,
			], 500);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
//		$dd = 1;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

}
