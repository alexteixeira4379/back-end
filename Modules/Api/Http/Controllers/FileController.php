<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Core\Entities\File;


class FileController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$dd = 1;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try
		{
			if(!$id)
				throw new \Exception('Erro: Desculpe, parâmetros inválidos', 1);

			/** @var $data File */
			$data = File::whereId($id)->get();

			if($data->isEmpty())
				throw new \Exception('Erro: Recurso não encontrado', 1);

			return response()->json([
				'status' => 'success',
				'data' => $data->toArray(),
			]);
		}
		catch (\Exception $ex)
		{
			$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

			return response()->json([
				'status' => 'error',
				'data' => $msg,
			], 500);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

}
