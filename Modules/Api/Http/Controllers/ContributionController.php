<?php

namespace Modules\Api\Http\Controllers;

use DBHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Contribution;
use Modules\Core\Events\TransactionCreated;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Notifications\ContributionApproved;


class ContributionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$dd = 1;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try
		{
			if(!$id)
				throw new \Exception('Erro: Desculpe, parâmetros inválidos', 1);

			/** @var $data Contribution */
			$data = Contribution::with(['user','user.account'])->whereId($id)->get();

			if($data->isEmpty())
				throw new \Exception('Erro: Recurso não encontrado', 1);

			$data_response = $data->first()->toArray();
			$data_response['value'] = $data->first()->getOriginal('value');

            $data_response['user'] = $data->first()->user->toArray();

			return response()->json([
				'status' => 'success',
				'data' => $data_response,
			]);
		}
		catch (\Exception $ex)
		{
			$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

			return response()->json([
				'status' => 'error',
				'data' => $msg,
			], 500);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
	public function update(Request $request, $id)
	{
	    if(!$request->has('approve') || !$id)
            throw new \Exception('Erro: Desculpe, parâmetros inválidos', 1);

	    if(\Auth::getUser()->hasRole(\Role::PROFILE_USER))
            throw new \Exception('Chamada não permitida', 1);

	    $contribution_status = $request->input('approve') === 'true' ?
            Contribution::CONTRIBUTION_APPROVED:Contribution::CONTRIBUTION_SCHEDULED;


        return RequestHelper::doApiRequest(function() use($id, $contribution_status)
        {

            $contribution = DBHelper::update( Contribution::class, ['id' => $id, 'status' => $contribution_status] );

            if($contribution->getOriginal('status') === contribution::CONTRIBUTION_APPROVED &&
                $contribution->transaction == null)
            {
                $transaction = $contribution
                    ->transaction()->create(['value' => $contribution->getOriginal('value')]);

                event(new TransactionCreated($transaction, $contribution->user));

                $contribution->user->notify(new ContributionApproved($transaction));
            }

        });

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

}
