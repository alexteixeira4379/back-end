<?php

namespace Modules\Api\Http\Controllers;

use Carbon\Carbon;
use DBHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Contribution;
use Modules\Core\Events\TransactionCreated;
use Modules\Core\Helpers\ProfitabilityHelper;
use Modules\Core\Helpers\RequestHelper;


class ProfitabilityController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$dd = 1;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $year_filter
	 * @return \Illuminate\Http\Response
	 */
	public function show($year_filter = null)
	{
		try
		{

		    if($year_filter){
		        $date = Carbon::createFromDate($year_filter);
                $data_response = ProfitabilityHelper::getProfitabilityOnScale($date);
            }
		    else
                $data_response = ProfitabilityHelper::getProfitabilityOnScale();

			return response()->json([
				'status' => 'success',
				'data' => $data_response->toArray(),
			]);
		}
		catch (\Exception $ex)
		{
			$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

			return response()->json([
				'status' => 'error',
				'data' => $msg,
			], 500);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * @param  int  $id
     * Update the specified resource in storage.
     *
     */
	public function update($id)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

}
