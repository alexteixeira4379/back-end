<?php

namespace Modules\Core\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class NewProfitability extends Notification
{
    use Queueable;

    public $profitability;
    /**
     * Create a new notification instance.
     *
     * @param $profitability
     */
    public function __construct($profitability)
    {
        $this->profitability = $profitability;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->profitability;
    }
}
