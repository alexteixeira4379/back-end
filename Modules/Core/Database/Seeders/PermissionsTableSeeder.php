<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

	    // -- as admin has all system permissions, then, create all of them
	    foreach (Permission::getAllPermissions() as $permission)
	    {
		    // -- permission
		    Permission::create([
			    'name'             => $permission,
		    ]);
	    }

	    // -- as admin has all api system permissions, then, create all of them
	    foreach (Permission::getAllPermissions(true) as $permission)
	    {
		    // -- permission
		    Permission::create([
			    'name'             => $permission,
		    ]);
	    }
    }
}
