<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $banks = [
            [ 'code' => '001', 'name' => 'Banco do Brasil S.A.', ],
            [ 'code' => '341', 'name' => 'Banco Itaú S.A.', ],
            [ 'code' => '033', 'name' => 'Banco Santander (Brasil) S.A.', ],
            [ 'code' => '356', 'name' => 'Banco Real S.A. (antigo)', ],
            [ 'code' => '652', 'name' => 'Itaú Unibanco Holding S.A.', ],
            [ 'code' => '237', 'name' => 'Banco Bradesco S.A.', ],
            [ 'code' => '745', 'name' => 'Banco Citibank S.A.', ],
            [ 'code' => '399', 'name' => 'HSBC Bank Brasil S.A. – Banco Múltiplo', ],
            [ 'code' => '104', 'name' => 'Caixa Econômica Federal', ],
            [ 'code' => '389', 'name' => 'Banco Mercantil do Brasil S.A.', ],
            [ 'code' => '453', 'name' => 'Banco Rural S.A.', ],
            [ 'code' => '422', 'name' => 'Banco Safra S.A.', ],
            [ 'code' => '633', 'name' => 'Banco Rendimento S.A.', ],
            [ 'code' => '246', 'name' => 'Banco ABC Brasil S.A.', ],
            [ 'code' => '025', 'name' => 'Banco Alfa S.A.', ],
            [ 'code' => '641', 'name' => 'Banco Alvorada S.A.', ],
            [ 'code' => '029', 'name' => 'Banco Banerj S.A.', ],
            [ 'code' => '038', 'name' => 'Banco Banestado S.A.', ],
            [ 'code' => '000', 'name' => 'Banco Bankpar S.A.', ],
            [ 'code' => '740', 'name' => 'Banco Barclays S.A.', ],
            [ 'code' => '107', 'name' => 'Banco BBM S.A.', ],
            [ 'code' => '031', 'name' => 'Banco Beg S.A.', ],
            [ 'code' => '096', 'name' => 'Banco BM&F de Serviços de Liquidação e Custódia S.A', ],
            [ 'code' => '318', 'name' => 'Banco BMG S.A.', ],
            [ 'code' => '752', 'name' => 'Banco BNP Paribas Brasil S.A.', ],
            [ 'code' => '248', 'name' => 'Banco Boavista Interatlântico S.A.', ],
            [ 'code' => '036', 'name' => 'Banco Bradesco BBI S.A.', ],
            [ 'code' => '204', 'name' => 'Banco Bradesco Cartões S.A.', ],
            [ 'code' => '225', 'name' => 'Banco Brascan S.A.', ],
            [ 'code' => '044', 'name' => 'Banco BVA S.A.', ],
            [ 'code' => '263', 'name' => 'Banco Cacique S.A.', ],
            [ 'code' => '473', 'name' => 'Banco Caixa Geral – Brasil S.A.', ],
            [ 'code' => '222', 'name' => 'Banco Calyon Brasil S.A.', ],
            [ 'code' => '040', 'name' => 'Banco Cargill S.A.', ],
            [ 'code' => 'M08', 'name' => 'Banco Citicard S.A.', ],
            [ 'code' => 'M19', 'name' => 'Banco CNH Capital S.A.', ],
            [ 'code' => '215', 'name' => 'Banco Comercial e de Investimento Sudameris S.A.', ],
            [ 'code' => '756', 'name' => 'Banco Cooperativo do Brasil S.A. – BANCOOB', ],
            [ 'code' => '748', 'name' => 'Banco Cooperativo Sicredi S.A.', ],
            [ 'code' => '505', 'name' => 'Banco Credit Suisse (Brasil) S.A.', ],
            [ 'code' => '229', 'name' => 'Banco Cruzeiro do Sul S.A.', ],
            [ 'code' => '003', 'name' => 'Banco da Amazônia S.A. ', ],
            [ 'code' => '083-3', 'name' => 'Banco da China Brasil S.A.', ],
            [ 'code' => '707', 'name' => 'Banco Daycoval S.A.', ],
            [ 'code' => 'M06', 'name' => 'Banco de Lage Landen Brasil S.A.', ],
            [ 'code' => '024', 'name' => 'Banco de Pernambuco S.A. – BANDEPE', ],
            [ 'code' => '456', 'name' => 'Banco de Tokyo-Mitsubishi UFJ Brasil S.A.', ],
            [ 'code' => '214', 'name' => 'Banco Dibens S.A.', ],
            [ 'code' => '047', 'name' => 'Banco do Estado de Sergipe S.A.', ],
            [ 'code' => '037', 'name' => 'Banco do Estado do Pará S.A.', ],
            [ 'code' => '041', 'name' => 'Banco do Estado do Rio Grande do Sul S.A.', ],
            [ 'code' => '004', 'name' => 'Banco do Nordeste do Brasil S.A.', ],
            [ 'code' => '265', 'name' => 'Banco Fator S.A.', ],
            [ 'code' => 'M03', 'name' => 'Banco Fiat S.A.', ],
            [ 'code' => '224', 'name' => 'Banco Fibra S.A.', ],
            [ 'code' => '626', 'name' => 'Banco Ficsa S.A.', ],
            [ 'code' => '394', 'name' => 'Banco Finasa BMC S.A.', ],
            [ 'code' => 'M18', 'name' => 'Banco Ford S.A.', ],
            [ 'code' => '233', 'name' => 'Banco GE Capital S.A.', ],
            [ 'code' => '734', 'name' => 'Banco Gerdau S.A.', ],
            [ 'code' => 'M07', 'name' => 'Banco GMAC S.A.', ],
            [ 'code' => '612', 'name' => 'Banco Guanabara S.A.', ],
            [ 'code' => 'M22', 'name' => 'Banco Honda S.A.', ],
            [ 'code' => '063', 'name' => 'Banco Ibi S.A. Banco Múltiplo', ],
            [ 'code' => 'M11', 'name' => 'Banco IBM S.A.', ],
            [ 'code' => '604', 'name' => 'Banco Industrial do Brasil S.A.', ],
            [ 'code' => '320', 'name' => 'Banco Industrial e Comercial S.A.', ],
            [ 'code' => '653', 'name' => 'Banco Indusval S.A.', ],
            [ 'code' => '630', 'name' => 'Banco Intercap S.A.', ],
            [ 'code' => '249', 'name' => 'Banco Investcred Unibanco S.A.', ],
            [ 'code' => '184', 'name' => 'Banco Itaú BBA S.A.', ],
            [ 'code' => '479', 'name' => 'Banco ItaúBank S.A', ],
            [ 'code' => 'M09', 'name' => 'Banco Itaucred Financiamentos S.A.', ],
            [ 'code' => '376', 'name' => 'Banco J. P. Morgan S.A.', ],
            [ 'code' => '074', 'name' => 'Banco J. Safra S.A.', ],
            [ 'code' => '217', 'name' => 'Banco John Deere S.A.', ],
            [ 'code' => '065', 'name' => 'Banco Lemon S.A.', ],
            [ 'code' => '600', 'name' => 'Banco Luso Brasileiro S.A.', ],
            [ 'code' => '755', 'name' => 'Banco Merrill Lynch de Investimentos S.A.', ],
            [ 'code' => '746', 'name' => 'Banco Modal S.A.', ],
            [ 'code' => '151', 'name' => 'Banco Nossa Caixa S.A.', ],
            [ 'code' => '045', 'name' => 'Banco Opportunity S.A.', ],
            [ 'code' => '623', 'name' => 'Banco Panamericano S.A.', ],
            [ 'code' => '611', 'name' => 'Banco Paulista S.A.', ],
            [ 'code' => '643', 'name' => 'Banco Pine S.A.', ],
            [ 'code' => '638', 'name' => 'Banco Prosper S.A.', ],
            [ 'code' => '747', 'name' => 'Banco Rabobank International Brasil S.A.', ],
            [ 'code' => 'M16', 'name' => 'Banco Rodobens S.A.', ],
            [ 'code' => '072', 'name' => 'Banco Rural Mais S.A.', ],
            [ 'code' => '250', 'name' => 'Banco Schahin S.A.', ],
            [ 'code' => '749', 'name' => 'Banco Simples S.A.', ],
            [ 'code' => '366', 'name' => 'Banco Société Générale Brasil S.A.', ],
            [ 'code' => '637', 'name' => 'Banco Sofisa S.A.', ],
            [ 'code' => '464', 'name' => 'Banco Sumitomo Mitsui Brasileiro S.A.', ],
            [ 'code' => '082-5', 'name' => 'Banco Topázio S.A.', ],
            [ 'code' => 'M20', 'name' => 'Banco Toyota do Brasil S.A.', ],
            [ 'code' => '634', 'name' => 'Banco Triângulo S.A.', ],
            [ 'code' => '208', 'name' => 'Banco UBS Pactual S.A.', ],
            [ 'code' => 'M14', 'name' => 'Banco Volkswagen S.A.', ],
            [ 'code' => '655', 'name' => 'Banco Votorantim S.A.', ],
            [ 'code' => '610', 'name' => 'Banco VR S.A.', ],
            [ 'code' => '370', 'name' => 'Banco WestLB do Brasil S.A.', ],
            [ 'code' => '021', 'name' => 'BANESTES S.A. Banco do Estado do Espírito Santo', ],
            [ 'code' => '719', 'name' => 'Banif-Banco Internacional do Funchal (Brasil)S.A.', ],
            [ 'code' => '073', 'name' => 'BB Banco Popular do Brasil S.A.', ],
            [ 'code' => '078', 'name' => 'BES Investimento do Brasil S.A.-Banco de Investimento', ],
            [ 'code' => '069', 'name' => 'BPN Brasil Banco Múltiplo S.A.', ],
            [ 'code' => '070', 'name' => 'BRB – Banco de Brasília S.A.', ],
            [ 'code' => '477', 'name' => 'Citibank N.A.', ],
            [ 'code' => '081-7', 'name' => 'Concórdia Banco S.A.', ],
            [ 'code' => '487', 'name' => 'Deutsche Bank S.A. – Banco Alemão', ],
            [ 'code' => '751', 'name' => 'Dresdner Bank Brasil S.A. – Banco Múltiplo', ],
            [ 'code' => '062', 'name' => 'Hipercard Banco Múltiplo S.A.', ],
            [ 'code' => '492', 'name' => 'ING Bank N.V.', ],
            [ 'code' => '488', 'name' => 'JPMorgan Chase Bank', ],
            [ 'code' => '409', 'name' => 'UNIBANCO – União de Bancos Brasileiros S.A.', ],
            [ 'code' => '230', 'name' => 'Unicard Banco Múltiplo S.A.', ],
        ];

        foreach ($banks as $key => $bank)
        {
            // -- create
            $bank = Bank::create([
                'name'          => $bank['name'],
                'code'          => $bank['code'],
            ]);
        }

    }
}
