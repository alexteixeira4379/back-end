<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Events\TransactionCreated;
use User;
use Contribution;
class ContributionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach ( User::all() as $user ){

            Contribution::create([
                'user_id' => $user->id,
                'value' => rand (1000 * 10, 10000 * 10) / 10,
                'date' => date("Y-m-d"),
                'status' => Contribution::CONTRIBUTION_PENDING
            ]);


            $value = rand (1000 * 10, 10000 * 10) / 10;
            $contribution = Contribution::create([
                'user_id' => $user->id,
                'value' => $value,
                'date' => date("Y-m-d"),
                'status' => Contribution::CONTRIBUTION_APPROVED
            ]);

            /** @var \Modules\Core\Entities\Contribution $contribution */
            /** @var \Modules\Core\Entities\Transaction $transaction */
           $transaction = $contribution->transaction()->create([
                'value' => $value
            ]);

            event(new TransactionCreated($transaction,$user));

            $value = rand (1000 * 10, 10000 * 10) / 10;
            $contribution = Contribution::create([
                'user_id' => $user->id,
                'value' => $value,
                'date' => date("Y-m-d"),
                'status' => Contribution::CONTRIBUTION_APPROVED
            ]);

            /** @var \Modules\Core\Entities\Contribution $contribution */
            /** @var \Modules\Core\Entities\Transaction $transaction */
            $transaction = $contribution->transaction()->create([
                'value' => $value
            ]);

            event(new TransactionCreated($transaction,$user));

        }
    }
}
