<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Setting;
use Modules\Core\Helpers\AppHelper;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

	    Setting::create([

		    // -- general settings
		    'site_title'             => 'Portal Invista em Consórcio',
		    'footer_text'            => 'Portal Invista em Consórcio /  Todos os direitos reservados.',

		    'company_name'           => 'Portal Invista em Consórcio',
		    'company_cnpj'           => '12312312312',
		    'min_profitability'      => '1.4',
		    'day_operation'          => '14',

		    'smtp_host'              => 'smtp.gmail.com',
		    'smtp_username'          => 'user@gmail.com',
		    'smtp_password'          => 'senha',
		    'smtp_port'              => '587',
		    'smtp_encryption_type'   => AppHelper::SMTP_ENCRYPTION_TYPE_TLS,

		    'primary_color'         => '#4c1430',
		    'secondary_color'       => '#482939',

	    ]);

    }
}
