<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use User;
use Modules\Core\Entities\Withdrawal;
class WithdrawalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach ( User::all() as $user ){

            Withdrawal::create([
                'user_id' => $user->id,
                'value' => 100.00,
                'date' => date("Y-m-d"),
                'status' => Withdrawal::WITHDRAWAL_PENDING
            ]);

        }
    }
}
