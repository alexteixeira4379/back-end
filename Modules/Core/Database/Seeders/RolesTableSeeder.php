<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\Role;
use Modules\Core\Entities\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (Role::$roles as $role_name => $role_data)
        {
	        // -- admin
	        $role = Role::create([
		        'id'               => $role_data['id'],
		        'name'             => $role_data['name'],
		        'display_name'     => $role_data['display_name'],
		        'description'      => $role_data['description'],
	        ]);


	        // -- reset permissions
	        $permissions = Permission::getPermissionsByRole($role->getOriginal('name') );
	        $permissions = array_merge($permissions, Permission::getPermissionsByRole($role->getOriginal('name'), true ));

	        $role->attachPermission([
		        'id' => Permission::whereIn('name', $permissions)->get()->pluck('id', 'id')->toArray()
	        ]);

        }


    }
}
