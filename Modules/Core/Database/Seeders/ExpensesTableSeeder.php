<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Expense;

class ExpensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Expense::create([

            'addressee' => 'Salários',
            'kind' => 'Vendedor/Parceira',
            'value' => 200.00,
            'date' => '01/01/2019'
        ]);

        Expense::create([

            'addressee' => 'FGTS',
            'kind' => 'Assistência Odontológica',
            'value' => 157.00,
            'date' => '01/01/2019'
        ]);

        Expense::create([

            'addressee' => 'Assistência Odontológica',
            'kind' => 'Vendedor/Parceira',
            'value' => 382.00,
            'date' => '01/01/2019'

        ]);
    }
}
