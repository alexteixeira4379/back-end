<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

	    $this->call(PermissionsTableSeeder::class);             // Permissions
	    $this->call(RolesTableSeeder::class);                   // Roles

	    $this->call(UsersTableSeeder::class);                   // Users
        $this->call(BanksTableSeeder::class);                   // Banks
        $this->call(BankAccountsTableSeeder::class);            // Bank Accounts


        $this->call(SettingsTableSeeder::class);                // Settings
	    $this->call(ContributionTableSeeder::class);            // Contribution
	    $this->call(CapitalTableSeeder::class);                 // Capital
	    $this->call(ClosedCapitalTableSeeder::class);           // Closed Capital
	    $this->call(WithdrawalTableSeeder::class);              // Withdrawal

        $this->call(RecipesTableSeeder::class);                 // Recipe
        $this->call(ExpensesTableSeeder::class);                // Expense
        $this->call(TaxationsTableSeeder::class);               // Taxation

        $this->call(ProfitabilityTableSeeder::class);           // Profitability


    }
}