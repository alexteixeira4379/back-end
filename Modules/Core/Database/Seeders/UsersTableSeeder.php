<?php

namespace Modules\Core\Database\Seeders;

use App;
use function foo\func;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\Role;
use Modules\Core\Entities\User;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 * @throws \Exception
	 */
    public function run()
    {
        try
        {
	        Model::unguard();


	        $admin_role     = Role::whereName( Role::PROFILE_ADMIN )->first();

	        // -- an admin
	        $admin = User::create([

		        // -- personal data
		        'email'             => 'admin@email.com',
		        'password'          => \Hash::make('mudar123'),
	        ]);

            $admin->account()->create(
                [
                    'full_name' => "Admin Smith",
                    'cpf'               => mt_rand ( 12345678911234 , 99999999999999 ),
                    'rg'                => mt_rand ( 123456789 , 999999999 ),
                    'birthday'          => ''.mt_rand(10, 27).'/11/19'.mt_rand(80, 90).'',
                    'marital_status'    => mt_rand(1,4),
                    'nationality'       => 'Brasileiro',
                    'person_type'       => '1',
                    'telephone_1'       => '(11) 9999-9999',
                    'telephone_2'       => '(11) 92222-9999',

                    // -- contact
                    'address'           => 'Rua dos Enderecos',
                    'number'            => '300',
                    'zip_code'          => '99999-222',
                    'neighborhood'      => 'Bairro xyz',
                    'city'              => 'São Paulo',
                    'state'             => 'SP',
                    'met_for'           => '2',
                    'indicated_by'      => 'Alex'

                ]
            );

	        // -- attach role
	        $admin->attachRole($admin_role); // parameter can be an Role object, array, or id

            // -- Other users
	        if(App::environment() !== 'production')
	        {

		        $createUser = function ($role_to_attach) {

                    $names = [1 => 'Alex',"Ronaldo","Alicia","Debora","Natalhia","Ketelijn","Matheus","Lucas","Vitor",'Mario',"Diego","Dembellé"];
                    $last_names = [1 => 'Silva',"Beckham","Smith","Johnson","Williams","Moore","Harris","Walker","Phillips",'Perreira',"Teixeira","Edwards"];

		            $type_person = mt_rand(1,2);

		            $first_name = $names[mt_rand(1,sizeof($names))];
                    $last_names = $last_names[mt_rand(1,sizeof($last_names))];


                    $data_persons = [

                        '1' => [
                            'full_name'         => $first_name . ' ' . $last_names,
                            'cpf'               => mt_rand ( 12345678911234 , 99999999999999 ),
                            'rg'                => mt_rand ( 123456789 , 999999999 ),
                            'birthday'          => ''.mt_rand(10, 27).'/11/19'.mt_rand(80, 90).'',
                            'marital_status'    => mt_rand(1,4),
                            'nationality'       => 'Brasileiro',
                        ],
                        '2' => [
                            'company_name'      => $last_names . ' Company',
                            'cnpj'              => mt_rand ( 12345678911234 , 99999999999999 ),
                        ],
                    ];

                    $account_data = array_merge([
                        'person_type'       => $type_person,
                        'telephone_1'       => '(11) 9999-9999',
                        'telephone_2'       => '(11) 92222-9999',

                        // -- contact
                        'address'           => 'Rua dos Enderecos',
                        'number'            => '300',
                        'zip_code'          => '99999-222',
                        'neighborhood'      => 'Bairro xyz',
                        'city'              => 'São Paulo',
                        'state'             => 'SP',
                        'met_for'           => '2',
                        'indicated_by'      => 'Alex',

                    ],$data_persons[$type_person]);

                    $email = snake_case($first_name.'_'.$last_names."@email.com");
                    $check_email = false;
                    while ($check_email == false){

                        if(User::whereEmail($email)->get()->isEmpty())
                        {
                            $check_email = true;
                        }
                        else
                        {
                            $email = snake_case($first_name.'_'.$last_names.mt_rand(1,546)."@email.com");
                        }

                    }

			        $user = User::create([

				        // -- personal data
				        'email'             => $email,
				        'password'          => \Hash::make('mudar123'),
                        'code_shared'       =>  \Illuminate\Support\Str::random(10),

                    ]);

                    $user->account()->create($account_data);

			        // -- attach role
			        $user->attachRole( $role_to_attach ); // parameter can be an Role object, array, or id
		        };

		        $user_role  = Role::whereName( Role::PROFILE_USER )->first();

                for($i = 0; $i < 20; $i++){
                    $createUser($user_role);
                }


	        }


        }
		catch (\Exception $ex)
		{
			throw new \Exception("Error: '{$ex->getMessage()}' on line {$ex->getLine()}");
		}
    }
}
