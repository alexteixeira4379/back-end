<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Recipe;

class RecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Recipe::create([

            'assignee_assignor' => 'www.pecontemplados.com.br',
            'kind' => 'Vendedor/Parceira',
            'value' => 4554.00,
            'date' => '01/01/2019'
        ]);

        Recipe::create([

            'assignee_assignor' => 'www.imoveispremiumconsorcio.com.br',
            'kind' => 'Vendedor/Parceira',
            'value' => 200.00,
            'date' => '01/01/2019'
        ]);

        Recipe::create([

            'assignee_assignor' => 'www.imoveispremiumconsorcio.com.br',
            'kind' => 'Vendedor/Parceira',
            'value' => 382.00,
            'date' => '01/01/2019'
        ]);
    }
}
