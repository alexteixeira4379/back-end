<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Taxation;

class TaxationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Taxation::create([

            'tribute' => 'Simples Nacional – 16%',
            'kind' => 'Vendedor/Parceira',
            'value' => 200.00,
            'date' => '01/01/2019'
        ]);


    }
}
