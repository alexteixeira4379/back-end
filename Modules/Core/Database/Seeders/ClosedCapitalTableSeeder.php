<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Capital;

class ClosedCapitalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach ( Capital::all() as $capital ){
            $capital->closed_capital()->create([
               'balance' => ($capital->balance - 157.37)
            ]);
        }
    }
}
