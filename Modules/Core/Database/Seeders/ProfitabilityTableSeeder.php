<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Profitability;

class ProfitabilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Profitability::create([
            'gross_sum' => 35000,
            'company_value' => 35000/2,
            'partners_value' => 35000/2,
            'profitability' => 1.6,
            'min_profitability' => 1.5,
            'created_at' =>"2019-01-01 00:00:00",
            'updated_at' =>"2019-01-01 00:00:00"

        ]);

        Profitability::create([
            'gross_sum' => 35000,
            'company_value' => 35000/2,
            'partners_value' => 35000/2,
            'profitability' => 2.0,
            'min_profitability' => 1.5,
            'created_at' =>"2019-02-01 00:00:00",
            'updated_at' =>"2019-02-01 00:00:00"

        ]);


        Profitability::create([
            'gross_sum' => 35000,
            'company_value' => 35000/2,
            'partners_value' => 35000/2,
            'profitability' => 2.0,
            'min_profitability' => 1.5,
            'created_at' =>"2019-03-01 00:00:00",
            'updated_at' =>"2019-03-01 00:00:00"

        ]);


        Profitability::create([
            'gross_sum' => 35000,
            'company_value' => 35000/2,
            'partners_value' => 35000/2,
            'profitability' => 1.8,
            'min_profitability' => 1.5,
            'created_at' =>"2019-04-01 00:00:00",
            'updated_at' =>"2019-04-01 00:00:00"

        ]);

        Profitability::create([
            'gross_sum' => 35000,
            'company_value' => 35000/2,
            'partners_value' => 35000/2,
            'profitability' => 2.5,
            'min_profitability' => 1.5,
            'created_at' =>"2019-08-01 00:00:00",
            'updated_at' =>"2019-08-01 00:00:00"
        ]);




    }
}
