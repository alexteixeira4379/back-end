<?php

namespace Modules\Core\Database\Seeders;

use App;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Bank;
use Modules\Core\Entities\BankAccount;
use User;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        if(App::environment() !== 'production')
        {
            $banks = Bank::all();

            $bank_accounts = [
                [
                    'bank_id'           => $banks->random()->id,

                    'document_number'   => '387914729',
                    'owner_name'        => 'Favorecido A',
                    'agency'            => '1626',
                    'agency_digit'      => null,
                    'account_number'    => '454398',
                    'account_digit'     => null,
                    'document_type'     => collect(BankAccount::DOCUMENT_TYPES)->random(),
                    'account_type'      => collect(BankAccount::ACCOUNT_TYPES)->random(),
                ],
                [
                    'bank_id'           => $banks->random()->id,

                    'document_number'   => '259255312',
                    'owner_name'        => 'Favorecido B',
                    'agency'            => '0240',
                    'agency_digit'      => '1',
                    'account_number'    => '16816',
                    'account_digit'     => '3',
                    'document_type'     => collect(BankAccount::DOCUMENT_TYPES)->random(),
                    'account_type'      => collect(BankAccount::ACCOUNT_TYPES)->random(),
                ],
                [
                    'bank_id'           => $banks->random()->id,

                    'document_number'   => '205030609',
                    'owner_name'        => 'Favorecido C',
                    'agency'            => '3221',
                    'agency_digit'      => null,
                    'account_number'    => '1572304',
                    'account_digit'     => null,
                    'document_type'     => collect(BankAccount::DOCUMENT_TYPES)->random(),
                    'account_type'      => collect(BankAccount::ACCOUNT_TYPES)->random(),
                ],
                [
                    'bank_id'           => $banks->random()->id,

                    'document_number'   => '351862109',
                    'owner_name'        => 'Favorecido D',
                    'agency'            => '5607',
                    'agency_digit'      => null,
                    'account_number'    => '42200',
                    'account_digit'     => null,
                    'document_type'     => collect(BankAccount::DOCUMENT_TYPES)->random(),
                    'account_type'      => collect(BankAccount::ACCOUNT_TYPES)->random(),
                ],
            ];


            foreach (User::all() as $user)
            {
                shuffle($bank_accounts);
                $user->bank_account()->create($bank_accounts[0]);

            }


        }

    }
}
