<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Core\Entities\File as File;


class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Modules\Core\Entities\File())->getTable(), function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('user_id');

	        $table->string('name');
	        $table->string('file_name');
	        $table->string('file_type');
	        $table->integer('size');
	        $table->enum('status', File::FILE_STATUSES)->default(File::FILE_UNPROCESSED);
	        $table->dateTime('date_process')->nullable();

	        $table->foreign('user_id')
	              ->references('id')
	              ->on( with(new Modules\Core\Entities\User())->getTable() )
	              ->onUpdate('no action')
	              ->onDelete('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Modules\Core\Entities\File())->getTable() );
    }
}
