<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new UserAccount())->getTable(), function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('user_id');
            $table->unsignedInteger('indicated_user_id')->nullable();


            $table->enum('person_type', UserAccount::PERSON_TYPE_STATUSES)->default(null);

            // -- personal physics data
            $table->string('full_name')->nullable();
            $table->string('cpf', 14)->unique()->nullable();
            $table->string('rg', 9)->unique()->nullable();
            $table->date('birthday')->nullable();
            $table->enum('marital_status', UserAccount::MARITAL_STATUS_STATUSES)->default(null)->nullable();
            $table->string('nationality')->nullable();
            $table->string('profession')->nullable();

            // -- legal person data
            $table->string('company_name')->nullable();
            $table->string('cnpj', 14)->unique()->nullable();


            // -- contact
            $table->string('telephone_1')->nullable();
            $table->string('telephone_2')->nullable();
            $table->string('address');
            $table->string('address_2')->nullable();
            $table->integer('number');
            $table->string('zip_code', 20);
            $table->string('neighborhood');
            $table->string('city');
            $table->enum('state', UserAccount::STATES);
            $table->string('country')->default('Brasil');

            $table->enum('met_for', UserAccount::MET_FOR_STATUSES)->default(null);
            $table->text('indicated_by');

            $table->boolean('automatic_application')->default(false);

            $table->enum('active', [0, 1])->default(1);

	        $table->foreign('user_id')
	              ->references('id')
	              ->on( with(new Modules\Core\Entities\User())->getTable() )
	              ->onUpdate('no action')
	              ->onDelete('no action');


            $table->foreign('indicated_user_id')
                ->references('id')
                ->on( with(new Modules\Core\Entities\User())->getTable() )
                ->onUpdate('no action')
                ->onDelete('no action');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new UserAccount())->getTable() );
    }
}
