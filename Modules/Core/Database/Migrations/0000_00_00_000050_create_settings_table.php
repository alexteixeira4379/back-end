<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( with(new Modules\Core\Entities\Setting())->getTable(), function (Blueprint $table) {
			$table->increments('id');

			// -- general settings
			$table->string('site_title');
			$table->string('footer_text')->nullable();

			$table->string('company_name')->nullable();
			$table->bigInteger('company_cnpj')->nullable();

            $table->decimal('min_profitability');
            $table->enum('profitability_type', Setting::PROFITABILITY_STATUSES)->default(Setting::PROFITABILITY_FIXED);
            $table->string('day_operation');


			$table->string('smtp_host');
			$table->string('smtp_username');
			$table->string('smtp_password');
			$table->integer('smtp_port');
			$table->enum('smtp_encryption_type', AppHelper::SMTP_ENCRYPTION_TYPES)
			      ->nullable()
			      ->default(AppHelper::SMTP_ENCRYPTION_TYPE_TLS);

			$table->string('primary_color')->default('#FFE11F');
			$table->string('secondary_color')->default('#1651B8');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( with(new Modules\Core\Entities\Setting)->getTable() );
	}
}
