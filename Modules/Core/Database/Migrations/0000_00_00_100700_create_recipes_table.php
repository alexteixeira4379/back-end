<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Core\Entities\Recipe;


class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Modules\Core\Entities\Recipe())->getTable(), function (Blueprint $table) {
	        $table->increments('id');

            $table->text('assignee_assignor');
            $table->text('kind');
            $table->decimal('value');

            $table->date('date')->default(null);
            $table->text('file_name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Modules\Core\Entities\Recipe())->getTable() );
    }
}
