<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Core\Entities\BankAccount;


class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Modules\Core\Entities\BankAccount())->getTable(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('bank_id');

            $table->string('document_number', 20);
            $table->string('owner_name');
            $table->string('agency');
            $table->string('agency_digit')->nullable();
            $table->string('account_number');
            $table->string('account_digit')->nullable();

            $table->enum('document_type', BankAccount::DOCUMENT_TYPES);
            $table->enum('account_type', BankAccount::ACCOUNT_TYPES);

            $table->enum('active', BankAccount::ACTIVE_OPTIONS)
                ->default(BankAccount::ACTIVE_YES);

            $table->foreign('user_id')
                ->references('id')
                ->on( with(new Modules\Core\Entities\User())->getTable() )
                ->onUpdate('no action')
                ->onDelete('no action');

            $table->foreign('bank_id')
                ->references('id')
                ->on( with(new Modules\Core\Entities\Bank())->getTable() )
                ->onUpdate('no action')
                ->onDelete('no action');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Modules\Core\Entities\BankAccount)->getTable() );
    }
}
