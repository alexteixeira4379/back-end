<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Entities\ClosedCapital;

class CreateClosedCapitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new ClosedCapital())->getTable(), function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('capital_id');

	        $table->decimal('balance');

	        $table->foreign('capital_id')
	              ->references('id')
	              ->on( with(new Modules\Core\Entities\Capital())->getTable() )
	              ->onUpdate('no action')
	              ->onDelete('no action');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new ClosedCapital())->getTable() );
    }
}
