<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Entities\Capital;

class CreateCapitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Capital())->getTable(), function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('user_id');

	        $table->decimal('balance');

	        $table->foreign('user_id')
	              ->references('id')
	              ->on( with(new Modules\Core\Entities\User())->getTable() )
	              ->onUpdate('no action')
	              ->onDelete('no action');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Capital())->getTable() );
    }
}
