<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Modules\Core\Entities\User)->getTable(), function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('user_id')->nullable();

	        // -- personal data
	        $table->string('email')->nullable()->unique();
	        $table->string('password');
	        $table->enum('active', [0, 1])->default(1);
            $table->string('code_shared')->nullable()->unique();


            $table->foreign('user_id')
                ->references('id')
                ->on( with(new Modules\Core\Entities\User())->getTable() )
                ->onUpdate('no action')
                ->onDelete('no action');


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Modules\Core\Entities\User)->getTable() );
    }
}
