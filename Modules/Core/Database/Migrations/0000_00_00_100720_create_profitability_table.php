<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Core\Entities\Profitability;


class CreateProfitabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Modules\Core\Entities\Profitability())->getTable(), function (Blueprint $table) {

	        $table->increments('id');

            $table->decimal('gross_sum');
            $table->decimal('company_value');
            $table->decimal('partners_value');
            $table->decimal('profitability',8,6);
            $table->decimal('min_profitability',8,6);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Modules\Core\Entities\Profitability())->getTable() );
    }
}
