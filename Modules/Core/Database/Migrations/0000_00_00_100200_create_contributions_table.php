<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Core\Entities\Contribution as Contribution;


class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( with(new Modules\Core\Entities\Contribution())->getTable(), function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('user_id');

	        $table->decimal('value');
	        $table->date('date')->default(null);
            $table->enum('status', Contribution::CONTRIBUTION_STATUSES)->default(Contribution::CONTRIBUTION_PENDING);
            $table->enum('type', Contribution::CONTRIBUTION_TYPE_STATUSES)->default(Contribution::CONTRIBUTION_TYPE_NORMAL);
            $table->text('file_name');

	        $table->foreign('user_id')
	              ->references('id')
	              ->on( with(new Modules\Core\Entities\User())->getTable() )
	              ->onUpdate('no action')
	              ->onDelete('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( with(new Modules\Core\Entities\Contribution())->getTable() );
    }
}
