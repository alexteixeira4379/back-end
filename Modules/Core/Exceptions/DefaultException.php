<?php

namespace Modules\Core\Exceptions;

/**
 * Class DefaultException
 *
 * @package Modules\Core\Exceptions
 */
class DefaultException extends \Exception
{
	/**
	 * Report the exception.
	 *
	 * @return void
	 */
	public function report()
	{
		//
	}

	/**
	 * Render the exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function render($request)
	{


		return response()
			->view('admin::errors.default', [
				'error' => $this->getMessage(),
				'error_code' => $this->getCode() ? $this->getCode() : 500,
			], $this->getCode() ? $this->getCode() : 500);
	}

}