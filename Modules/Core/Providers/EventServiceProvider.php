<?php
namespace Modules\Core\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Core\Events\ProfitabilityCreated;
use Modules\Core\Listeners\GeneratePartnerProfitability;
use Modules\Core\Events\TransactionCreated;
use Modules\Core\Listeners\ChangeCapitalBalance;

class EventServiceProvider extends ServiceProvider
{

    protected $listen = [
        ProfitabilityCreated::class =>
        [
            GeneratePartnerProfitability::class
        ],
        TransactionCreated::class =>
        [
            ChangeCapitalBalance::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}