<?php

namespace Modules\Core\Providers;

use DaveJamesMiller\Breadcrumbs\ServiceProvider;
use Nwidart\Modules\Module;
use SplFileInfo;

/**
 * Class BreadcrumbServiceProvider
 * Overrides the default path of breadcrumbs
 *
 * @author Felipe Douradinho
 * @package Modules\Core\Providers
 */
class BreadcrumbServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function registerBreadcrumbs()
    {
    	if(!defined( 'DS' ))
    	    define('DS', DIRECTORY_SEPARATOR);

	    // -- if default file exists
	    if(file_exists(base_path('routes/breadcrumbs.php')))
	    {
		    /** @noinspection PhpIncludeInspection */
		    require base_path('routes/breadcrumbs.php');
	    }

	    foreach (\Module::all() as $module)
	    {
		    /** @var $module Module */
		    $path = $module->getPath();
		    $breadcrumb_dir = $path . DS . 'Config' . DS . 'breadcrumbs';

		    if(file_exists( $breadcrumb_dir ))
		    {
		    	// -- list all php files inside
			    foreach (\File::allFiles($breadcrumb_dir) as $breadcrumb_file)
			    {
			    	/** @var $breadcrumb_file SplFileInfo */
				    /** @noinspection PhpIncludeInspection */
				    require $breadcrumb_file->getRealPath();
			    }
		    }
	    }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
