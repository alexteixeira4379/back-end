<?php
namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class ScheduleServiceProvider extends ServiceProvider
{

    public function boot()
    {
        /**
         * The Artisan commands provided by your application.
         */
        $this->commands([
            \Modules\Core\Console\CreateProfitability::class,
        ]);

        $this->app->booted(function ()
        {
            $schedule = $this->app->make(Schedule::class);

            /**
             * The schedules
             */

            $schedule->command('command:createprofitability')->daily();

        });
    }


    public function register()
    {
        //
    }

}