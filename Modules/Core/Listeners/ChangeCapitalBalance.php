<?php
namespace Modules\Core\Listeners;

use App\Events\Event;
use Modules\Core\Events\TransactionCreated;
use Modules\Core\Entities\Capital;

class ChangeCapitalBalance
{

    /** @var Capital $capital */
    public $capital;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Action change Capital Balance
     *
     * @param TransactionCreated $event
     */
    public function handle(TransactionCreated $event)
    {
        $this->capital = $event->user->capital;

        if($event->transaction->typeable_type == \Modules\Core\Entities\Contribution::class)
            $this->increaseBalance($event->transaction->value);

        else if($event->transaction->typeable_type == \Modules\Core\Entities\Withdrawal::class )
                $this->decreaseBalance($event->transaction->value);

        $this->capital->save();
    }

    /**
     * Contribution Balance
     *
     * @param $value
     */
    private function decreaseBalance(float $value ){
       $this->capital->balance -= $value;
    }

    /**
     * Withdrawal Balance
     *
     * @param $value
     */
    private function increaseBalance( float $value ){
        $this->capital->balance += $value;
    }

}