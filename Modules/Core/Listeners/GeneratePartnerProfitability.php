<?php
namespace Modules\Core\Listeners;

use Modules\Core\Entities\Withdrawal;
use Modules\Core\Events\ProfitabilityCreated;
use Modules\Core\Notifications\ProfitApplied;
use Modules\Core\Notifications\ProfitRescued;

class GeneratePartnerProfitability
{


    public $profitability;
    public $profitability_type;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
      $this ->profitability  = \ConfigHelper::get('min_profitability') / 100;
      $this ->profitability_type  =  floatval(\ConfigHelper::get('profitability_type'));

    }


    /**
     * Action of generate capital with profitability of users

     *
     * @param ProfitabilityCreated $event
     * @throws \Throwable
     */
    public function handle(ProfitabilityCreated $event)
    {

        /** checking operation mode and min values */
        $new_profitability = floatval($event->profitability->profitability);
        if($this->profitability_type == \Setting::PROFITABILITY_DYNAMIC && $new_profitability > $this->profitability)
           $this ->profitability = $new_profitability;


        /** handle operations */
        $users = \Role::where('name','user')->first()->users()->with('capital','account')->get();
        foreach ($users as $user)
        {
            $this->setPartnerProfitability($user);
        }

    }


    /**
     * @param \User $user
     * @throws \Throwable
     */
    private function setPartnerProfitability(\User $user)
    {

        $value = $this->getUserProfitability($user->capital->balance);
        /** create a new contribution or withdrawal from profitability value */
        $data = [
            'user_id' => $user->id,
            'value' => $value,
            'date' => date("Y-m-d"),
            'status' => \Contribution::CONTRIBUTION_APPROVED,
            'type' => \Contribution::CONTRIBUTION_TYPE_PROFITABILITY,
        ];

        \DB::transaction(function() use($data,$user)
        {

            /** close capital */
            $user->capital->closed_capital()->create([
                'balance' => $user->capital->balance
            ]);

            if($user->account->automatic_application){

                /** @var \Modules\Core\Entities\Contribution $contribution*/
                $contribution = \DBHelper::update(\Contribution::class,$data);
                \Notification::send($user, new ProfitApplied($contribution));
            }
            else{
                $data['status'] = Withdrawal::WITHDRAWAL_PENDING;
                $data['type'] =   Withdrawal::WITHDRAWAL_TYPE_PROFITABILITY;

                /** @var \Modules\Core\Entities\Contribution $contribution*/
                $withdrawal = \DBHelper::update(Withdrawal::class,$data);
                \Notification::send($user, new ProfitRescued($withdrawal));
            }


        });

    }

    /**
     * @param float $balance
     * @return float|int
     */
    private function getUserProfitability(float $balance)
    {

        return ($balance*($this->profitability))-$balance;

    }


}