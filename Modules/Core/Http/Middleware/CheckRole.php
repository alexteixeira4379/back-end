<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try
	    {
		    if(! \Auth::getUser()->can( \Route::getCurrentRoute()->getName() ))
				throw new \Exception('Erro: Não autorizado.', 1);
	    }
	    catch(\Exception $ex)
	    {
	    	if($request->ajax())
		    {
			    return response()->json([
				    'status' => 'error',
				    'data' => $ex->getMessage()
			    ], 403);
		    }

		    if(\URL::previous())
			    return redirect()->back()->withErrors([ 'msg' => $ex->getMessage() ]);

		    return redirect(route('admin.dashboard.index'))->withErrors([ 'msg' => $ex->getMessage() ]);
	    }

        return $next($request);
    }
}
