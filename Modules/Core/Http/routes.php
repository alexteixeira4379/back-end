<?php

Route::group([
	'middleware' => ['web'], 'namespace' => 'Modules\Core\Http\Controllers'], function()
{
	// Authentication Routes...
	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@authenticate')->name('authenticate');
	Route::get('logout', 'LoginController@logout')->name('logout');

	// -- Medias
	Route::resource('/medias', 'MediaController');
	Route::get('/medias/{id}/thumb/{width?}/{height?}', 'MediaController@thumb')->name('medias.thumb');
	Route::get('/medias/{id}/download', 'MediaController@download')->name('medias.download');

});