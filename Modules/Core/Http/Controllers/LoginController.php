<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Modules\Core\Helpers\RequestHelper;
use Redirect;
use Request;

class LoginController extends Controller
{
	/**
	 * Handle an authentication attempt.
	 *
	 * @return bool|RedirectResponse
	 */
	public function authenticate()
	{
		$options = [
			RequestHelper::REDIRECT_ERROR => route('login'),
			RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(), [
				'email'     => 'required',
				'password'  => 'required',
			], [
				'email.required'      => 'Desculpe, o E-mail é obrigatório',
				'password.required' => 'Desculpe, a senha é obrigatória',
			])
		];

		return RequestHelper::doRequest(function()
		{
			if(\Auth::attempt(['email' => \Request::get('email'), 'password' => \Request::get('password') ],
				\Request::filled('remember')))
			{
				if(\Session::has('from_url'))
					return RequestHelper::$redirect_success_url = \Session::pull('from_url');

				return RequestHelper::$redirect_success_url = route('admin.dashboard.index');

//				if(\Auth::getUser()->hasRole( Role::PROFILE_ADMIN ))
//					return RequestHelper::$redirect_success_url = route('admin.dashboard.index');

//				if(\Auth::getUser()->hasRole( Role::PROFILE_SUPPLIER ))
//					return RequestHelper::$redirect_success_url = route('customer.dashboard.index');
			}

			return Redirect::route('login')->withErrors(['Usuário ou senha inválidos']);

		}, $options);
	}

	/**
	 * Show the application's login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm()
	{
		return view('core::pages.login.index');
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param \Illuminate\Http\Request|Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
		Auth::logout();

		return redirect( route('login') );
	}

}
