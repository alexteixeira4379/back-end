<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3247002eb2d2fb49412b6e5a11e70e83
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Modules\\Core\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Modules\\Core\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static $classMap = array (
	    'Modules\\Core\\Database\\Seeders\\QuestionCategoriesTableSeeder' => __DIR__ . '/../..',
	    'Modules\\Core\\Database\\Seeders\\SettingsTableSeeder'           => __DIR__ . '/../..',
	    'Modules\\Core\\Database\\Seeders\\CoreDatabaseSeeder'            => __DIR__ . '/../..' . '/Database/Seeders/CoreDatabaseSeeder.php',
	    'Modules\\Core\\Database\\Seeders\\PackagesTableSeeder'           => __DIR__ . '/../..' . '/Database/Seeders/PackagesTableSeeder.php',
	    'Modules\\Core\\Database\\Seeders\\PermissionsTableSeeder'        => __DIR__ . '/../..' . '/Database/Seeders/PermissionsTableSeeder.php',
	    'Modules\\Core\\Database\\Seeders\\PiecesTableSeeder' => __DIR__ . '/../..' . '/Database/Seeders/PiecesTableSeeder.php',
	    'Modules\\Core\\Database\\Seeders\\PlansTableSeeder'  => __DIR__ . '/../..' . '/Database/Seeders/PlansTableSeeder.php',
	    'Modules\\Core\\Database\\Seeders\\RolesTableSeeder'  => __DIR__ . '/../..' . '/Database/Seeders/RolesTableSeeder.php',
	    'Modules\\Core\\Database\\Seeders\\UsersTableSeeder'  => __DIR__ . '/../..' . '/Database/Seeders/UsersTableSeeder.php',
	    'Modules\\Core\\Entities\\QuestionCategory'           => __DIR__ . '/../..',
	    'Modules\\Core\\Entities\\Setting'                    => __DIR__ . '/../..',
	    'Modules\\Core\\Entities\\Package'                    => __DIR__ . '/../..' . '/Entities/Package.php',
	    'Modules\\Core\\Entities\\Permission'                 => __DIR__ . '/../..' . '/Entities/Permission.php',
	    'Modules\\Core\\Entities\\Piece'                      => __DIR__ . '/../..' . '/Entities/Piece.php',
	    'Modules\\Core\\Entities\\Plan'                       => __DIR__ . '/../..' . '/Entities/Plan.php',
	    'Modules\\Core\\Entities\\Role'                       => __DIR__ . '/../..' . '/Entities/Role.php',
	    'Modules\\Core\\Entities\\User'                       => __DIR__ . '/../..' . '/Entities/User.php',
	    'Modules\\Core\\Events\\UserUpdating'               => __DIR__ . '/../..' . '/Events/UserUpdating.php',
	    'Modules\\Core\\Helpers\\ConfigHelper'              => __DIR__ . '/../..' . '/Helpers/ConfigHelper.php',
	    'Modules\\Core\\Helpers\\DBHelper'                  => __DIR__ . '/../..' . '/Helpers/DBHelper.php',
	    'Modules\\Core\\Helpers\\DataTableHelper'           => __DIR__ . '/../..' . '/Helpers/DataTableHelper.php',
	    'Modules\\Core\\Helpers\\EventHelper'               => __DIR__ . '/../..' . '/Helpers/EventHelper.php',
	    'Modules\\Core\\Helpers\\FileHelper'                => __DIR__ . '/../..' . '/Helpers/MediaHelper.php',
	    'Modules\\Core\\Helpers\\RequestHelper'             => __DIR__ . '/../..' . '/Helpers/RequestHelper.php',
	    'Modules\\Core\\Helpers\\ResponseHelper'            => __DIR__ . '/../..' . '/Helpers/ResponseHelper.php',
	    'Modules\\Core\\Http\\Controllers\\CoreController'  => __DIR__ . '/../..' . '/Http/Controllers/CoreController.php',
	    'Modules\\Core\\Http\\Controllers\\LoginController' => __DIR__ . '/../..' . '/Http/Controllers/LoginController.php',
	    'Modules\\Core\\Providers\\CoreServiceProvider' => __DIR__ . '/../..' . '/Providers/CoreServiceProvider.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3247002eb2d2fb49412b6e5a11e70e83::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3247002eb2d2fb49412b6e5a11e70e83::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit3247002eb2d2fb49412b6e5a11e70e83::$classMap;

        }, null, ClassLoader::class);
    }
}
