<?php
namespace Modules\Core\Helpers
{
	use Closure;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Http\Request;
	use Illuminate\Http\UploadedFile;
	use Illuminate\Support\Facades\DB;
	use Nwidart\Modules\Facades\Module;

	/**
	 * Class FileHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class FileHelper
	{
		/**
		 * Set the storage page (inside `/storage` directory)
		 */
		const STORE_PATH = 'app/public/';

		/**
		 * Set the html|form field name
		 */
		const FILE_UPLOAD_FIELD_NAME = 'file-upload';

		/**
		 * Set the file class name
		 */
		const FILE_MODEL = \Modules\Core\Entities\File::class;
		

		/**
		 * Download a file
		 *
		 * @param int $file_id
		 * @param Request|null $request
		 *
		 * @return mixed
		 */
		public static function download($file_id, Request $request = null)
		{
			/** @var $file_model Model */
			$file_model = self::FILE_MODEL;

			if($media = $file_model::find($file_id))
			{
				$full_path = self::getPath($file_id);

				// -- check for file existence
				if(!\File::exists($full_path))
					return abort(404);

				// -- download it
				return response()->download($full_path, \File::basename($full_path));
			}

			return abort(404);
		}

		/**
		 * Upload a file
		 *
		 * @param Request $request
		 * @param string $field_name
		 * @param array $extra_fields
		 *
		 * @param null|Closure $callback
		 *
		 * @return bool|array|Closure
		 * @throws \Exception
		 * @throws \Throwable
		 */
		public static function upload(Request $request, $field_name, array $extra_fields = [ ], $callback = null)
		{
			$return = false;

			\DB::transaction(function() use(&$return, $request, $field_name, $extra_fields, $callback)
			{
				if ($request->hasFile( $field_name ))
				{
					$media_data = [ ];

					// -- a method to get defaults
					$getDefaults = function(UploadedFile $file) {
						return $defaults_fields = [
							'name' => $file->getClientOriginalName(),
							'file_name' => $file->getClientOriginalName(),
							'file_type' => $file->getMimeType(),
							'size' => $file->getSize(),
						];
					};

					// -- multiple file upload
					if( is_array( $request->$field_name ) )
					{
						foreach ( $request->$field_name as $file_media )
						{
							// -- replace
							$extra_fields = array_merge( $getDefaults($file_media), $extra_fields );

							$media_data[] = self::doUpload($file_media, $extra_fields);
						}
					}

					// -- single file upload
					else
					{
						// -- replace
						$extra_fields = array_merge( $getDefaults($request->$field_name), $extra_fields );

						$media_data[] = self::doUpload($request->$field_name, $extra_fields);
					}

					if($callback)
					{
						$callback( $request, $media_data );
					}

					$return = $media_data;
				}
			});

			return $return;
		}

		/**
		 * @param UploadedFile $file_media
		 * @param array $extra_fields
		 *
		 * @return array
		 * @throws \Exception
		 */
		protected static function doUpload(UploadedFile $file_media, array $extra_fields = [ ])
		{
			/** @var $file_model Model */
			$file_model = self::FILE_MODEL;

			$file = $file_model::create($extra_fields);
			$path = $file_media->storeAs('public', "{$file->id}.{$file_media->extension()}");

			if(!file_exists(storage_path('app/' . $path)))
				throw new \Exception('Desculpe, ocorreu algum problema durante o upload do arquivo!', 1);

			return [
				'id'            => $file->id,
				'path'          => self::getPath($file->id),
				'extra_fields'  => $extra_fields,
			];
		}

		/**
		 * Upload a file
		 *
		 * @param int $file_id
		 *
		 * @return bool
		 * @throws \Exception
		 * @throws \Throwable
		 */
		public static function destroy($file_id)
		{
			$return = false;

			DB::transaction(function() use(&$return, $file_id)
			{
				/** @var $file_model Model */
				$file_model = self::FILE_MODEL;

				$file_model::destroy($file_id);

				if($media = $file_model::find($file_id))
					throw new \Exception('Desculpe, ocorreu algum problema ao tenta excluir o arquivo no banco de dados', 1);

				// -- get file
				$media_file = self::getPath() . self::getFileName($file_id);

				// -- try to delete from file
				@unlink($media_file);

				// -- if file still exists
				if(file_exists( $media_file ) && !is_dir($media_file))
					throw new \Exception('Desculpe, ocorreu algum problema ao tenta excluir o arquivo no disco', 1);

				$return = true;
			});

			return $return;
		}

		/**
		 * Get a path name
		 *
		 * @param int|null $id
		 *
		 * @return mixed
		 */
		public static function getPath($id = null)
		{
			return storage_path( self::STORE_PATH ) . ( $id ? self::getFileName( $id ) : '' );
		}

		/**
		 * Get a media full URL
		 *
		 * @param $id
		 *
		 * @return string
		 * @throws \Nwidart\Modules\Facades\InvalidAssetPath
		 */
		public static function getUri( $id )
		{
			if($file_name = self::getFileName( $id ))
			{
				return asset("storage/{$file_name}");
			}

			return Module::asset('core:images/no-image.jpg');
		}

		/**
		 * Get an storage pull path
		 *
		 * @param $id
		 *
		 * @return string|bool
		 */
		public static function getFileName( $id )
		{
			// -- search the file based on
			$result = glob( self::getPath() . "{$id}.*");

			if(!empty($result))
				return basename( $result[0] );

			return false;
		}

	}
}