<?php
namespace Modules\Core\Helpers
{

    use Carbon\Carbon;
    use Illuminate\Support\Collection;
    use Modules\Core\Entities\Capital;
    use Modules\Core\Entities\ClosedCapital;
    use Modules\Core\Entities\Profitability;

    /**
	 * Class CapitalHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class DemonstrativeHelper
	{


        /**
         * @param Collection $recipes
         * @param Collection $expenses
         * @param Collection $taxations
         * @return array
         */
        public static function getCalculatedNet(Collection $recipes, Collection $expenses, Collection $taxations)
        {

            $totalRecipes = $recipes->sum('original_value');
            $totalDispenses = $expenses->sum('original_value') + $taxations->sum('original_value');
            $profit = $totalRecipes - $totalDispenses;

           return [
                'totalRecipes' => 'R$ ' . number_format($totalRecipes, 2, ',', '.'),
                'totalDispenses' => 'R$ ' . number_format($totalDispenses, 2, ',', '.'),
                'profit' => 'R$ ' . number_format($profit, 2, ',', '.')
            ];

        }

        public static function getProfitDistribution( $date )
        {

            $year_filter_select =  explode('-', $date)[1];
            $month_filter_select = explode('-', $date)[0];

            $profitability = Profitability::whereYear('created_at',$year_filter_select)
                ->whereMonth('created_at', $month_filter_select)->get();


            if(! $profitability->isEmpty() && $profitability = $profitability->first() )
            {

                $profitability = $profitability->profitability/100;


                return [
                    'value' => 'R$ '.number_format(10000, 2, ',', '.'),
                    'participation_index' => number_format($profitability, 6, ',', ' '),
                    'result' => 'R$ '.number_format((10000 * $profitability), 2, ',', '.'),
                ];

            }
            else
            {
                return [
                    'value' => '---',
                    'participation_index' => '---',
                    'result' => '---',
                ];
            }

        }

        public static function getParticipationIndex($date)
        {
            $year_filter_select =  explode('-', $date)[1];
            $month_filter_select = explode('-', $date)[0];

            $profitability = Profitability::whereYear('created_at',$year_filter_select)
                ->whereMonth('created_at', $month_filter_select)->get();


            if(! $profitability->isEmpty() && $profitability = $profitability->first() )
            {

                if($year_filter_select  == Carbon::now()->year && $month_filter_select == Carbon::now()->month)
                {
                    $capital_partner = 'R$ '.number_format(Capital::all()->sum('balance'), 2, ',', '.');
                }

                $shared_capital = ClosedCapital::whereYear('created_at', '=', $year_filter_select)
                    ->whereMonth('created_at', '=',$month_filter_select)->get()->sum('balance');

                $capital_partner = 'R$ '.number_format($shared_capital, 2, ',', '.');


                return [
                    'partner_profit' => 'R$ '.number_format($profitability->partners_value, 2, ',', '.'),
                    'partner_profitability' => number_format(($profitability->profitability/100), 6, ',', ' '),
                    'partner_capital' => $capital_partner,
                ];

            }
            else
            {
                return [
                    'partner_profit' => '---',
                    'partner_profitability' => '---',
                    'partner_capital' => '---',
                ];
            }

        }
    }
}