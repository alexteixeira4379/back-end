<?php

namespace Modules\Core\Helpers
{
	use \Illuminate\Support\Collection;

	/**
	 * Class DatatableHelper
	 *
	 * @package app\Helpers
	 */
	class DataTableHelper
	{
		const DOM = "<'row'<'col-sm-6'B><'col-sm-6'>> <'row'<'col-sm-6 div-limit-place'l><'col-sm-6 div-filter-place'f>> <'row'<'col-sm-12'tr>> <'row'<'col-sm-5'i><'col-sm-7'p>>";
		const DOM_NO_EXPORT = "<'row'<'col-sm-6 div-limit-place'l><'col-sm-6 div-filter-place'f>> <'row'<'col-sm-12'tr>> <'row'<'col-sm-5'i><'col-sm-7'p>>";
		const DOM_ONLY_TABLE = "<'row'<'col-sm-12'tr>>";
		const DOM_WITH_FOOTER = "<'row'<'col-sm-6'B><'col-sm-6'>> <'row'<'col-sm-6 div-limit-place'l><'col-sm-6 div-filter-place'f>> <'row'<'col-sm-12'tr>> <'row'<'col-sm-5'i><'col-sm-7'p>>";

		const CALLBACK_INIT_CALLBACK = 'CoreModule.datatables.initCallback';
		const CALLBACK_ROW_CALLBACK = 'CoreModule.datatables.rowCallback';
		const CALLBACK_DRAW_CALLBACK = 'CoreModule.datatables.drawCallback';

		const ACTION_EDIT = 'edit';
		const ACTION_VIEW = 'view';
		const ACTION_DESTROY = 'destroy';

		const ACTIONS = [
			self::ACTION_EDIT,
			self::ACTION_VIEW,
			self::ACTION_DESTROY,
		];

		/**
		 * Get an xeditable link struct
		 *
		 * @param array|Collection $params
		 *
		 * @return string
		 */
		public static function getXEditableLink($params)
		{
			$params = !is_array($params) ? $params->toArray() : $params;

			$a = '<a href="#" class="xeditable" ';
			$a .= 'data-type="'.$params['type'].'" ';
			$a .= 'data-name="'.$params['name'].'" ';
			$a .= 'data-pk="{{$id}}" ';
			$a .= 'data-url="'.$params['url'].'/{{$id}}" ';

			// -- populate options
			if($params['type'] == 'select')
			{
				$a .= 'data-value="{{$'.$params['name'].'}}" ';
				$a .= 'data-source="[ '.implode(',', $params['source']).' ]" ';
			}

			$a .= '>{{$'.$params['name'].'}}</a>';
			return $a;
		}

		/**
		 * Get action column
		 *
		 * @param array $actions
		 * @param bool $string
		 *
		 * @return string|array
		 */
		public static function getActions(array $actions = [ ], $string = true)
		{
			$buttons = [ ];

			foreach ($actions as $action => $params)
			{
				// -- class
				switch ((string) $action)
				{
					case DataTableHelper::ACTION_EDIT:
						$extra_attrs = array_key_exists('extra_attrs', $params) ? $params['extra_attrs'] : [ ];
						$class = array_key_exists('class', $params) ? $params['class'] : 'btn-primary';
						$title = array_key_exists('title', $params) ? $params['title'] : 'Editar';
						break;

					case DataTableHelper::ACTION_VIEW:
						$extra_attrs = array_key_exists('extra_attrs', $params) ? $params['extra_attrs'] : [ ];
						$class = array_key_exists('class', $params) ? $params['class'] : 'btn-info';
						$title = array_key_exists('title', $params) ? $params['title'] : 'Visualizar';
						break;

					case DataTableHelper::ACTION_DESTROY:
						$extra_attrs = array_key_exists('extra_attrs', $params) ? $params['extra_attrs'] : [ ];
						$class = array_key_exists('class', $params) ? $params['class'] : 'btn-danger bt-destroy';
						$title = array_key_exists('title', $params) ? $params['title'] : 'Excluir';
						break;

					default:
						$extra_attrs = array_key_exists('extra_attrs', $params) ? $params['extra_attrs'] : [ ];
						$class = array_key_exists('class', $params) ? $params['class'] : 'btn-default';
						$title = array_key_exists('title', $params) ? $params['title'] : '';
				}

				$buttons[] = html_entity_decode(
					link_to($params['route'], $params['button'], array_merge([
						'class' => "btn bg-purple {$class} btn-circle waves-effect waves-circle waves-float",
						'data-toggle' => 'tooltip', 'title' => $title,
					],$extra_attrs))
				);

				if(!array_key_exists( 'permission', $params) || !$params['permission'])
				{
					unset($buttons[ count($buttons) - 1 ]); // remove last
					$buttons = array_values( $buttons );
				}
			}

			return $string ? implode("\n", $buttons) : $buttons;
		}

		/**
		 * @param array $columns
		 * @param string $name
		 * @param string $value
		 *
		 * @return int
		 */
		public static function getColumnIndex( array $columns = [ ],  $name = '', $value = '' )
		{
			return collect($columns)->whereInStrict($name, $value)->keys()->first();
		}

		/**
		 * @param array $columns
		 * @param string $key
		 * @param string $value
		 *
		 * @return array
		 */
		public static function pullColumn( array &$columns = [ ], $key, $value )
		{
			/** @var $columns Collection */
			$columns = collect( $columns );

			$key_to_remove = $columns->where($key, $value)->keys()->first();
			$columns->pull( $key_to_remove );

			$columns = $columns->values()->toArray();
			return $columns;
		}

	}
}