<?php
namespace Modules\Core\Helpers
{
	use Illuminate\Pagination\LengthAwarePaginator;

	/**
	 * Class PaginatorHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class PaginatorHelper
	{
		const PAGE_NAME = 'page';

		/**
		 * @var int $max_per_page
		 */
		private $max_per_page = 10;

		/**
		 * @var int $total_records
		 */
		private $total_records = 0;

		/**
		 * @var $data array
		 */
		private $data = [ ];

		/**
		 * @var $paginator LengthAwarePaginator|null
		 */
		private $paginator = null;


		/**
		 * @param array $data
		 * @param int $max_per_page
		 */
		public function __construct( array $data = [ ], $max_per_page = 10 )
		{
			// -- set data
			$this->setData($data);

			// -- set data
			$this->setTotalRecords(count($data));

			// -- set max per page
			$this->setMaxPerPage( $max_per_page );

			// -- set paginator
			$this->setPaginator();

			return $this->getPaginator();
		}

		/**
		 * @return int
		 */
		public function getMaxPerPage() 
		{
			return $this->max_per_page;
		}

		/**
		 * @param int $max_per_page
		 */
		public function setMaxPerPage( $max_per_page ) 
		{
			$this->max_per_page = $max_per_page;
		}

		/**
		 * @return array
		 */
		public function getData()
		{
			return $this->data;
		}

		/**
		 * @param array $data
		 */
		public function setData( array $data = [ ])
		{
			$this->data = $data;
		}

		/**
		 * @return int
		 */
		public function getTotalRecords()
		{
			return $this->total_records;
		}

		/**
		 * @param int $total_records
		 */
		public function setTotalRecords( $total_records )
		{
			$this->total_records = $total_records;
		}

		/**
		 * @return LengthAwarePaginator|null
		 */
		public function getPaginator()
		{
			return $this->paginator;
		}

		/**
		 *
		 */
		public function setPaginator()
		{
			$page = \Request::get(self::PAGE_NAME, 1); // Get the current page or default to 1, this is what you miss!
			$offset = ($page * $this->getMaxPerPage()) - $this->getMaxPerPage();

			$this->paginator = new LengthAwarePaginator(
				array_slice($this->getData(), $offset, $this->getMaxPerPage(), true),
				($this->getTotalRecords() > -1 ? $this->getTotalRecords() : count($this->getData())),
				$this->getMaxPerPage(),
				$page,
				['path' => \Request::url(), 'query' => \Request::query()]
			);
		}

	}
}