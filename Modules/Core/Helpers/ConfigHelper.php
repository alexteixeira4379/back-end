<?php
namespace Modules\Core\Helpers
{

	use Modules\Core\Entities\Setting;

	/**
	 * Class ConfigHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class ConfigHelper
	{

		/**
		 * Get a site config
		 *
		 * @param string $key
		 *
		 * @return bool|mixed
		 */
		public static function get($key)
		{
			try
			{
				$data = Setting::firstOrFail()->$key;
				return strlen( $data ) > 0 ? $data : false;
			}
			catch (\Exception $ex)
			{
				return false;
			}
		}

		/**
		 * Get contact address
		 *
		 * @return string
		 */
		public static function getAddress()
		{
			// -- default
			$output = '';

			// - data
			$contact_address = self::get('contact_address');
			$contact_number = self::get('contact_number');
			$contact_neighborhood = self::get('contact_neighborhood');

			if($contact_address)
				$output .= "{$contact_address}";

			if($contact_number)
				$output .= ", {$contact_number} ";

			if($contact_neighborhood)
				$output .= "<br> {$contact_neighborhood} ";

			return $output;
		}

	}
}