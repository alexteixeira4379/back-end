<?php

namespace Modules\Core\Helpers;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait FilterTrait
 *
 * @package Modules\Core\Helpers
 */
trait FilterTrait
{

	/**
	 * Get filters
	 *
	 * @param Builder $query
	 * @param array $filters
	 *
	 * @return Builder
	 */
	public function scopeFiltered($query, array $filters = [ ])
	{
		$scope_filter = ( new ScopeHelper() )->setQuery( $query )->setFilters( $filters );
		return $scope_filter->doFilter();
	}

}