<?php
namespace Modules\Core\Helpers
{

	/**
	 * Class ResponseHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class ResponseHelper
	{
		const RESPONSE_SUCCESS = 'success';
		const RESPONSE_ERROR = 'error';

		const RESPONSE_TYPE_AJAX = 'ajax';
		const RESPONSE_TYPE_HTTP = 'http';

		/**
		 * Get a Response
		 *
		 * @param null|string $to
		 * @param string $message
		 * @param int $code
		 * @param string|null $response_type
		 * @param array $extra_fields
		 *
		 * @return \Illuminate\Http\RedirectResponse
		 */
		public static function getResponse($to = null, $message = '', $code = 200, $response_type = null, array $extra_fields = [ ])
		{
			$status = $code == 200 ? self::RESPONSE_SUCCESS : self::RESPONSE_ERROR;

			$response_type = $response_type ? $response_type :
				( \Request::ajax() ? self::RESPONSE_TYPE_AJAX : self::RESPONSE_TYPE_HTTP );

			// -- if is ajax
			if( $response_type == self::RESPONSE_TYPE_AJAX )
			{
				$data = array_merge([
					'status' => $status,
					'message' => $message,
				], $extra_fields);

				return response()->json($data, $code);
			}

			if( $to && $status == self::RESPONSE_SUCCESS )
				return redirect( $to )->with('message', $message);

			if( $to && $status == self::RESPONSE_ERROR )
				return redirect( $to )->withInput()->withErrors([ 'error' => $message ]);

			if( !$to && $status == self::RESPONSE_SUCCESS )
				return redirect()->back()->with('message', $message);

			if( !$to && $status == self::RESPONSE_ERROR )
				return redirect()->back()->withInput()->withErrors([ 'error' => $message ]);

			return redirect()->back()->with('message', $message);
		}

	}
}