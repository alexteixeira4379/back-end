<?php

namespace Modules\Core\Helpers;

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\Role;
use Modules\Core\Entities\User;

/**
 * Trait PermissionTrait
 *
 * @package Modules\Core\Helpers
 */
trait PermissionTrait
{

	/**
	 * Ger permission
	 *
	 * @param string $action
	 * @param string $resource
	 * @param bool $api
	 *
	 * @return string
	 */
	public static function getPermission($action, $resource, $api = false)
	{
		$resource = $resource ? $resource . '.' : '';
		$prefix = $resource || $api ? ($api ? 'api.' : 'admin.') : '';

		if($api)
			return "{$prefix}{$resource}{$action}";

		return "{$prefix}{$resource}{$action}";
	}

	/**
	 * Get all permissions
	 *
	 * @param bool $api
	 *
	 * @return array
	 */
	public static function getAllPermissions( $api = false ) : array
	{
		return self::getPermissionsByRole( Role::PROFILE_ADMIN, $api ); // admin has all permissions by default
	}

	/**
	 * Get permissions by
	 *
	 * @param string $role
	 * @param bool $api
	 *
	 * @return array
	 */
	public static function getPermissionsByRole( string $role, $api = false ) : array
	{
		$permissions = array_key_exists( $role, self::ALL_PERMISSIONS ) ? self::ALL_PERMISSIONS[ $role ] : [ ];
		$output = [ ];

		foreach ($permissions as $action => $resources)
		{
			if(!is_array($resources) && $resources == self::NO_PREFIX)
			{
				$output[] = self::getPermission( $action, '', $api );
				continue;
			}

			foreach($resources as $resource)
			{
				$output[] = self::getPermission( $action, $resource, $api );
			}
		}

		return $output;
	}

	/**
	 * @param string $action
	 * @param string $resource
	 * @param bool $api
	 * @param User $user
	 *
	 * @return bool
	 */
	public static function canDo(string $action, string $resource, $api = false, $user = null) : bool
	{
		if(!$user)
			$user = \Auth::getUser();

		$permission = Permission::getPermission( $action, $resource, $api );
		return $user->can( $permission );
	}

}