<?php
namespace Modules\Core\Helpers
{

	use Carbon\Carbon;
	use DB;
	use Illuminate\Http\Request;
	use Modules\Core\Entities\Setting;
	use Modules\Core\Entities\Invoice;
	use Modules\Core\Entities\Package;
	use Modules\Core\Entities\Subscription;
	use Modules\Core\Entities\User;
	use Route;

	/**
	 * Class AppHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class AppHelper
	{
		const SMTP_ENCRYPTION_TYPE_SSL = 'SSL';
		const SMTP_ENCRYPTION_TYPE_TLS = 'TLS';
		const SMTP_ENCRYPTION_TYPE_STARTTLS = 'STARTTLS';
		const SMTP_ENCRYPTION_TYPE_NONE = null;

		const SMTP_ENCRYPTION_TYPES = [
			self::SMTP_ENCRYPTION_TYPE_SSL => self::SMTP_ENCRYPTION_TYPE_SSL,
			self::SMTP_ENCRYPTION_TYPE_TLS => self::SMTP_ENCRYPTION_TYPE_TLS,
			self::SMTP_ENCRYPTION_TYPE_STARTTLS => self::SMTP_ENCRYPTION_TYPE_STARTTLS,
			self::SMTP_ENCRYPTION_TYPE_NONE => self::SMTP_ENCRYPTION_TYPE_NONE,
		];

		/**
		 * Get current menu controller
		 *
		 * @param array ...$controllers
		 *
		 * @return string
		 */
		public static function menuActive(...$controllers)
		{
			try
			{
				$class_name = 'active';
				$current_controller = get_class(Route::getCurrentRoute()->getController());
				return in_array( $current_controller, $controllers )  ? $class_name : '';
			}
			catch (\Exception $ex)
			{
				return '';
			}
		}

		/**
		 * @param string $text
		 * @param string $class
		 * @param null $icon
		 *
		 * @return string
		 */
		public static function getBadge($text = 'texto', $class = 'bg-light-blue', $icon = null)
		{
			$output = "<button type='button' class='btn {$class} btn-block btn-xs waves-effect' style='box-shadow: none;'>";

			if($icon)
				$output .= "<i class='material-icons'>{$icon}</i>";

			$output .= "<span ".(!$icon ? 'style="top: 0;"' : '')." >{$text}</span></button>";
            return $output;
		}

		/**
		 * Get states
		 *
		 * @return array
		 */
		public static function getStates()
		{
			return DBHelper::getEnumValues( with(new User)->getTable(), 'state' );
		}

		/**
		 * Get the first pickup minimium date
		 *
		 * @param string $format
		 *
		 * @return string
		 */
		public static function getFirstPickupDate($format = 'd/m/Y')
		{
			return Carbon::today()->addDays( ConfigHelper::get('pickup_days_delay') )->format( $format );
		}

		/**
		 * Check if user filled all address
		 *
		 * @param $user_id
		 *
		 * @return bool
		 * @throws \Exception
		 */
		public static function userFilledAddress($user_id)
		{
			if(!($user = User::find($user_id)))
				throw new \Exception('Error: invalid user', 1);

			$addres_fields = [ 'address', 'number', 'district', 'zip_code', ];
			$has_address = true;

			foreach ( $addres_fields as $addres_field )
			{
				if(is_null($user->$addres_field) || strlen( trim($user->$addres_field)) == 0)
				{
					$has_address = !$has_address;
					break;
				}
			}

			return $has_address;
		}

		/**
		 * Change password (if needed)
		 *
		 * @param Request $request
		 * @param int $user_id
		 *
		 * @throws \Exception
		 */
		public static function changePassword(Request &$request, $user_id)
		{
			// -- if changing password
			if($request->filled('password_old') && $request->filled('password_new') && $request->filled('password_new2'))
			{
				// -- old password invalid
				if(($user = User::find($user_id)) && !\Hash::check($request->get('password_old'), $user->password))
					throw new \Exception('Desculpe, sua senha antiga não confere com os nossos registros', 1);

				// -- new passwords does not match
				else if($request->get('password_new') !== $request->get('password_new2'))
					throw new \Exception('Desculpe, sua confirmação de senha não coincide!', 1);

				// -- set password
				else
					$request->merge(['password' => $request->get('password_new')]);
			}
		}

		/**
		 * Get states
		 *
		 * @return array
		 */
		public static function getAddressesStates()
		{
			return DBHelper::getEnumValues( with(new User)->getTable(), 'state' );
		}

		/**
		 * Get logo ID
		 *
		 * @return id
		 */
		public static function getLogoId()
		{
			return Setting::find(1)->logo_media_id;
		}

		/**
		 * Get logo URL
		 *
		 * @return string
		 */
		public static function getLogoUrl()
		{
			return FileHelper::getUri( self::getLogoId() );
		}

		/**
		 * Get budget formatted id
		 *
		 * @param int $id
		 * @param int $pad_length
		 *
		 * @return string
		 */
		public static function getFriendlyId($id, $pad_length = 10)
		{
			return '#' . str_pad($id, $pad_length, "0", STR_PAD_LEFT);
		}

		/**
		 * Get user profile pic
		 *
		 * @param int $user_id
		 * @param int $width
		 * @param int $height
		 *
		 * @return string
		 */
		public static function getUserProfilePic($user_id,
			$width = FileHelper::THUMB_DEFAULT_WIDTH,
			$height = FileHelper::THUMB_DEFAULT_HEIGHT)
		{
			$user = User::find($user_id);
			return MediaHelper::getThumbUri($user->profile_image_id, $width, $height);
		}

		/**
		 * Get url
		 *
		 * @param array $url
		 * @param array $segments
		 *
		 * @return string
		 */
		public static function getAppendUrl($url, $segments = [ ])
		{
			$params = \Request::all();
			return route( $url, array_merge($params, $segments ) );
		}

		/**
		 * Remove all from CPF
		 *
		 * @param string $cpf
		 *
		 * @return null|string|string[]
		 */
		public static function onlyNumbers($cpf = '')
		{
			return preg_replace("/[^0-9]/", "", $cpf);
		}

		/**
		 * Mask
		 *
		 * Eg: $data = "10102010";
		 * echo mask($data,'##/##/####');
		 * echo mask($data,'[##][##][####]');
		 * echo mask($data,'(##)(##)(####)');
		 *
		 * @param $val
		 * @param $mask
		 *
		 * @return string
		 */
		public static function mask($val, $mask)
		{
			$maskared = '';
			$k = 0;
			for($i = 0; $i<=strlen($mask)-1; $i++)
			{
				if($mask[$i] == '#')
				{
					if(isset($val[$k]))
						$maskared .= $val[$k++];
				}
				else
				{
					if(isset($mask[$i]))
						$maskared .= $mask[$i];
				}
			}

			return $maskared;
		}

		/**
		 * @param string $model
		 *
		 * @return mixed
		 */
		public static function getTable($model)
		{
			return with(new $model())->getTable();
		}

	}
}