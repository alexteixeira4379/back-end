<?php
namespace Modules\Core\Helpers
{

    use Carbon\Carbon;
    use Modules\Core\Entities\Capital;
    use Modules\Core\Entities\ClosedCapital;

    /**
	 * Class CapitalHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class CapitalHelper
	{

        /**
         * @param Carbon $date
         * @return string
         */
        public static function getShareCapital($date = null)
        {

            if($date == null)
                $date = today();

            $month = $date->month;
            $year = $date->year;


            $shared_capital = ClosedCapital::whereYear('created_at', '=', $year)
                    ->whereMonth('created_at', '=',$month)->get()->sum('balance');

            return 'R$ '.number_format($shared_capital, 2, ',', '.');
        }

        /**
         * @return string
         */
        public static function getCurrentShareCapital()
        {

            return 'R$ '.number_format(Capital::all()->sum('balance'), 2, ',', '.');
        }




    }
}