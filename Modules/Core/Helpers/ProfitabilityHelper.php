<?php
namespace Modules\Core\Helpers
{

    use Carbon\Carbon;
    use Modules\Core\Entities\Expense;
    use Modules\Core\Entities\Profitability;
    use Modules\Core\Entities\Recipe;
    use Modules\Core\Entities\Role;
    use Modules\Core\Entities\Taxation;
    use Modules\Core\Events\ProfitabilityCreated;

    /**
	 * Class ResponseHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class ProfitabilityHelper
	{

	    protected $date;

	    private $gross_sum;
	    private $company_value;
	    private $partners_value;
	    private $current_capital;

	    private $profitability;
	    private $min_profitability;

	    public function __construct(Carbon $date)
        {
            $this->date = $date;

            $this->current_capital = Role::where('name','user')
            ->first()->users()->with('capital')->get()->pluck('capital')->sum('balance');

            $this->setValues();
        }


        private function setValues(){

	        $total = $this->getRecipeValue();
	        $total -= $this->getExpenseValue();
	        $total -= $this->getTaxationValue();

            $this->gross_sum = $total;
            $this->partners_value = $this->company_value = ($total/2);

            $this->profitability = $this->partners_value / $this->current_capital;
            $this->min_profitability = floatval(\ConfigHelper::get('min_profitability'));

        }


        /**
         * @return float
         */
        private function getRecipeValue(){

	        return  Recipe::whereYear('created_at',$this->date->year)
                    ->whereMonth('created_at', $this->date->month)->sum('value');
        }

        /**
         * @return float
         */
        private function getExpenseValue(){

            return  Expense::whereYear('created_at',$this->date->year)
                    ->whereMonth('created_at', $this->date->month)->sum('value');
        }

        /**
         * @return float
         */
        private function getTaxationValue(){

            return  Taxation::whereYear('created_at',$this->date->year)
                    ->whereMonth('created_at', $this->date->month)->sum('value');
        }

        /**
         * @return Profitability
         * @throws \Throwable
         */
        public function save(){

            $data = [
                'gross_sum' => $this->gross_sum,
                'company_value' => $this->company_value,
                'partners_value' => $this->partners_value,
                'profitability' => $this->profitability,
                'min_profitability' => $this->min_profitability,
            ];

            $profitability = null;

           \DB::transaction(function() use($data)
            {

              /** @var Profitability $profitability */
              $profitability = DBHelper::update(Profitability::class, $data);

                event( new ProfitabilityCreated($profitability));

            });

           return $profitability;

        }

        public static function getProfitabilityOnScale(Carbon $date = null){
            if($date == null)
                $date = Carbon::now();

           return Profitability::whereYear('created_at',$date->year)->orderBy('created_at', 'asc')->get();
        }

    }
}