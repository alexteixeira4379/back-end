<?php
namespace Modules\Core\Helpers
{

	use DB;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Support\Facades\Schema;

	/**
	 * Class DBHelper
	 *
	 * @package app\Helpers\HTML
	 */
	class DBHelper
	{
		/**
		 * The model
		 *
		 * @var Model $model
		 */
		public static $model;

		/**
		 * The data
		 *
		 * @var array $data
		 */
		public static $data = [ ];

		/**
		 * The ignored keys came from request or data
		 *
		 * @var array $ignored_keys
		 */
		public static $ignored_keys = [ '_token', '_method', 'created_at', 'updated_at' ];


		/**
		 * Do a request and return
		 *
		 * @param string $model
		 * @param array $data
		 *
		 * @return Model
		 * @throws \Exception
		 */
		public static function update($model, array $data)
		{
			try
			{
				/** @var $model Model */
				self::setParams($model, $data);

				if(array_key_exists( 'id', self::$data ))
				{
					$resource = $model::find(self::$data['id']);
					$resource->update(array_except( self::$data, 'id' ));
				}
				else
				{
					$resource = $model::create(self::$data);
				}
			}
			catch (\Exception $ex)
			{
				$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Desculpe, ocorreu um erro interno ao inserir/atualizar resource';
				throw new \Exception($msg, 1);
			}

			return $resource;
		}

		/**
		 * @param string $model
		 * @param array $data
		 */
		private static function setParams($model, array $data)
		{
			$ignored_keys = [ ];
			$table = with(new $model)->getTable();

			foreach ( $data as $column => $value )
			{
				if(!Schema::hasColumn($table, $column))
					$ignored_keys[] = $column;

				// -- hash password
				if($column == 'password')
					$data[$column] = \Hash::make( $value );
			}

			self::$data = array_except( $data, array_merge(self::$ignored_keys, $ignored_keys) );
			self::$model = $model;
		}

		/**
		 * Filter an Builder
		 *
		 * @param Builder $query
		 * @param string $operator
		 * @param array $allowed_fields
		 * @param array $filter
		 *
		 * @return Builder
		 */
		public static function filter(Builder $query, $operator = '=', array $allowed_fields = [ ], array $filter = [ ])
		{
			$data = !empty($filter) ? $filter :
				( \Request::filled('filter') && is_array(\Request::get('filter')) ? \Request::get('filter') : [ ]);

			foreach ( $data as $column => $value )
			{
				// -- if allowed filters
				if(in_array($column, $allowed_fields) && $value !== '-1')
				{
					$query->where($column, $operator, $value);
				}
			}

			return $query;
		}

		/**
		 * Sort an Builder
		 *
		 * @param Builder $query
		 * @param array $allowed_fields
		 * @param array $default_sorting
		 *
		 * @return Builder
		 */
		public static function sort(Builder $query, array $allowed_fields = [ ], array $default_sorting = [ ])
		{
			if(\Request::filled('sorting'))
			{
				foreach ( \Request::get('sorting') as $column => $type )
				{
					// -- if allowed filters
					if(in_array($column, $allowed_fields))
					{
						$query->orderBy($column, $type);
					}
				}
			}

			// -- default sorting
			else
			{
				foreach ( $default_sorting as $column => $type )
				{
					$query->orderBy($column, $type);
				}
			}

			return $query;
		}

		/**
		 * Get enum values
		 *
		 * @param string $table_name
		 * @param string $field_name
		 *
		 * @return array
		 */
		public static function getEnumValues($table_name, $field_name)
		{
			try
			{
				$type = DB::select(DB::raw('SHOW COLUMNS FROM '.$table_name.' WHERE Field = "'.$field_name.'"'))[0]->Type;
				preg_match('/^enum\((.*)\)$/', $type, $matches);
				$values = array();

				foreach(explode(',', $matches[1]) as $value)
					$values[trim($value, "'")] = trim($value, "'");

				return $values;
			}
			catch (\Exception $ex)
			{
				return [ ];
			}
		}
	}
}