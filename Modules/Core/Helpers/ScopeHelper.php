<?php

namespace Modules\Core\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class ScopeHelper
 *
 * @package Modules\Core\Helpers
 */
class ScopeHelper extends DBHelper
{
	/**
	 * The query
	 *
	 * @var $query Builder
	 */
	protected $query;

	/**
	 * The filters
	 *
	 * @var $filters Collection
	 */
	protected $filters;

	/**
	 * The datatabse date field name
	 *
	 * @var string
	 */
	protected $db_date_field = 'date';

	/**
	 * The filters fields coming from HTML
	 *
	 * @var array $date_filters
	 */
	protected $date_filters = [ 'date_from', 'date_to' ];


	/**
	 * ScopeHelper constructor.
	 *
	 * @return ScopeHelper
	 */
	public function __construct()
	{
		return $this;
	}

	/**
	 * Do filter
	 *
	 * @param \Closure $extra_filters
	 *
	 * @return Builder
	 */
	public function doFilter(\Closure $extra_filters = null)
	{
		if($this->getFilters()->only( $this->getDateFilters() )->count() == 2)
		{
			$date_from = $this->getFilters()->pull( $this->getFilterFrom() );
			$date_to = $this->getFilters()->pull( $this->getFilterTo() );

			$this->getQuery()->whereBetween( $this->getDbDateField(), [
				Carbon::createFromFormat('d/m/Y', $date_from)->format('Y-m-d 00:00:00'),
				Carbon::createFromFormat('d/m/Y', $date_to)->format('Y-m-d 23:59:59'),
			]);
		}

		foreach ($this->getFilters()->except( $this->getDateFilters() ) as $filter => $value)
		{
			// -- ignore some custom filters
			if(strpos($filter, 'custom_') !== false)
				continue;

			// -- run extra filters
			if(($extra_filters && ( $extra_filters( $this->getQuery(), $filter, $value ))) || is_null($value))
				continue;

			// -- get relations
			$relations = collect(explode('.', $filter));

			// -- has relations
			if($relations->isNotEmpty() && $relations->count() > 1)
			{
				$where_has = $relations->slice(0, ($relations->count()-1))->implode('.'); // eg: gas_station.district

				$this->getQuery()->with( $relations->first() )
				      ->whereHas( $where_has, function( Builder $query) use($relations, $value) {
					      $query->where($relations->last(), '=', $value);
				      });
			}

			// -- does not have relations
			else
			{
				$this->getQuery()->where($filter, '=', $value);
			}
		}

		return $this->getQuery();
	}

	/**
	 * @param Builder $query
	 *
	 * @return ScopeHelper
	 */
	public function setQuery( Builder $query )
	{
		$this->query = $query;
		return $this;
	}

	/**
	 * @return Builder
	 */
	public function getQuery() : Builder
	{
		return $this->query;
	}

	/**
	 * @return string
	 */
	public function getDbDateField(): string
	{
		return $this->db_date_field;
	}

	/**
	 * @param string $db_date_field
	 *
	 * @return ScopeHelper
	 */
	public function setDbDateField( string $db_date_field )
	{
		$this->db_date_field = $db_date_field;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getDateFilters(): array
	{
		return $this->date_filters;
	}

	/**
	 * @param array $date_filters
	 *
	 * @return ScopeHelper
	 */
	public function setDateFilters( array $date_filters = [ ] )
	{
		$this->date_filters = $date_filters;
		return $this;
	}

	/**
	 * Get the filters
	 *
	 * @return Collection
	 */
	public function getFilters(): Collection
	{
		return $this->filters;
	}

	/**
	 * @param array $filters
	 *
	 * @return ScopeHelper
	 */
	public function setFilters( array $filters = [ ] )
	{
		$this->filters = collect(array_filter( $filters, function($value) {
			return ($value !== null && $value !== false && $value !== '');
		}));

		return $this;
	}

	/**
	 * Get the FROM html filter name
	 *
	 * @return string
	 */
	private function getFilterFrom() : string
	{
		return array_first( $this->getDateFilters() );
	}

	/**
	 * Get the TO html filter name
	 *
	 * @return string
	 */
	private function getFilterTo() : string
	{
		return array_last( $this->getDateFilters() );
	}

}