<?php
namespace Modules\Core\Helpers
{
	use Intervention\Image\Facades\Image;

	/**
	 * Class MediaHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class MediaHelper
	{
		/**
		 * Set default thumbnail width
		 */
		const THUMB_DEFAULT_WIDTH = 270;

		/**
		 * Set default thumbnail height
		 */
		const THUMB_DEFAULT_HEIGHT = 263;


		/**
		 * Get a media thumbnail content
		 *
		 * @param $id
		 *
		 * @param int $width
		 * @param int|null $height
		 *
		 * @return mixed
		 */
		public static function getThumb( $id, $width = self::THUMB_DEFAULT_WIDTH, $height = self::THUMB_DEFAULT_HEIGHT )
		{
			if(($file_name = FileHelper::getFileName( $id )) && preg_match( '/^.*\.(jpg|jpeg|png|gif)$/i', $file_name ))
			{
				$file_path = FileHelper::getPath() . $file_name;

				if(file_exists( $file_path ))
				{
					/** @var  $img \Intervention\Image\Image */
					$img = Image::make( $file_path );

					// add callback functionality to retain maximal original image size
					$img->fit($width, $height, function ($constraint) {
						$constraint->upsize();
					});

					return $img->response( $img->extension );
				}
			}

			/** @var  $img \Intervention\Image\Image */
			$img = Image::make( public_path('modules/core/images/no-image.jpg') );

			return $img->response( $img->extension );
		}

		/**
		 * Get a media thumbnail uri
		 *
		 * @param $id
		 * @param int $width
		 * @param int $height
		 *
		 * @return string
		 */
		public static function getThumbUri($id, $width = self::THUMB_DEFAULT_WIDTH, $height = self::THUMB_DEFAULT_HEIGHT )
		{
			return route('medias.thumb', [
				'id' => $id,
				'width' => $width,
				'height' => $height,
			]);
		}

	}
}