<?php
namespace Modules\Core\Helpers
{

	use Illuminate\Database\Eloquent\Model;

	/**
	 * Class EventHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class EventHelper
	{

		/**
		 * @param Model $model
		 * @param string $field
		 *
		 * @return bool
		 */
		public function fieldAttributeExists(Model $model, $field = '')
		{
			return array_key_exists($field, $model['attributes']) && !is_null($model['attributes'][$field]);
		}

	}
}