<?php
namespace Modules\Core\Helpers
{

	use Illuminate\Contracts\Support\Arrayable;
	use Illuminate\Database\Eloquent\Collection;
	use Illuminate\Validation\Validator;

	/**
	 * Class AppHelper
	 *
	 * @package app\Helpers\HTML
	 */
	class RequestHelper
	{
		const FLASH_RESPONSE_SUCCESS = "success";
		const FLASH_RESPONSE_INFO = "info";
		const FLASH_RESPONSE_WARNING = "warning";
		const FLASH_RESPONSE_ERROR = "danger";

		const REDIRECT_SUCCESS = 'redirect_success';
		const REDIRECT_ERROR = 'redirect_error_url';
		const VALIDATOR = 'validator';
		const CUSTOM_MSG = 'custom-message';


		/**
		 * Store the redirect URL if exists
		 *
		 * @var null|string $redirect_success_url
		 */
		public static $redirect_success_url = null;

		/**
		 * Store the redirect URL if exists
		 *
		 * @var null|string $redirect_error_url
		 */
		public static $redirect_error_url = null;

		/**
		 * Store the validator instance (if exists)
		 *
		 * @var Validator $validator
		 */
		public static $validator = null;

		/**
		 * Store a custom message
		 *
		 * @var Validator $validator
		 */
		public static $custom_message = null;


		/**
		 * Do a request and return
		 *
		 * @param \Closure $closure
		 * @param array $options
		 *
		 * @return \Illuminate\Http\RedirectResponse|RequestHelper
		 */
		public static function doRequest(\Closure $closure, array $options = [ ])
		{
			self::setParams($options);

			try
			{
				if($validator = self::$validator)
				{
					if($validator->fails())
					{
						foreach (self::$validator->getMessageBag()->messages() as $field_name => $message)
						{
							// throw validator error
							throw new \Exception(collect($message)->first(), 1);
							break;
						}
					}
				}

				$closure();
			}
			catch (\Exception $ex)
			{
				$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Desculpe, ocorreu um erro interno';

				if(strpos($ex->getMessage(), "Integrity constraint violation") !== false)
					$msg = 'Desculpe, este recurso não pode ser alterado. Verifique se ele não está sendo utilzado por outro (?)';

				return ResponseHelper::getResponse(
					self::$redirect_error_url, $msg, 500, null, [ 'debug' => $ex->getMessage() ]
				);

			}

			return ResponseHelper::getResponse(
				\Session::has('from_url') ? \Session::pull('from_url') : self::$redirect_success_url,
				( self::$custom_message ? self::$custom_message : 'Ação concluída com sucesso!' ),
				200,
				null,
				[ 'redirect_to' => self::$redirect_success_url ? self::$redirect_success_url : null ]
			);

		}

		/**
		 * @param array $options
		 */
		private static function setParams( array $options = [ ] )
		{
			self::$validator = array_key_exists( self::VALIDATOR, $options ) ?
				$options[ self::VALIDATOR ] : null;

			self::$redirect_success_url = array_key_exists( self::REDIRECT_SUCCESS, $options ) ?
				$options[ self::REDIRECT_SUCCESS ] : null;

			self::$redirect_error_url = array_key_exists( self::REDIRECT_ERROR, $options ) ?
				$options[ self::REDIRECT_ERROR ] : null;

			self::$custom_message = array_key_exists( self::CUSTOM_MSG, $options ) ?
				$options[ self::CUSTOM_MSG ] : null;
		}

		/**
		 * Do a request and return api specific response
		 *
		 * @param \Closure $closure
		 * @return \Illuminate\Http\Response|RequestHelper
		 */
		public static function doApiRequest( \Closure $closure )
		{
			try
			{
				/** @var $data Collection */
				$data = $closure();

				return response()->json([
					'status' => 'success',
					'message' => 'Ação concluida com sucesso!',
					'data' => is_null($data) ? [ ] : ( $data instanceof Arrayable) ? $data->toArray() : [ ],
				]);
			}
			catch (\Exception $ex)
			{
				$msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

				return response()->json([
					'status' => 'error',
					'message' => $msg,
					'data' => $msg,
				], 500);
			}
		}

	}
}