<?php
namespace Modules\Core\Helpers
{

    use Illuminate\Support\Carbon;
    use Modules\Core\Entities\Capital;
    use Modules\Core\Entities\ClosedCapital;

    /**
	 * Class CapitalHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class DateHelper
	{

        /**
         * Customize the column
         *
         * @param Carbon $data
         *
         * @return string
         */
        static public function getColumnDateMonthYear( $data)
        {
            $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

            $datatime =  $data->format('m/Y');

            $datatime = explode('/',$datatime);

            $datatime[0] = $M[(int)$datatime[0]];

            return implode('/',$datatime);
        }


    }
}