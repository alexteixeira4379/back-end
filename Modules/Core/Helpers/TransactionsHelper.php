<?php
namespace Modules\Core\Helpers
{

    use Eloquent;
    use Modules\Core\Entities\Transaction;
    use Illuminate\Support\Collection;
    use \Carbon\Carbon;

    /**
	 * Class ResponseHelper
	 *
	 * @package Modules\Core\Helpers
	 */
	class TransactionsHelper
	{


        /**
         * @param \Eloquent $data
         * @return Collection
         */
        public static function getTransactionExtract($data = null)
        {

            $date_filter = today();

            if(\Request::filled('filters'))
            {
                $filters = new Collection(\Request::get('filters'));

                if($filters->get('date_filter') != null){
                    $date_filter = Carbon::createFromTimeString($filters->get('date_filter'));

                    $data = self::filterDate($data, 'Y-m-01 00:00:00', $date_filter,
                            (clone $date_filter)->addMonth());
                }

            }


            $extracts = [];

            for($i=$date_filter->daysInMonth; $i > 0 ; $i--) {

                $date = Carbon::createFromDate($date_filter->year, $date_filter->month, $i);

                $extract = new \stdClass();

                $extract->transactions =
                    self::filterDate( clone $data,'Y-m-d 00:00:00',$date,(clone $date)->addDay() );

                $extract->date = $date;

                $extracts[] = $extract;


            }

            return new Collection($extracts);

        }


        /**
         * Contribution Balance
         *
         * @param $value
         * @return float
         */
        public static function decreaseBalance(&$balance, $value)
        {
            $balance = floatval($balance);
            $value = floatval($value);

            $balance -= $value;

            return $balance;
        }

        /**
         * Withdrawal Balance
         *
         * @param $value
         * @return float
         */
        public static function increaseBalance(&$balance, $value)
        {
            $balance = floatval($balance);
            $value = floatval($value);

            $balance += $value;

            return $balance;
        }

        /**
         * Filter By Date
         *
         * @param Eloquent $query
         * @param $date_format
         * @param Carbon $start_date
         * @param Carbon $end_date
         * @return \Illuminate\Database\Eloquent\Builder
         */
        static public function filterDate($query, $date_format, $start_date, $end_date)
        {

            return $query->where('created_at','>=', $start_date->format($date_format))
                    ->where('created_at','<', $end_date->format($date_format));

        }

        /**
         * Customize the column
         *
         * @param $data
         *
         * @return string
         */
        static public function getColumnDate( $data )
        {
            $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

            $datatime = Carbon::parse($data->date->format('Y-m-d'))->format('d m Y');

            $datatime = explode(' ',$datatime);

            $datatime[1] = ' de ' . $M[(int)$datatime[1]] . ' de ';

            return implode(' ',$datatime);
        }


    }
}