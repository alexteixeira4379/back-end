<?php

namespace Modules\Core\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Modules\Core\Entities\Profitability;

class ProfitabilityCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $profitability;

	/**
	 * Create a new event instance.
	 *
	 * @param Profitability $profitability
	 */
    public function __construct(Profitability $profitability)
    {
        $this->profitability = $profitability;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('profitability-created');
    }

}
