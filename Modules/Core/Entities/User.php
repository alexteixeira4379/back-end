<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Gerardojbaez\Messenger\Contracts\MessageableInterface;
use Gerardojbaez\Messenger\Traits\Messageable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Core\Helpers\AppHelper;
use Modules\Core\Helpers\DBHelper;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Contribution;
use UserAccount;
use Modules\Core\Entities\Capital;
use Modules\Core\Entities\Withdrawal;

/**
 * Modules\Core\Entities\User
 *
 * @property int $id
 * @property string|null $email
 * @property string $password
 * @property string $active
 * @property string|null $remember_token
 * @property string $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Entities\UserAccount $account
 * @property-read \Modules\Core\Entities\Capital $capital
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\Contribution[] $contributions
 * @property string $birthday
 * @property string $cpf
 * @property-read string $path
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User filtered()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User withRole($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\User withRoles($roles = array())
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\Withdrawal[] $withdrawals
 */
class User extends Authenticatable implements MessageableInterface
{
    use Notifiable, EntrustUserTrait,Messageable;


	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function contributions()
    {
        return $this->hasMany(Contribution::class);
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class);
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function capital()
    {
        return $this->hasOne(Capital::class);
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function account()
    {
        return $this->hasOne(UserAccount::class);
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function bank_account()
    {
        return $this->hasOne(BankAccount::class);
    }

    /**
     * Relationship
     *
     * Get all of the transactions for the contributions.
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasManyThrough
     */
    public function contributions_transactions()
    {
        return $this->hasManyThrough('Transactions', 'Contributions','typeable_id')
            ->where('subscribable_type',Contribution::class);
    }

    /**
     * Relationship
     *
     * Get all of the transactions for the contributions.
     *
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_membership()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * Relationship
     *
     * Get all of the transactions for the contributions.
     *
     *  @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users_association()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Relationship
     *
     * Get all of the transactions for the withdrawals.
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasManyThrough
     */
    public function transactions()
    {
        return $this->hasManyThrough(
                Transaction::class,
               Contribution::class,
                null,
                'typeable_id');
    }

    /**
     * Relationship
     *
     * Get all of the indications
     *
     *  @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function indications()
    {
        return $this->hasMany(UserAccount::class,'indicated_user_id');
    }

    /**
     * Relationship
     *
     * Get indicated
     *
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicated()
    {
        return $this->belongsTo(UserAccount::class,'indicated_user_id');
    }

    /**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getBirthdayAttribute($value)
	{
		if($value)
			return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');

		return null;
	}

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getCodeSharedAttribute($value)
    {
        if($value)
            return $value;

        $token = \Illuminate\Support\Str::random(10);
        $check_token = false;
        while ($check_token == false){

            if(User::whereCodeShared($token)->get()->isEmpty())
            {
                $check_token = true;
            }
            else
            {
                $token = \Illuminate\Support\Str::random(10);
            }

        }

        $this->code_shared = $token;

        $this->save();

        return $token;
    }

    /**
     * Get the mutator
     *
     * @return mixed
     */
    public function getAllUsersMembersAttribute()
    {
        $users_association = $this->users_association;
        $user_membership = $this->user_membership()->get();

        return $users_association->merge($user_membership);
    }

    /**
     * Set the mutator
     *
     * @param  string  $value
     */
    public function setBirthdayAttribute($value)
    {
        if($value instanceof Carbon)
            return;

        $this->attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }


	/**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getCreatedAtAttribute($value)
	{
		if($value)
			return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');

		return null;
	}

	/**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getCpfAttribute($value)
	{
		if($value)
			return AppHelper::mask( $value, '###.###.###-##' );

		return null;
	}

    /**
	 * Set the mutator
	 *
	 * @param  string  $value
	 */
	public function setCpfAttribute($value)
	{
		$this->attributes['cpf'] = (string) "".AppHelper::onlyNumbers($value)."";
	}

	/**
	 * Set orders
	 *
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function scopeOrdered($query)
	{
		return DBHelper::sort($query, [
			'created_at',
		], [
			'created_at' => 'asc',
		]);
	}

	/**
	 * Get filters
	 *
	 * @param Builder $query
	 *
	 * @return Builder
	 */
	public function scopeFiltered($query)
	{
		return DBHelper::filter($query, '=', [  ]);
	}

	/**
	 * Get scope
	 *
	 * @param Builder $query
	 * @param array $roles
	 *
	 * @return Builder
	 */
	public function scopeWithRoles($query, array $roles = [ ])
	{
		if(!empty( $roles ))
		{
			$query->with('roles')->whereHas('roles', function(Builder $role) use($roles) {
				$role->whereIn('name', $roles);
			})->get()->toArray();
		}

		return $query;
	}

	/**
	 * Check if user can destroy
	 *
	 * @param int $id
	 *
	 * @return bool
	 */
	public static function canDestroy($id)
	{
		$allowed = true;

		// -- disallow destroy himself
		$allowed = !(\Auth::getUser()->id == $id);

		if(!$allowed)
			return false;

		if(!$allowed)
			return false;

		return $allowed;
	}

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getPathAttribute()
    {
        return 'users'.DIRECTORY_SEPARATOR.'user_'.$this->id;
    }

    /**
     * Manger Events
     */
    public static function boot()
    {
        parent::boot();

        /** Call function after create
         */

        self::created(function($user){
            /** @var User $user */
            Capital::create([ 'user_id' => $user->id,  'balance' => 0.0 ]);
        });

    }


}
