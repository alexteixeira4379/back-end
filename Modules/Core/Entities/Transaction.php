<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Entities\Transaction
 *
 * @property int $id
 * @property int $typeable_id
 * @property string $typeable_type
 * @property float $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $typeable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction whereTypeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction whereTypeableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Transaction whereValue($value)
 * @mixin \Eloquent
 */
class Transaction extends Model
{

    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'float',
    ];

    /**
     * Get all of the owning typeable models.
     */
    public function typeable()
    {
        return $this->morphTo();
    }

}
