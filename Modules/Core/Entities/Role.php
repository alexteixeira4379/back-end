<?php

namespace Modules\Core\Entities;

use Zizaco\Entrust\EntrustRole;


/**
 * Modules\Core\Entities\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\Permission[] $perms
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Role query()
 */
class Role extends EntrustRole
{
    const PROFILE_ADMIN = 'admin';
    const PROFILE_MANAGER = 'manager';

	const PROFILE_USER = 'user';

	public static $roles = [

        self::PROFILE_ADMIN => [
            'id'               => '1',
            'name'             => Role::PROFILE_ADMIN,
            'display_name'     => 'Super Administrador',
            'description'      => 'Administra todo o sistema',
        ],
		self::PROFILE_MANAGER => [
			'id'               => '2',
			'name'             => Role::PROFILE_MANAGER,
			'display_name'     => 'Administrador',
			'description'      => 'Gerencia o sistema',
		],

        self::PROFILE_USER => [
            'id'               => '3',
            'name'             => Role::PROFILE_USER,
            'display_name'     => 'Sócio',
            'description'      => '',
        ],

	];

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];


	/**
	 * Get the mutator
	 *
	 * @param string $value
	 *
	 * @return string
	 */
	public function getNameAttribute($value)
	{
		return ucwords( $value );
	}


    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(\User::class );
    }

}
