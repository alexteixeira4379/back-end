<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use User;

/**
 * Modules\Core\Entities\Capital
 *
 * @property-read string $value
 * @property-read \Modules\Core\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital whereValue($value)
 * @property float $balance
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Capital whereBalance($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\ClosedCapital[] $closed_capital
 * @property-read string $balance_to_real
 * @property-read mixed $last_closed_capital
 */
class Capital extends Model
{

    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id','user_id','created_at','updated_at'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'balance' => 'float',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['balance_to_real'];

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class );
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function closed_capital()
    {
        return $this->hasMany(ClosedCapital::class );
    }


    /**
     * Get the mutator
     *
     * @return string
     */
    public function getBalanceToRealAttribute()
    {
        $value = $this->balance;

        if($value)
            return 'R$ '.number_format($value, 2, ',', '.');

        return '---';
    }

    /**
     * Get last capital closed.
     *
     */
    public function getLastClosedCapitalAttribute()
    {
        return $this->closed_capital()->orderByDesc('created_at')->get()->first();

    }

    /**
     * Set the balance.
     *
     * @param  string  $value
     * @return void
     */
    public function setBalanceAttribute($value)
    {
        $this->attributes['balance'] = (float)strtolower($value);
    }


    /**
     * Get view of last capital
     *
     * @param null $date of month and year [m-Y]
     * @return array || string
     */
    public static function getLastCapital($date = null){

        $not_found = ['label' => "nemhum fechamento", "value" => "---"];

        if($date)
        {
            $date = explode('-', $date);
            $closed_capital = \Auth::getUser()->capital->closed_capital()
                ->whereYear('created_at', $date[1])
                ->whereMonth('created_at', $date[0])
                ->get();

            if(! $closed_capital->isEmpty() )
            {
                $closed_capital = $closed_capital->first();

                if(($date[1] == Carbon::now()->year && $date[0] == Carbon::now()->month) || !$closed_capital)
                    $closed_capital = \Auth::getUser()->capital->last_closed_capital->first();

                $data = [
                    'label' => 'Fechamento em ' . $closed_capital->date_closed,
                    'value' => $closed_capital->balance_to_real
                ];
            }
            else

                return $not_found;

        }
        else
        {

            if($closed_capital = \Auth::getUser()->capital->last_closed_capital)
            {
                $closed_capital = $closed_capital->first();
                $data = [
                    'label' => 'Fechamento em ' . $closed_capital->date_closed,
                    'value' => $closed_capital->balance_to_real
                ];
            }
            else
               return $not_found;

        }


        return $data;
    }




}
