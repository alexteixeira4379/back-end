<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Storage;
use User;
use File;

/**
 * Modules\Core\Entities\Contribution
 *
 * @property int $id
 * @property int $user_id
 * @property string $value
 * @property string $date
 * @property string $status
 * @property string $file_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $receipt_path
 * @property-read \Modules\Core\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereValue($value)
 * @mixin \Eloquent
 * @property string $type
 * @property-read string $date_format
 * @property-read string $file_link
 * @property-read string $friendly_type
 * @property-read \Modules\Core\Entities\Transaction $transaction
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Contribution whereType($value)
 */
class Contribution extends Model
{

    const CONTRIBUTION_PENDING      = '0';
    const CONTRIBUTION_SCHEDULED    = '1';
    const CONTRIBUTION_NOT_APPROVED = '2';
    const CONTRIBUTION_APPROVED     = '3';

    const CONTRIBUTION_STATUSES = [
        self::CONTRIBUTION_PENDING,
        self::CONTRIBUTION_APPROVED,
        self::CONTRIBUTION_SCHEDULED,
        self::CONTRIBUTION_NOT_APPROVED,
    ];

    // -- all package status
    const STATUS_LABELS = [
        self::CONTRIBUTION_PENDING      => "Pendente",
        self::CONTRIBUTION_APPROVED     => "Aprovado",
        self::CONTRIBUTION_SCHEDULED    => "Programado",
        self::CONTRIBUTION_NOT_APPROVED => "Não Aprovado",
    ];

    // -- Contribution types
    const CONTRIBUTION_TYPE_NORMAL      = '0';
    const CONTRIBUTION_TYPE_PROFITABILITY    = '1';

    const CONTRIBUTION_TYPE_STATUSES = [
        self::CONTRIBUTION_TYPE_NORMAL,
        self::CONTRIBUTION_TYPE_PROFITABILITY,
    ];


    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['file_link','date_format'];


    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class );
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function transaction()
    {
        return $this->morphOne(Transaction::class,'typeable' );
    }



    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        if($value)
            return 'R$ '.number_format($value, 2, ',', '.');

        return null;
    }


    /**
     * Get the mutator
     *
     * @param  string  $status
     * @return string
     */
    public function getStatusAttribute($status)
    {
        if($status || $status == self::CONTRIBUTION_PENDING)
            return self::STATUS_LABELS[$status];

        return null;
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFriendlyTypeAttribute()
    {

        if($this->type == self::CONTRIBUTION_TYPE_NORMAL)
            return null;
        else
            return '** Investimento automatico da rentabilidade';
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getReceiptPathAttribute()
    {
        return $this->user->path . DIRECTORY_SEPARATOR . 'contributions_receipt';
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFileLinkAttribute()
    {
        if(is_null($this->file_name) || empty($this->file_name))
            return null;
        else
            return Storage::url($this->receipt_path.DIRECTORY_SEPARATOR.$this->file_name);
    }


    /**
     * get dynamic accept fields
     *
     * @return array
     */
    static function acceptedFields(){

        if(\Auth::getUser()->hasRole(Role::PROFILE_USER))
            return ['id','user_id','value','file_name','date'];
        else
            return ['id','user_id','value','file_name','date','status'];

    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getDateFormatAttribute()
    {

        $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

        $datatime = \Carbon\Carbon::parse($this->created_at)->format('d m Y H:i:00');;

        $datatime = explode(' ',$datatime);
        $datatime[sizeof($datatime) - 1] = 'as ' . $datatime[sizeof($datatime) - 1];

        $datatime[1] = ' de ' . $M[(int)$datatime[1]] . ' de ';

        return implode(' ',$datatime);
    }



}
