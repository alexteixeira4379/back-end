<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;


/**
 * Modules\Core\Entities\FileCategory
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\File[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\FileCategory query()
 */
class FileCategory extends Model
{

	//const FILE_CATEGORY_TEST    = 'Teste (excel)';

	/**
	 * @var array $categories
	 */
	public static $categories = [
		//self::FILE_CATEGORY_TEST,
	];

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];


    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }

	/**
	 * Check if a cateogry matches
	 *
	 * @param $file_id
	 * @param $name
	 *
	 * @return bool
	 */
	public static function isCategoryOf($file_id, $name)
	{
		if($file = File::with('category')->find($file_id))
		{
			return strpos( $file->category->name, $name ) !== false;
		}

		return false;
	}

}