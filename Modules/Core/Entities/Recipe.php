<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Entities\Recipe
 *
 * @property int $id
 * @property string $assignee_assignor
 * @property string $kind
 * @property string $value
 * @property string $date
 * @property string|null $file_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $date_time_format
 * @property-read string $original_value
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereAssigneeAssignor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Recipe whereValue($value)
 * @mixin \Eloquent
 */
class Recipe extends Model
{

    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];


    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        if($value)
            return 'R$ '.number_format($value, 2, ',', '.');

        return null;
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getOriginalValueAttribute()
    {

        return $this->getOriginal('value');
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getDateTimeFormatAttribute()
    {

        $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

        $datatime = \Carbon\Carbon::parse($this->created_at)->format('d m Y H:i:00');;

        $datatime = explode(' ',$datatime);
        $datatime[sizeof($datatime) - 1] = 'as ' . $datatime[sizeof($datatime) - 1];

        $datatime[1] = ' de ' . $M[(int)$datatime[1]] . ' de ';

        return implode(' ',$datatime);
    }



}
