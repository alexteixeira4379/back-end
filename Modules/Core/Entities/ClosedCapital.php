<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Entities\ClosedCapital
 *
 * @property int $id
 * @property int $capital_id
 * @property float $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $balance_to_real
 * @property-read string $date_closed
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital whereCapitalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\ClosedCapital whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClosedCapital extends Model
{

    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'balance' => 'float',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'balance_to_real','date_closed ' ];

    /**
 * Get the mutator
 *
 * @return string
 */
    public function getBalanceToRealAttribute()
    {
        $value = $this->balance;

        if($value)
            return 'R$ '.number_format($value, 2, ',', '.');

        return null;
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getDateClosedAttribute()
    {
        return $this->created_at->format('d-m-Y');
    }


    /**
     * Set the balance.
     *
     * @param  string  $value
     * @return void
     */
    public function setBalanceAttribute($value)
    {
        $this->attributes['balance'] = (float)strtolower($value);
    }

}
