<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helpers\DateHelper;

/**
 * Modules\Core\Entities\Profitability
 *
 * @property int $id
 * @property float $gross_sum
 * @property float $company_value
 * @property float $partners_value
 * @property float $profitability
 * @property float $min_profitability
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $supplier_profitability
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereCompanyValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereGrossSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereMinProfitability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability wherePartnersValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereProfitability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Profitability whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Profitability extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getSupplierProfitabilityAttribute()
    {
        return $this->profitability;

    }

    /**
     * Get viw of prev profitability
     *
     * @param null $date
     * @return \Illuminate\Contracts\View\View
     */
    public static function getPrevProfitability($date = null){

        if($date)
        {
            $date = explode('-', $date);

            $label =  DateHelper::getColumnDateMonthYear(Carbon::createFromDate($date[1], $date[0])->startOfMonth());

            $capital = \Auth::getUser()->capital->closed_capital()
                ->whereYear('created_at', $date[1])
                ->whereMonth('created_at', $date[0])
                ->get()->first();

            $current_profitability = self::whereYear('created_at', $date[1])
                ->whereMonth('created_at', $date[0])
                ->get()->first();
        }

        else{
            $current_profitability = self::orderBy('created_at', 'desc');
            $capital = \Auth::getUser()->capital->last_closed_capital;
            if($current_profitability && $capital ){
                $current_profitability = $current_profitability->first();
                $capital = $capital->first();
                $label =  DateHelper::getColumnDateMonthYear(Carbon::now()->subMonth(1));
            }
        }

        if( !$current_profitability || !$capital)
        {
            $data = ['date_label' => 'Não Encontrado', 'profitability' => '--', 'about_my_capital' => '--', 'amount_received' => '--',];
        }
        else{
            $amount_received = $capital->balance * (floatval($current_profitability->profitability)/100);

            $about_my_capital = $amount_received +  $capital->balance;

            $data = [
                'date_label' =>  $label,
                'profitability' =>  $current_profitability->supplier_profitability,
                'about_my_capital' => 'R$ '.number_format($about_my_capital, 2, ',', '.'),
                'amount_received' => 'R$ '.number_format($amount_received, 2, ',', '.'),
            ];

        }

        return \View::make('admin::shared._prev_profitability')->with($data);
    }

}
