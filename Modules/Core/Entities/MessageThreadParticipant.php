<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gerardojbaez\Messenger\Contracts\MessageThreadParticipantInterface;

/**
 * Modules\Core\Entities\MessageThreadParticipant
 *
 * @property int $id
 * @property int $thread_id
 * @property int $user_id
 * @property string|null $title
 * @property string|null $last_read
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Gerardojbaez\Messenger\Models\MessageThread $thread
 * @property-read \Modules\Core\Entities\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Entities\MessageThreadParticipant onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant whereLastRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\MessageThreadParticipant whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Entities\MessageThreadParticipant withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Entities\MessageThreadParticipant withoutTrashed()
 * @mixin \Eloquent
 */
class MessageThreadParticipant extends Model implements MessageThreadParticipantInterface
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_id',
        'user_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    public $dates = ['deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * Get thread.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(config('messenger.models.thread'));
    }

    /**
     * Get user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
}
