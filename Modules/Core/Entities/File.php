<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Modules\Core\Entities\File
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $file_name
 * @property string $file_type
 * @property string $size
 * @property string $status
 * @property string $date_process
 * @property string $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read bool $is_sheet
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereDateProcess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereFileType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\File whereUserId($value)
 * @mixin \Eloquent
 */
class File extends Model
{
	const FILE_UNPROCESSED = '0';
	const FILE_PROCESSED = '1';
	const FILE_PROCESSING = '2';
	const FILE_SCHEDULED = '3';
	const FILE_ERROR = '4';

	const SHEET_FILES_EXTENSIONS = [ 'csv', 'xls', 'xlsx' ];

	const FILE_STATUSES = [
		self::FILE_UNPROCESSED,
		self::FILE_PROCESSED,
		self::FILE_PROCESSING,
		self::FILE_SCHEDULED,
		self::FILE_ERROR,
	];

	// -- all package status
	const STATUS_LABELS = [
		self::FILE_UNPROCESSED   => 'Não Processado',
		self::FILE_PROCESSED     => 'Processado',
		self::FILE_PROCESSING    => 'Processando',
		self::FILE_SCHEDULED     => 'Agendado',
		self::FILE_ERROR         => 'Erro',
	];

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];


	/**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getStatusAttribute($value)
	{
		if(!$this->is_sheet)
			return 'N/A';

		switch ($value)
		{
			case self::FILE_UNPROCESSED:
				return self::STATUS_LABELS[self::FILE_UNPROCESSED];
				break;

			case self::FILE_PROCESSING:
				return self::STATUS_LABELS[self::FILE_PROCESSING];
				break;

			case self::FILE_SCHEDULED:
				return self::STATUS_LABELS[self::FILE_SCHEDULED];
				break;

			case self::FILE_PROCESSED:
				return self::STATUS_LABELS[self::FILE_PROCESSED];
				break;

			case self::FILE_ERROR:
				return self::STATUS_LABELS[self::FILE_ERROR];
				break;

			default:
				return 'N/A';
				break;
		}
	}

	/**
	 * Set the mutator
	 *
	 * @param  string  $value
	 */
	public function setStatusAttribute($value)
	{
		$this->attributes['status'] = $value;
	}

	/**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getSizeAttribute($value)
	{
		return number_format( (int) $value / 1024, 2  ) . ' KB';
	}

	/**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getCreatedAtAttribute($value)
	{
		if($value)
			return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i:s');

		return null;
	}



	/**
	 * Get the mutator
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getDateProcessAttribute($value)
	{
		if($value)
			return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i:s');

		return null;
	}

	/**
	 * Get the mutator
	 *
	 * @return bool
	 */
	public function getIsSheetAttribute()
	{
		return in_array(\File::extension($this->file_name), self::SHEET_FILES_EXTENSIONS);
	}

}