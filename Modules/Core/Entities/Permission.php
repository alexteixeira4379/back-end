<?php

namespace Modules\Core\Entities;

use Modules\Core\Helpers\PermissionTrait;
use Zizaco\Entrust\EntrustPermission;


/**
 * Modules\Core\Entities\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Permission query()
 */
class Permission extends EntrustPermission
{
	use PermissionTrait;

	// -- no prefix
	const NO_PREFIX             = 'no-prefix';

	// -- actions
	const ACTION_LIST           = 'index';
	const ACTION_CREATE         = 'create';
	const ACTION_STORE          = 'store';
	const ACTION_SHOW           = 'show';
	const ACTION_EDIT           = 'edit';
	const ACTION_UPDATE         = 'update';
	const ACTION_DESTROY        = 'destroy';
	const ACTION_MODEL_DOWNLOAD = 'model_download';
	const ACTION_SHEET_PROCESS  = 'process';
	const ACTION_LOGIN          = 'login';
	const ACTION_LOGOUT         = 'logout';


	// -- resources (like in routes)
	const DASHBOARD             = 'dashboard';
	const SETTING               = 'settings';
	const USER                  = 'users';
	const USER_ACCOUNT          = 'accounts';
	const FILE                  = 'files';
	const FILE_CATEGORY         = 'file-categories';
	const TRANSACTION           = 'transaction-approve';
	const ROLE                  = 'roles';
	const PERMISSION            = 'permissions';
	const CONTRIBUTION          = 'contributions';
	const CAPITAL               = 'capital';
	const WITHDRAWAL            = 'withdrawals';
	const DEMONSTRATIVE         = 'demonstratives';
	const FINANCE               = 'finances';
	const RECIPE                = 'recipes';
	const EXPENSE               = 'expenses';
	const TAXATION              = 'taxations';
	const PROFITABILITY         = 'profitability';
	const MESSAGE               = 'message';


	// -- all permissions
	const ALL_PERMISSIONS = [

		// -- all admin permissions
		Role::PROFILE_ADMIN => [

			self::ACTION_LIST => [
				self::DASHBOARD,
				self::SETTING,
				self::USER,
				self::USER_ACCOUNT,
				self::FILE,
				self::FILE_CATEGORY,
				self::ROLE,
				self::PERMISSION,
				self::CONTRIBUTION,
				self::CAPITAL,
				self::WITHDRAWAL,
				self::DEMONSTRATIVE,
				self::FINANCE,
				self::RECIPE,
				self::TRANSACTION,
				self::EXPENSE,
				self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_CREATE => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_STORE => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_SHOW => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_EDIT => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_UPDATE => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_DESTROY => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_MODEL_DOWNLOAD => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_SHEET_PROCESS => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
			],

			self::ACTION_LOGIN => [self::USER],

			self::ACTION_LOGOUT => self::NO_PREFIX,
		],

		// -- all root permissions
        Role::PROFILE_MANAGER => [

            self::ACTION_LIST => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_CREATE => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_STORE => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_SHOW => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_EDIT => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_UPDATE => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_DESTROY => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_MODEL_DOWNLOAD => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_SHEET_PROCESS => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::FILE,
                self::FILE_CATEGORY,
                self::ROLE,
                self::PERMISSION,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::FINANCE,
                self::RECIPE,
                self::TRANSACTION,
                self::EXPENSE,
                self::TAXATION,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_LOGIN => [self::USER],

            self::ACTION_LOGOUT => self::NO_PREFIX,
        ],

        // -- all user permissions
        Role::PROFILE_USER => [

            self::ACTION_LIST => [
                self::DASHBOARD,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_CREATE => [
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::WITHDRAWAL,
            ],

            self::ACTION_STORE => [
                self::USER,
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::WITHDRAWAL,
            ],

            self::ACTION_SHOW => [
                self::DASHBOARD,
                self::SETTING,
                self::USER,
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::CAPITAL,
                self::WITHDRAWAL,
                self::DEMONSTRATIVE,
                self::PROFITABILITY,
                self::MESSAGE,
            ],

            self::ACTION_EDIT => [
                self::USER,
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::WITHDRAWAL,
            ],

            self::ACTION_UPDATE => [
                self::USER,
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::WITHDRAWAL,
            ],

            self::ACTION_DESTROY => [
                self::USER,
                self::USER_ACCOUNT,
            ],

            self::ACTION_MODEL_DOWNLOAD => [
                self::USER,
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::WITHDRAWAL,
            ],

            self::ACTION_SHEET_PROCESS => [
                self::USER,
                self::USER_ACCOUNT,
                self::CONTRIBUTION,
                self::WITHDRAWAL,
            ],

            self::ACTION_LOGIN => [self::USER],

            self::ACTION_LOGOUT => self::NO_PREFIX,
        ],


    ];

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

}
