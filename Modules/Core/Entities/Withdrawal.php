<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use User;

/**
 * Modules\Core\Entities\Withdrawal
 *
 * @property int $id
 * @property int $user_id
 * @property string $value
 * @property string $date
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Withdrawal whereValue($value)
 * @mixin \Eloquent
 */
class Withdrawal extends Model
{

    const WITHDRAWAL_PENDING      = '0';
    const WITHDRAWAL_SCHEDULED    = '1';
    const WITHDRAWAL_NOT_APPROVED = '2';
    const WITHDRAWAL_APPROVED     = '3';

    const WITHDRAWAL_STATUSES = [
        self::WITHDRAWAL_PENDING,
        self::WITHDRAWAL_APPROVED,
        self::WITHDRAWAL_SCHEDULED,
        self::WITHDRAWAL_NOT_APPROVED,
    ];

    // -- all package status
    const STATUS_LABELS = [
        self::WITHDRAWAL_PENDING      => "Pendente",
        self::WITHDRAWAL_APPROVED     => "Aprovado",
        self::WITHDRAWAL_SCHEDULED    => "Programado",
        self::WITHDRAWAL_NOT_APPROVED => "Não Aprovado",
    ];


    // -- Withdrawal types
    const WITHDRAWAL_TYPE_NORMAL      = '0';
    const WITHDRAWAL_TYPE_PROFITABILITY    = '1';

    const WITHDRAWAL_TYPE_STATUSES = [
        self::WITHDRAWAL_TYPE_NORMAL,
        self::WITHDRAWAL_TYPE_PROFITABILITY,
    ];



    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['file_link','date_format'];

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class );
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function transaction()
    {
        return $this->morphOne(Transaction::class,'typeable' );
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        if($value)
            return 'R$ '.number_format($value, 2, ',', '.');

        return null;
    }

    /**
     * Get the mutator
     *
     * @param  string  $status
     * @return string
     */
    public function getStatusAttribute($status)
    {
        if($status || $status == self::WITHDRAWAL_PENDING)
            return self::STATUS_LABELS[$status];

        return null;
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFriendlyTypeAttribute()
    {

        if($this->type == self::WITHDRAWAL_TYPE_NORMAL)
            return null;
        else
            return '** Resgate automatico da rentabilidade';
    }

    /**
     * get dynamic accept fields
     *
     * @return array
     */
    static function acceptedFields(){

        if(\Auth::getUser()->hasRole(Role::PROFILE_USER))
            return ['id','user_id','value','file_name','date'];
        else
            return ['id','user_id','value','file_name','date','status'];

    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getReceiptPathAttribute()
    {
        return $this->user->path . DIRECTORY_SEPARATOR . 'withdrawals_receipt';
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFileLinkAttribute()
    {
        if(is_null($this->file_name) || empty($this->file_name))
            return null;
        else
            return Storage::url($this->receipt_path.DIRECTORY_SEPARATOR.$this->file_name);
    }


    /**
     * Get the mutator
     *
     * @return string
     */
    public function getDateFormatAttribute()
    {

        $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

        $datatime = \Carbon\Carbon::parse($this->created_at)->format('d m Y H:i:00');;

        $datatime = explode(' ',$datatime);
        $datatime[sizeof($datatime) - 1] = 'as ' . $datatime[sizeof($datatime) - 1];

        $datatime[1] = ' de ' . $M[(int)$datatime[1]] . ' de ';

        return implode(' ',$datatime);
    }


}
