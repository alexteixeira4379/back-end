<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;


/**
 * Modules\Core\Entities\Setting
 *
 * @property int $id
 * @property string $site_title
 * @property string|null $footer_text
 * @property string|null $company_name
 * @property int|null $company_cnpj
 * @property string $smtp_host
 * @property string $smtp_username
 * @property string $smtp_password
 * @property int $smtp_port
 * @property string|null $smtp_encryption_type
 * @property string $primary_color
 * @property string $secondary_color
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereCompanyCnpj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereFooterText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting wherePrimaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSecondaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSiteTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSmtpEncryptionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSmtpHost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSmtpPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSmtpPort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereSmtpUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting query()
 * @property float $min_profitability
 * @property string $profitability_type
 * @property string $day_operation
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereDayOperation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereMinProfitability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Setting whereProfitabilityType($value)
 */
class Setting extends Model
{

    const PROFITABILITY_FIXED      = '0';
    const PROFITABILITY_DYNAMIC    = '1';

    const PROFITABILITY_STATUSES = [
        self::PROFITABILITY_FIXED,
        self::PROFITABILITY_DYNAMIC,
    ];

    // -- all package status
    const PROFITABILITY_STATUS_LABELS = [
        self::PROFITABILITY_FIXED      => "Fixa",
        self::PROFITABILITY_DYNAMIC    => "Dinâmica",
    ];


    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];



}
