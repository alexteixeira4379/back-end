<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use User;

/**
 * Modules\Core\Entities\BankAccount
 *
 * @property int $id
 * @property int $user_id
 * @property int $bank_id
 * @property string $document_number
 * @property string $owner_name
 * @property string $agency
 * @property string|null $agency_digit
 * @property string $account_number
 * @property string|null $account_digit
 * @property string $document_type
 * @property string $account_type
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 * @property-read \Modules\Core\Entities\Bank $bank
 * @property-read string $friendly_account_number
 * @property-read string $friendly_agency
 * @property-read string $friendly_name
 * @property-read string $owner_document_type
 * @property-read \Modules\Core\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereAccountDigit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereAccountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereAgency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereAgencyDigit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereBankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereOwnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\BankAccount whereUserId($value)
 * @mixin \Eloquent
 */
class BankAccount extends Model
{

    // -- account types
    const ACCOUNT_TYPE_CC  = 'cc';
    const ACCOUNT_TYPE_CP  = 'cp';

    const ACCOUNT_TYPES = [
        self::ACCOUNT_TYPE_CC,
        self::ACCOUNT_TYPE_CP,
    ];

    // -- all account types labels
    const ACCOUNT_TYPE_LABELS = [
        self::ACCOUNT_TYPE_CC  => 'C\ Corrente',
        self::ACCOUNT_TYPE_CP => 'C\ Poupança',
    ];


    // -- Account types
    const DOCUMENT_TYPE_CPF  = 'cpf';
    const DOCUMENT_TYPE_CNPJ  = 'cnpj';

    const DOCUMENT_TYPES = [
        self::DOCUMENT_TYPE_CPF,
        self::DOCUMENT_TYPE_CNPJ,
    ];

    // -- all package status
    const DOCUMENT_TYPE_LABELS = [
        self::DOCUMENT_TYPE_CPF  => 'CPF',
        self::DOCUMENT_TYPE_CNPJ => 'CNPJ',
    ];

    // -- ACTIVE
    const ACTIVE_NO     = '0';
    const ACTIVE_YES    = '1';

    // -- all OPTIONS
    const ACTIVE_OPTIONS = [
        self::ACTIVE_NO,
        self::ACTIVE_YES,
    ];

    // -- all labels
    const ACTIVE_LABELS = [
        self::ACTIVE_NO    => 'Não',
        self::ACTIVE_YES   => 'Sim',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'friendly_name','friendly_agency','friendly_account_number' ];


    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        if($value)
            return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');

        return null;
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        if($value)
            return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');

        return null;
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFriendlyNameAttribute()
    {
        $output = "Ag: {$this->agency} / ";
        $output .= mb_strtoupper( $this->getOriginal('account_type') ) . ": {$this->account_number}";
        $output .= " ({$this->bank->name})";

        return $output;
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFriendlyAgencyAttribute()
    {

        $output = "{$this->agency} ";
        if($this->agency_digit)
            $output .= "- {$this->agency_digit} ";

        return $output;
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFriendlyAccountNumberAttribute()
    {

        $output = "{$this->account_number} ";

        if($this->account_digit)
            $output .= "- {$this->account_digit} ";

        return $output;
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getOwnerDocumentTypeAttribute($value)
    {
        if(!$value)
            return 'N/A';

        return mb_strtoupper( self::DOCUMENT_TYPE_LABELS[ $value ] );
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getAccountTypeAttribute($value)
    {
        if(!$value)
            return 'N/A';

        return mb_strtoupper( self::ACCOUNT_TYPE_LABELS[ $value ] );
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getDocumentTypeAttribute($value)
    {
        if(!$value)
            return 'N/A';

        return mb_strtoupper( self::DOCUMENT_TYPE_LABELS[ $value ] );
    }


}
