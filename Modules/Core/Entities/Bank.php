<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;


/**
 * Modules\Core\Entities\Bank
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\SupplierBankAccount[] $bank_accounts
 * @property-read string $friendly_name
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Bank query()
 */
class Bank extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'friendly_name' ];


    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bank_accounts()
    {
        return $this->hasMany(BankAccount::class);
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getFriendlyNameAttribute()
    {
        return "{$this->code} - {$this->name}";
    }

}
