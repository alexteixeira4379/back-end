<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use User;

/**
 * Modules\Core\Entities\UserAccount
 *
 * @property int $id
 * @property int $user_id
 * @property string $person_type
 * @property string|null $full_name
 * @property string|null $cpf
 * @property string|null $rg
 * @property string|null $birthday
 * @property string|null $marital_status
 * @property string|null $nationality
 * @property string|null $profession
 * @property string|null $company_name
 * @property string|null $cnpj
 * @property string|null $telephone_1
 * @property string|null $telephone_2
 * @property string $address
 * @property string|null $address_2
 * @property int $number
 * @property string $zip_code
 * @property string $neighborhood
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $met_for
 * @property string $indicated_by
 * @property int $automatic_application
 * @property string $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $name
 * @property-read \Modules\Core\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereAutomaticApplication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereCnpj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereCpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereIndicatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereMaritalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereMetFor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereNeighborhood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount wherePersonType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereProfession($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereRg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereTelephone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereTelephone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\UserAccount whereZipCode($value)
 * @mixin \Eloquent
 */
class UserAccount extends Model
{

    const MET_FOR_INDICATION = 1;
    const MET_FOR_SEARCH_SITES = 2;
    const MET_FOR_INSTAGRAM = 3;
    const MET_FOR_BLOG = 4;

    const MET_FOR_STATUSES = [
        self::MET_FOR_INDICATION,
        self::MET_FOR_SEARCH_SITES,
        self::MET_FOR_INSTAGRAM,
        self::MET_FOR_BLOG,
    ];

    // -- all package status
    const MET_FOR_LABELS = [
        self::MET_FOR_INDICATION    => "Por Indicação",
        self::MET_FOR_SEARCH_SITES  => "Sites de Busca",
        self::MET_FOR_INSTAGRAM     => "Instagram",
        self::MET_FOR_BLOG          => "Blog",
    ];


    const PERSON_TYPE_PHYSICAL = 1;
    const PERSON_TYPE_JURISTIC = 2;

    const PERSON_TYPE_STATUSES = [
        self::PERSON_TYPE_PHYSICAL,
        self::PERSON_TYPE_JURISTIC,
    ];

    // -- all package status
    const PERSON_TYPE_LABELS = [
        self::PERSON_TYPE_PHYSICAL  => "Pessoa Física",
        self::PERSON_TYPE_JURISTIC  => "Pessoa Jurídica",
    ];


    const MARITAL_STATUS_NOT_MARRIED    = 1;
    const MARITAL_STATUS_MARRIED        = 2;
    const MARITAL_STATUS_DIVORCED       = 3;
    const MARITAL_STATUS_WIDOWER        = 4;

    const MARITAL_STATUS_STATUSES = [
        self::MARITAL_STATUS_NOT_MARRIED,
        self::MARITAL_STATUS_MARRIED,
        self::MARITAL_STATUS_DIVORCED,
        self::MARITAL_STATUS_WIDOWER,
    ];

    // -- all package status
    const MARITAL_STATUS_LABELS = [
        self::MARITAL_STATUS_NOT_MARRIED  => "Solteiro(a)",
        self::MARITAL_STATUS_MARRIED      => "Casado(a)",
        self::MARITAL_STATUS_DIVORCED     => "Divorciado(a)",
        self::MARITAL_STATUS_DIVORCED     => "Viúvo(a)",
    ];


    const STATES = [ 'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO',
        'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ',
        'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO', ];

    // -- package of status
    const STATES_LABELS = [
        'AC'=>'Acre',
        'AL'=>'Alagoas',
        'AP'=>'Amapá',
        'AM'=>'Amazonas',
        'BA'=>'Bahia',
        'CE'=>'Ceará',
        'DF'=>'Distrito Federal',
        'ES'=>'Espírito Santo',
        'GO'=>'Goiás',
        'MA'=>'Maranhão',
        'MT'=>'Mato Grosso',
        'MS'=>'Mato Grosso do Sul',
        'MG'=>'Minas Gerais',
        'PA'=>'Pará',
        'PB'=>'Paraíba',
        'PR'=>'Paraná',
        'PE'=>'Pernambuco',
        'PI'=>'Piauí',
        'RJ'=>'Rio de Janeiro',
        'RN'=>'Rio Grande do Norte',
        'RS'=>'Rio Grande do Sul',
        'RO'=>'Rondônia',
        'RR'=>'Roraima',
        'SC'=>'Santa Catarina',
        'SP'=>'São Paulo',
        'SE'=>'Sergipe',
        'TO'=>'Tocantins'
    ];



    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['name'];


    /**
     * Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class );
    }

    /**
     * Get the user's name.
     *
     * @return string
     */
    public function getNameAttribute()
    {

        if($this->person_type == self::PERSON_TYPE_PHYSICAL)
            return $this->full_name;
        if($this->person_type == self::PERSON_TYPE_JURISTIC)
            return $this->company_name;

        return null;
    }

    /**
     * Get the user's name.
     *
     * @return string
     */
    public function getDocumentAttribute()
    {

        if($this->person_type == self::PERSON_TYPE_PHYSICAL)
            return $this->cpf;
        if($this->person_type == self::PERSON_TYPE_JURISTIC)
            return $this->cnpj;

        return null;
    }


}
