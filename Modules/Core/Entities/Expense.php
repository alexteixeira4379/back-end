<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Entities\Expense
 *
 * @property int $id
 * @property string $addressee
 * @property string $kind
 * @property string $value
 * @property string $date
 * @property string|null $file_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $date_time_format
 * @property-read string $original_value
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereAddressee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Entities\Expense whereValue($value)
 * @mixin \Eloquent
 */
class Expense extends Model
{

    /**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [ ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];


    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        if($value)
            return 'R$ '.number_format($value, 2, ',', '.');

        return null;
    }

    /**
     * Get the mutator
     *
     * @param  string  $value
     * @return string
     */
    public function getOriginalValueAttribute()
    {

        return $this->getOriginal('value');
    }

    /**
     * Get the mutator
     *
     * @return string
     */
    public function getDateTimeFormatAttribute()
    {

        $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

        $datatime = \Carbon\Carbon::parse($this->created_at)->format('d m Y H:i:00');;

        $datatime = explode(' ',$datatime);
        $datatime[sizeof($datatime) - 1] = 'as ' . $datatime[sizeof($datatime) - 1];

        $datatime[1] = ' de ' . $M[(int)$datatime[1]] . ' de ';

        return implode(' ',$datatime);
    }

}
