<?php

namespace Modules\Core\Console;

use Carbon\Carbon;
use ConfigHelper;
use Illuminate\Console\Command;
use Modules\Core\Entities\Profitability;
use Modules\Core\Entities\User;
use Modules\Core\Helpers\ProfitabilityHelper;
use Modules\Core\Notifications\NewProfitability;
use Notification;

class CreateProfitability extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:createprofitability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new profitability of mouth';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle()
    {

        $date = Carbon::now();

        if ($date->day == ConfigHelper::get('day_operation') &&
            Profitability::whereMonth('created_at',$date->month)->get()->isEmpty())
        {
           $profitability = (new ProfitabilityHelper($date))->save();

            Notification::send(User::all(), new NewProfitability($profitability));
        }

    }


}
