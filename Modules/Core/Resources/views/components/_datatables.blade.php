<link href="{{ Module::asset('core:plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ Module::asset('core:plugins/jquery-datatable/extensions/responsive/responsive.bootstrap.min.css') }}" rel="stylesheet">

<script src="{{ Module::asset('core:plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>

<script src="{{ Module::asset('core:app/datatable/buttons.server-side.js') }}"></script>

<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/responsive/dataTables.responsive.min.js') }}"></script>
<script src="{{ Module::asset('core:plugins/jquery-datatable/extensions/responsive/responsive.bootstrap.min.js') }}"></script>

<link href="{{ Module::asset('core:app/datatable/datatables.css') }}" rel="stylesheet">
<script src="{{ Module::asset('core:app/datatable/datatables.js') }}"></script>


<script type="text/javascript">
    // -- translate
    $.extend( $.fn.dataTable.defaults, {
        language: {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "copyTitle": "Cópia",
                "copySuccess": "Copiadas %d linhas para a área de transfêrencia",

                "excel": "Excel",
                "export": "Exportar",
                "csv": "CSV",
                "pdf": "PDF",
                "print": "Imprimir",
                "reset": "Resetar",
                "reload": "Atualizar",
                "create": "Criar"
            }
        }
    });
</script>