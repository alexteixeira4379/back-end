<ul class="header-dropdown m-r--5 m-t--20">
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
           role="button" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">more_vert</i>
        </a>
        <ul class="dropdown-menu pull-right datatable-columns" style="width: 200px;">
            <!-- datatables columns will be filled here by researches.js -->
        </ul>
    </li>
</ul>