@if ($breadcrumbs)
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if (!$breadcrumb->last)
                <li>
                    @if($breadcrumb->url)
                        <a href="{{ $breadcrumb->url }}">
                            @if($breadcrumb->icon)
                                <i class="material-icons">{{ $breadcrumb->icon }}</i>
                            @endif
                            {{ $breadcrumb->title }}
                        </a>
                    @else
                        @if($breadcrumb->icon)
                            <i class="material-icons">{{ $breadcrumb->icon }}</i>
                        @endif
                        {{ $breadcrumb->title }}
                    @endif
                </li>
            @else
                <li class="active">
                    @if($breadcrumb->icon)
                        <i class="material-icons">{{ $breadcrumb->icon }}</i>
                    @endif

                     {{ $breadcrumb->title }}
                </li>
            @endif
        @endforeach
    </ol>
@endif