<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ ConfigHelper::get('site_title') }} | Login</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ Module::asset('core:plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ Module::asset('core:plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ Module::asset('core:plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ Module::asset('core:css/style.css') }}" rel="stylesheet" />
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo align-center">
        {{--<div class="logo">--}}
            {{--<a href="javascript:void(0);">Admin<b>PIC</b></a>--}}
            {{--<strong>Portal Invista em Consórcio</strong>--}}
        {{--</div>--}}
        <img src="{{ Module::asset('admin:images/logo.png') }}" style="width: 186px; margin-right: 32px;" />
    </div>
    <div class="card">
        <div class="body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif

            {!! Form::open(['route' => 'authenticate', 'class' => 'actExecutarFormAjax']) !!}

                <div class="msg">Efetue login para iniciar</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        {{ Form::email('email', null, [ 'class' => 'form-control', 'placeholder' => 'E-mail' ]) }}
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        {{ Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Senha' ]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 p-t-5">
                        {{ Form::checkbox('remember', null, false, [ 'id' => 'remember', 'class' => 'filled-in chk-col-pink' ]) }}
                        <label for="remember">Lembrar</label>
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-sm bg-blue-grey waves-effect" style="background-color: #351726 !important;" type="submit">
                            <i class="material-icons font-13">lock_outline</i>
                            <span class="font-13">ENTRAR</span>
                        </button>
                    </div>
                </div>

                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-6">
                        Não tem uma conta ainda? <a href="{{ route('admin.accounts.create') }}">Cadastre-se!</a>
                    </div>
                    <div class="col-xs-6 align-right">
                        <a href="forgot-password.html">Esqueceu sua senha?</a>
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{ Module::asset('core:plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ Module::asset('core:plugins/bootstrap/js/bootstrap.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ Module::asset('core:plugins/node-waves/waves.js') }}"></script>

<!-- Validation Plugin Js -->
<script src="{{ Module::asset('core:plugins/jquery-validation/jquery.validate.js') }}"></script>

<!-- JQUERY: Plugin MASK -->
<script src="{{ Module::asset('core:plugins/jquery.mask/jquery.mask.min.js') }}" type="text/javascript" ></script>

<!-- Custom Js -->
<script src="{{ Module::asset('core:js/admin.js') }}"></script>
<script src="{{ Module::asset('core:js/pages/examples/sign-in.js') }}"></script>

</body>
</html>