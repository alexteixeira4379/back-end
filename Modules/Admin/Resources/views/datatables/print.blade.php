<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Fechamento</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <style>
            body {margin: 30px}
            .upn{background-image:url("{{ Module::asset('admin:images/logo.png') }}"); background-repeat:no-repeat; width:700px; height:342px; position:absolute;}

        </style>
    </head>
    <body>

        <table style="width: 100%;margin-bottom: 80px; border-bottom: 3px solid {!!  ConfigHelper::get('primary_color') !!}">
            <tr>
                <td width="18%">
                    <img src="{{asset('modules/admin/images/logo.png') }}" width="210px" height="130px;">
                </td>
                <td width="80%">
                    <p style="font-size: 16px"><strong>Extrato de movimentações de {{ array_first($data[0]) }}</strong>
                    <p style="font-size: 16px"><strong>{{ \Auth::getUser()->account->name . ' - CPF/CNPJ: ' . \Auth::getUser()->account->document  }}</strong>
                    <p style="font-size: 16px"><strong>Data da operação: {{ now()->format('d/m/Y h\hi')  }}</strong>
                </td>
            </tr>
        </table>



        <table class="table table-bordered table-condensed table-striped">
            @foreach($data as $row)
                @if ($row == reset($data)) 
                    <tr>
                        @foreach($row as $key => $value)
                            <th>{!! $key !!}</th>
                        @endforeach
                    </tr>
                @endif
                <tr>
                    @foreach($row as $key => $value)
                        @if(is_string($value) || is_numeric($value))
                            <td>{!! $value !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </table>
    </body>
</html>
