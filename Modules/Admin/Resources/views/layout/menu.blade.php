<div class="menu">
    <ul class="list">
        <li class="header">NAVEGAÇÃO PRINCIPAL</li>
        <li class="active" style="display: none;"></li>


        <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\DashboardController::class) }}">
            <a href="{{ route('admin.dashboard.index') }}">
                <i class="material-icons">home</i>
                <span>HOME</span>
            </a>
        </li>

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::CAPITAL) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\CapitalController::class) }}">
                <a href="{{ route('admin.capital.index') }}">
                    <i class="material-icons">account_balance</i>
                    <span>Capital (Extrato)</span>
                </a>
            </li>
        @endif

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::DEMONSTRATIVE) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\DemonstrativeController::class) }}">
                <a href="{{ route('admin.demonstratives.index') }}">
                    <i class="material-icons">insert_chart</i>
                    <span>Resultados</span>
                </a>
            </li>
        @endif



        @if( Permission::canDo( Permission::ACTION_LIST, Permission::TRANSACTION) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\TransactionApproveController::class) }}">
                <a href="{{ route('admin.transaction-approve.index') }}">
                    <i class="material-icons">done</i>
                    <span>Aprovar Transações</span>
                </a>
            </li>
        @endif

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::CONTRIBUTION) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\ContributionController::class) }}">
                <a href="{{ route('admin.contributions.index') }}">
                    <i class="material-icons">add</i>
                    <span>Aportes (Aumento de capital)</span>
                </a>
            </li>
        @endif

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::CONTRIBUTION) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\WithdrawalController::class) }}">
                <a href="{{ route('admin.withdrawals.index') }}">
                    <i class="material-icons">remove</i>
                    <span>Resgates</span>
                </a>
            </li>
        @endif

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::FINANCE) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\FinanceController::class) }}">
                <a href="{{ route('admin.finances.index') }}">
                    <i class="material-icons">attach_money</i>
                    <span>Financeiro</span>
                </a>
            </li>
        @endif

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::MESSAGE) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\MessageController::class) }}">
                <a href="{{ route('admin.message.index') }}">
                    <i class="material-icons">message</i>
                    <span>Mensagens</span>
                </a>
            </li>
        @endif

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::USER) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\UserController::class) }}">
                <a href="{{ route('admin.users.index') }}">
                    <i class="material-icons">accessibility</i>
                    <span>Usuários</span>
                </a>
            </li>
        @endif

        {{--@if( Permission::canDo( Permission::ACTION_LIST, Permission::FILE) )--}}
            {{--<li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\FileController::class) }}">--}}
                {{--<a href="{{ route('admin.files.index') }}">--}}
                    {{--<i class="material-icons">attach_file</i>--}}
                    {{--<span>Arquivos</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--@endif--}}

        @if( Permission::canDo( Permission::ACTION_LIST, Permission::SETTING) )
            <li class="{{ AppHelper::menuActive(\Modules\Admin\Http\Controllers\SettingController::class) }}">
                <a href="{{ route('admin.settings.index') }}">
                    <i class="material-icons">settings</i>
                    <span>Configurações</span>
                </a>
            </li>
        @endif

        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                <i class="material-icons">picture_as_pdf</i>
                <span>Contratos</span>
            </a>
            <ul class="ml-menu" style="display: none;">
                <li>
                    <a href="{!! \Module::asset('admin:pdfs/contrato.pdf') !!}" target="_blank" class=" waves-effect waves-block">Contrato 1</a>
                </li>
                <li>
                    <a href="{!! \Module::asset('admin:pdfs/contrato.pdf') !!}" target="_blank" class=" waves-effect waves-block">Contrato 2</a>
                </li>
                <li>
                    <a href="{!! \Module::asset('admin:pdfs/contrato.pdf') !!}" target="_blank" class=" waves-effect waves-block">Contrato 3</a>
                </li>
            </ul>
        </li>
    </ul>
</div>