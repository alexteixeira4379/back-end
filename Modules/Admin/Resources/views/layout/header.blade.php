<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>{{ ConfigHelper::get('site_title') }} - @yield('page_title')</title>
  <link rel="icon" href="{{ Module::asset('admin:images/logo.ico') }}">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  <link href="{{ Module::asset('core:plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

  <!-- Waves Effect Css -->
  <link href="{{ Module::asset('core:plugins/node-waves/waves.css') }}" rel="stylesheet">

  <!-- Animation Css -->
  <link href="{{ Module::asset('core:plugins/animate-css/animate.css') }}" rel="stylesheet">

  <!-- Sweetalert Css -->
  <link href="{{ Module::asset('core:plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

  <!-- Light Gallery Plugin Css -->
  <link href="{{ Module::asset('core:plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">

  <!-- Multi Select Css -->
  <link href="{{ Module::asset('core:plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

  <!-- Bootstrap Spinner Css -->
{{--  <link href="{{ Module::asset('core:plugins/query-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet">--}}

  <!-- Bootstrap Tagsinput Css -->
  <link href="{{ Module::asset('core:plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

  <!-- Bootstrap select -->
  <link href="{{ Module::asset('core:plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet">

  <!-- Bootstrap select - Ajax extension -->
  <link href="{{ Module::asset('core:plugins/bootstrap-select-ajax/css/ajax-bootstrap-select.min.css') }}" rel="stylesheet">

  <!-- Bootstrap Material Datetime Picker Css -->
  <link href="{{ Module::asset('core:plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

  <!-- noUISlider Css -->
  <link href="{{ Module::asset('core:plugins/nouislider/nouislider.min.css') }}" rel="stylesheet">

  <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
  <link href="{{ Module::asset('core:css/themes/all-themes.css') }}" rel="stylesheet">

  <!-- Custom Css -->
  <link href="{{ Module::asset('core:css/style.css') }}" rel="stylesheet">

  <!-- General Css -->
  <link href="{{ Module::asset('core:app/general.css') }}" rel="stylesheet">

  <!-- General Css -->
  <link href="{{ Module::asset('admin:app/general.css') }}" rel="stylesheet">


  @stack('css')

  {{-- CUSTOM COLORS --}}
  @include('admin::layout.colors')
  <style>
    .sidebar .menu .list .header{
        color: #c3bec9;
        padding: 18px 16px !important;
        background: {!!  ConfigHelper::get('secondary_color') !!} !important;
    }

    .theme-blue-grey .sidebar .menu .list li.active > :first-child i, .theme-blue-grey .sidebar .menu .list li.active > :first-child span{
      color: {!!  ConfigHelper::get('secondary_color') !!} !important;
      font-weight: bold !important;
    }
  </style>

</head>

<body class="theme-blue-grey {{ isset($extra_class) ? $extra_class : '' }}">