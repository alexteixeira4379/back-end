@include('admin::layout.header')

<body background="{{  Module::asset('admin:images/new-account-business.jpg')  }}">

    {{--<!-- Top Bar -->--}}
    {{--<nav class="navbar">--}}
        {{--<div class="container-fluid">--}}

            {{--<a class="page-title" href="{{ route('admin.dashboard.index') }}">--}}
                {{-- <img src="{{ Module::asset('admin:images/logo1.png') }}" height="60px;" />--}}
                {{--<span style="font-size: 36px; padding-top: 10px;">--}}
                {{--{{ ConfigHelper::get('site_title') }}--}}
                {{--<small>| @yield('page_title')</small>--}}
                {{--</span>--}}
            {{--</a>--}}

            {{--<div class="navbar-header">--}}
                {{--<a href="javascript:void(0);" class="bars"></a>--}}
                {{--<a class="navbar-brand" href="javascript:void(0);">ADMIN - Portal Invista em Consórcio</a>--}}
            {{--</div>--}}

            {{--<div class="collapse navbar-collapse" id="navbar-collapse">--}}
                {{--<ul class="nav navbar-nav navbar-right">--}}

                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</nav>--}}
    {{--<!-- #Top Bar -->--}}



    <section class="content">
        <div class="container-fluid">
            {{--<div class="block-header">--}}
            <div>
                <div id="app">

                    @yield('content')
                </div>

            </div>
        </div>
    </section>

    @include('admin::layout.footer')

</body>
