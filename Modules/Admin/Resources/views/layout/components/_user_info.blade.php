<ul class="dropdown-menu">
    <li class="header"></li>
    <li class="body">
        <div class="profile-card">
            <div class="profile-header" style=" height: 0; padding: 33px; ">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="https://gurayyarar.github.io/AdminBSBMaterialDesign/images/user-lg.jpg" id="display_profile_image"
                         alt="AdminBSB - Profile Image" style="width: 79px;height: 79px;padding: 0;margin: 17px;" />
                </div>
                <div class="content-area">
                    <h4>{{  Auth::getUser()->account->full_name }}</h4>
                    @if(Auth::getUser()->account->profession)
                        <p>{{  Auth::getUser()->account->profession }}</p>
                    @endif
                    <p>{{ Auth::getUser()->roles()->first()->display_name }}</p>
                </div>
                <div class="content-area">
                    <p style="margin: 3px 9px 0px 17px; text-align: left"><strong>Mudar de conta</strong></p>

                    @foreach(Auth::getUser()->all_users_members as $user_association)

                    <div class="image-area" style="margin: 0;display: flex;padding: 11px;">
                        <img src="https://gurayyarar.github.io/AdminBSBMaterialDesign/images/user-lg.jpg"
                             id="display_profile_image" alt="AdminBSB - Profile Image" style="/* width: 79px; */height: 39px;padding: 0;margin: 0;border-radius: 48px;">

                        <a style="align-self: center; padding: 0 9px; margin: 0;color: initial" href="{{route('admin.users.login',$user_association->id)}}">{{ $user_association->account->name  }}</a>
                    </div>

                    @endforeach

                </div>
            </div>

            <div class="profile-footer align-center">
                <p><a  class="" href="{{ route('admin.users.edit', \Auth::getUser()->id) }}"><strong>Editar Perfil</strong></a></p>
                <p><a href="{{ route('logout') }}"><strong>Sair da conta</strong></a></p>
            </div>
        </div>
    </li>
    {{--<li class="footer">--}}
       {{----}}
    {{--</li>--}}

</ul>



