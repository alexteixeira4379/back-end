<ul class="dropdown-menu">
    <li class="header">Notificações</li>
    <li class="body">

        <ul class="menu">

            @php
                $unreadNotifications = \Auth::getUser()->unreadNotifications;
                $notifications = \Auth::getUser()->notifications->where('created_at','>=',now()->format('Y-m-01 00:00:00'));

                $notifications = $unreadNotifications->isEmpty() ? $notifications: $unreadNotifications;

            @endphp

            @foreach($notifications as $notification)

                @switch($notification->type)
                    @case(\Modules\Core\Notifications\NewProfitability::class)
                        @include('admin::notifications.new-profitability')
                    @break

                    @case(\Modules\Core\Notifications\ContributionApproved::class)
                        @include('admin::notifications.contribution-approved')
                    @break

                    @case(\Modules\Core\Notifications\WithdrawalApproved::class)
                    @include('admin::notifications.withdrawal-approved')

                    @case(\Modules\Core\Notifications\ProfitApplied::class)
                        @include('admin::notifications.profit-applied')

                    @case(\Modules\Core\Notifications\ProfitRescued::class)
                        @include('admin::notifications.profit-rescued')
                    @break
                @endswitch

            @endforeach
        </ul>

    </li>
</ul>
