<!-- Jquery Core Js -->
<script src="{{ Module::asset('core:plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ Module::asset('core:plugins/bootstrap/js/bootstrap.js') }}"></script>

<!-- Bootstrap select -->
<script src="{{ Module::asset('core:plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

<!-- Bootstrap select - Ajax extension -->
<script src="{{ Module::asset('core:plugins/bootstrap-select-ajax/js/ajax-bootstrap-select.min.js') }}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ Module::asset('core:plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ Module::asset('core:plugins/node-waves/waves.js') }}"></script>

<!-- Light Gallery Plugin Js -->
<script src="{{ Module::asset('core:plugins/light-gallery/js/lightgallery-all.js') }}"></script>

<!-- Custom Js -->
<script src="{{ Module::asset('core:js/admin.js') }}"></script>

<!-- Demo Js -->
<script src="{{ Module::asset('core:js/demo.js') }}"></script>

<!-- Sweet Alerts -->
<script src="{{ Module::asset('core:plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Popovers -->
<script src="{{ Module::asset('core:js/pages/ui/tooltips-popovers.js') }}"></script>

<!-- JQUERY: Plugin MASK -->
<script src="{{ Module::asset('core:plugins/jquery.mask/jquery.mask.min.js') }}" type="text/javascript" ></script>

<!-- Input Mask Plugin Js -->
<script src="{{ Module::asset('core:plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>

<!-- Moment Plugin Js -->
<script src="{{ Module::asset('core:plugins/momentjs/moment.js') }}" type="text/javascript" ></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ Module::asset('core:plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<!-- Count To -->
<script src="{{ Module::asset('core:plugins/jquery-countto/jquery.countTo.js') }}"></script>

<!-- Lodash JS -->
<script src="{{ Module::asset('core:plugins/lodash/lodash.min.js') }}"></script>

<!-- Core -->
<script src="{{ Module::asset('core:app/general.js') }}"></script>

<!-- Admin -->
<script src="{{ Module::asset('admin:app/general.js') }}"></script>


@if(App::environment('local') || App::environment('development'))
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
@elseif(App::environment('production'))
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.min.js"></script>
@endif

{{--<script src="{{ Module::asset('core:app/vue/app.js') }}"></script>--}}


@stack('scripts')

</body>

</html>