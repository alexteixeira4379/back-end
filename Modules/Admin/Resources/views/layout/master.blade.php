@include('admin::layout.header')
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Processando...</p>
    </div>
</div>
<!-- #END# Page Loader -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Search Bar -->
{{--@include('admin::layout.searchbar')--}}
<!-- #END# Search Bar -->

<!-- Top Bar -->
<nav class="navbar"
     style="background-color: {!!  ConfigHelper::get('primary_color') !!}">
    <div class="container-fluid">

        {{--<a class="page-title" href="{{ route('admin.dashboard.index') }}">--}}
             {{--<img src="{{ Module::asset('admin:images/logo.png') }}" height="60px;" />--}}
        {{--</a>--}}

        <div class="navbar-header" style="padding: 0" >
            <a href="javascript:void(0);" class="bars"></a>
            <img class="visible-lg" src="{{ Module::asset('admin:images/logo.png') }}" height="74px;" width="108px" />
            <img class="visible-xs visible-sm visible-md" src="{{ Module::asset('admin:images/logo.png') }}"
                 height="74px;"
                 width="108px"
                 style="margin-left: 37px"
            />
{{--            <img src="{{ Module::asset('admin:images/logo.png') }}" height="60px;" />--}}
            {{--<a class="navbar-brand" href="javascript:void(0);"><strong>ADMIN || </strong><span style="color: #b38b4b">INVESTE Consórcio</span></a>--}}
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">

                <!-- Call Search -->
            {{--<li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>--}}
            <!-- #END# Call Search -->

            <!-- USER - SHARE -->
            <li class="dropdown m-l--20" style=" margin-right: 15px; ">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                    <i class="material-icons">share</i>
                </a>
                <ul class="dropdown-menu">
                    <li class="header col-blue-grey">Link de compartilhamento</li>
                    <li class="footer">
                        <a href="javascript:void(0);" class="col-light-blue"
                           onclick="javascript:  window.prompt('Aperte ok para copiar o link abaixo:', '{{route('admin.user.indicate_link',Auth::getUser()->code_shared)}}');">

                            Https://.../{{Auth::getUser()->code_shared}}
                            <i class="material-icons" style="float: right">content_copy</i>
                        </a>
                    </li>
                </ul>

            </li>


            <!-- USER - INFO -->
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle  waves-effect" data-toggle="dropdown" role="button" style=" padding: 0; ">
                    <div class="image-area" style=" display: flex; ">
                        <img src="https://gurayyarar.github.io/AdminBSBMaterialDesign/images/user-lg.jpg" id="display_profile_image"
                             alt="AdminBSB - Profile Image" style="/* width: 79px; */height: 39px;padding: 0;margin: 0;border-radius: 48px;" />

                        <p style="align-self: center; padding: 0 9px; margin: 0;">{{  Auth::getUser()->account->full_name }}</p>
                    </div>
                </a>
                @include('admin::layout.components._user_info')
            </li>



            <!-- Notifications -->
            <li class="dropdown">
                <a href="{{ route('admin.notifiable.mark_as_read') }}" id="notifiable" class="dropdown-toggle  waves-effect" data-toggle="dropdown" role="button">
                    <i class="material-icons">notifications</i>
                    <span class="label-count">{{ \Auth::getUser()->unreadNotifications()->count()  }}</span>
                </a>
                @include('admin::layout.components._notifications')
            </li>
            <!-- #END# Notifications -->

            <!-- Messages -->
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                    <i class="material-icons">mail</i>
                    <span class="label-count">3</span>
                </a>
                @include('admin::layout.components._messages')
            </li>
            <!-- #END# Messages -->


            {{--<!-- Tasks -->--}}
            {{--<li class="dropdown">--}}
                {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">--}}
                    {{--<i class="material-icons">flag</i>--}}
                    {{--<span class="label-count">9</span>--}}
                {{--</a>--}}
                {{--@include('admin::layout.components._tasks')--}}
            {{--</li>--}}
            {{--<!-- #END# Tasks -->--}}



                {{--<li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true">--}}
                {{--<i class="material-icons">more_vert</i></a>--}}
                {{--</li>--}}

            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->

<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">

        <!-- User Info -->
        {{--<div class="user-info">--}}
            {{--<div class="gas-station"></div>--}}
            {{--<div style="width: 48px; height: 48px;"></div>--}}
            {{--<div class="image">--}}
            {{--<img src="{{ Module::asset('customer:images/user.png') }}" width="48" height="48" alt="User" />--}}
            {{--</div>--}}
            {{--<div class="info-container">--}}
                {{--<div class="name col-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--{{ Auth::getUser()->account->full_name }} ({{ Auth::getUser()->roles()->first()->name }})--}}
                {{--</div>--}}
                {{--<div class="email" style="color: rgba(255, 255, 255, .5)">{{ Auth::getUser()->email }}</div>--}}
                {{--<div class="btn-group user-helper-dropdown">--}}
                    {{--<i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                        {{--<li>--}}
                            {{--<a href="{{ route('admin.users.edit', \Auth::getUser()->id) }}">--}}
                                {{--<i class="material-icons">person</i>Perfil--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li role="seperator" class="divider"></li>--}}
                        {{--<li><a href="{{ route('logout') }}"><i class="material-icons">input</i>Logout</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- #User Info -->

        <!-- Menu -->
    @include('admin::layout.menu')
    <!-- #Menu -->

        <!-- Footer -->
        <div class="legal">
            <div class="version">
                © 2013 - {{ date('Y') }} {{ ConfigHelper::get('site_title') }} <br>
                <small><b>Versão </b> 3.0.0</small>
            </div>
        </div>
        <!-- #Footer -->

    </aside>
    <!-- #END# Left Sidebar -->

    <!-- Right Sidebar -->
{{--@include('admin::layout.rightbar')--}}
<!-- #END# Right Sidebar -->

</section>

<section class="content">
    <div class="container-fluid">
        {{--<div class="block-header">--}}
        <div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            <div id="app">
                {!! Breadcrumbs::render() !!}

                @yield('content')
            </div>

        </div>
    </div>
</section>

@include('admin::layout.footer')