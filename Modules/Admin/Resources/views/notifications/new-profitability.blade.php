<li>
    <a href="javascript:void(0);">
        <div class="icon-circle bg-light-green">
            <i class="material-icons">account_balance</i>
        </div>
        <div class="menu-info">
            <h4>Nova rentabilidade deste mês</h4>
            {{--<p>--}}
                {{--<i class="material-icons">access_time</i> 14 mins ago--}}
            {{--</p>--}}
        </div>
        @if($notification->read())
            <div class="notification_read"></div>
        @endif
    </a>
</li>
