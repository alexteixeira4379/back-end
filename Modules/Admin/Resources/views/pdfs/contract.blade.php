<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        @import url('https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98');
        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c9 {
            border-right-style: solid;
            padding: 0pt 5.4pt 0pt 5.4pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 246.1pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c17 {
            border-right-style: solid;
            padding: 0pt 5.4pt 0pt 5.4pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 241.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c23 {
            border-right-style: solid;
            padding: 0pt 5.4pt 0pt 5.4pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 283.9pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c15 {
            border-right-style: solid;
            padding: 0pt 5.4pt 0pt 5.4pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 219.6pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c7 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Calibri";
            font-style: normal
        }

        .c0 {
            padding-top: 14pt;
            padding-bottom: 14pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify;
            height: 11pt
        }

        .c4 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Arial";
            font-style: normal
        }

        .c27 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c14 {
            padding-top: 6pt;
            padding-bottom: 6pt;
            line-height: 1.1500000000000001;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c24 {
            padding-top: 6pt;
            padding-bottom: 6pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c19 {
            padding-top: 14pt;
            padding-bottom: 10pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c2 {
            padding-top: 14pt;
            padding-bottom: 14pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c8 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c11 {
            padding-top: 14pt;
            padding-bottom: 14pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c28 {
            padding-top: 0pt;
            padding-bottom: 14pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c3 {
            padding-top: 6pt;
            padding-bottom: 6pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c25 {
            padding-top: 14pt;
            padding-bottom: 14pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c6 {
            padding-top: 6pt;
            padding-bottom: 6pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c34 {
            padding-top: 52.5pt;
            padding-bottom: 41.2pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c16 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c20 {
            padding-top: 6pt;
            padding-bottom: 6pt;
            line-height: 1.1500000000000001;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c22 {
            background-color: #fefefe;
            vertical-align: baseline;
            font-family: "Arial";
            color: #4b4b4b;
            font-weight: 400
        }

        .c1 {
            vertical-align: baseline;
            font-size: 15pt;
            font-family: "Verdana";
            font-weight: 700
        }

        .c29 {
            margin-left: -12.8pt;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c21 {
            background-color: #ffffff;
            vertical-align: baseline;
            font-family: "Arial";
            font-weight: 400
        }

        .c12 {
            margin-left: -5.4pt;
            /*border-spacing: 0;*/
            /*border-collapse: collapse;*/
            margin-right: auto;

            border: none;

        }

        .c26 {
            vertical-align: baseline;
            font-family: "Arial";
            font-weight: 400
        }

        .c31 {
            text-decoration: none;
            font-size: 15pt;
            font-style: normal
        }

        .c30 {
            background-color: #ffffff;
            max-width: 630.2pt;
            padding: 42.5pt 42.5pt 42.5pt 42.5pt
        }

        .c10 {
            vertical-align: baseline;
            font-family: "Arial";
            font-weight: 700
        }

        .c33 {
            font-weight: 400;
            vertical-align: baseline;
            font-family: "Verdana"
        }

        .c32 {
            font-size: 15pt
        }

        .c13 {
            color: #000000
        }

        .c18 {
            height: 11pt
        }

        .c5 {
            height: 0pt
        }

        .title {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 39pt;
            padding-bottom: 6pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 18pt;
            color: #666666;
            font-size: 27pt;
            padding-bottom: 4pt;
            font-family: "Georgia";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 14pt;
            font-family: "Calibri"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 14pt;
            font-family: "Calibri"
        }

        h1 {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 27pt;
            padding-bottom: 6pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-weight: 700;
            font-size: 21pt;
            padding-bottom: 4pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 14pt;
            color: #000000;
            font-weight: 700;
            font-size: 17pt;
            padding-bottom: 4pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 12pt;
            color: #000000;
            font-weight: 700;
            font-size: 15pt;
            padding-bottom: 2pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 11pt;
            color: #000000;
            font-weight: 700;
            font-size: 14pt;
            padding-bottom: 2pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 10pt;
            color: #000000;
            font-weight: 700;
            font-size: 13pt;
            padding-bottom: 2pt;
            font-family: "Calibri";
            line-height: 1.1500000000000001;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c30">
<div>
    <p class="c16"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 850.93px; height: 156.00px;">
            <img alt="" src="{{asset('modules/admin/images/contract_header.png') }}" style="width: 850.93px; height: 156.00px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
    <p class="c16 c18"><span class="c7"></span></p>
</div>
<p class="c27"><span class="c7">INSTRUMENTO PARTICULAR DE SOCIEDADE EM COTA DE CONS&Oacute;RCIO</span></p>
<p class="c11"><span class="c1 c13">(</span><span class="c1">T&Iacute;TULO EXECUTIVO EXTRAJUDICIAL)</span></p>
<p class="c11"><span class="c10 c32">Contrato n&uacute;mero 000512</span></p>
<p class="c2"><span class="c10 c13">PROMITENTE CEDENTE:</span><span class="c4">&nbsp;VANIA MORAES DA CRUZ, inscrita no CNPJ/CPF sob o n&ordm; 305.101.298-03, domiciliada na Avenida Doutor Moura Ribeiro, 125 - 143E, Marap&eacute; &ndash; Santos &ndash; SP, Cep 11070-061.</span></p>
<p class="c2"><span class="c10 c13">PROMITENTE CESSION&Aacute;RIO (A):</span><span class="c4">&nbsp;Raphaelle Karoline de Souza, Brasileira, Solteira, Massoterapeuta, portador (a) da Carteira de Identidade n&ordm;49752451SP, e do CNPJ/CPF n&ordm; 380.583.508-69, residente e domiciliado (a) na Rua M&aacute;rio Ribeiro, 1454, apt 151B GUARUJ&Aacute;, SP, Cep 11.4101.92.</span></p>
<p class="c2"><span class="c4">As partes acima identificadas t&ecirc;m, entre si, justo e acertado o presente INSTRUMENTO PARTICULAR DE SOCIEDADE EM COTA DE CONS&Oacute;RCIO, que se reger&aacute; pelas cl&aacute;usulas seguintes e pelas condi&ccedil;&otilde;es descritas no presente.</span></p>
<p class="c2"><span class="c4">I &ndash; DO OBJETO DO CONTRATO</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 1&ordf;. O presente contrato tem por objeto a cess&atilde;o, pelo PROMITENTE CEDENTE ao PROMITENTE CESSION&Aacute;RIO, de parte dos direitos e obriga&ccedil;&otilde;es sobre a cota de cons&oacute;rcio n&ordm; 3503, do grupo n&ordm; 588, administrado pela BANCO DO BRASIL Cons&oacute;rcio, proporcional ao investimento.</span></p>
<p class="c2"><span class="c4">Par&aacute;grafo &uacute;nico. A base da negocia&ccedil;&atilde;o da cota de cons&oacute;rcio objeto desse contrato, pela qual responde o PROMITENTE CEDENTE, vem atualmente identificada com as seguintes informa&ccedil;&otilde;es:</span></p>
<p class="c25"><span class="c26 c13">Status: Contemplada<br>Categoria: Im&oacute;vel&nbsp; Bando do Brasil<br>Cr&eacute;dito atual: R$ </span><span class="c22">173.490,22</span><span class="c13 c26">&nbsp;<br>Saldo devedor nesta data: R$ 1.647,12<br>Parcelas restantes: </span><span class="c13 c21">01</span><span class="c4"><br>Pr&oacute;ximo vencimento: 10/11/2018<br>M&ecirc;s de reajuste: </span></p>
<p class="c2"><span class="c4">II &ndash; DO VALOR</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 2&ordf;. A cess&atilde;o se far&aacute; mediante o pagamento de R$ 2.000,00 (dois mil reais), que foram transferidos para a conta da CEDENTE atrav&eacute;s do BANCO ITA&Uacute; dia 09/10/2018, conta em nome da CESSIONARIA.</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 3&ordf;. Este valor render&aacute; a rentabilidade fixa de R$ 30,00 (trinta Reais) por m&ecirc;s at&eacute; que o valor seja resgatado, os pagamentos dos rendimentos ser&atilde;o agregados m&ecirc;s a m&ecirc;s ao valor principal do investimento. Caso o investidor queira retirar o lucro todos os meses dever&aacute; informar por escrito no ato da assinatura deste.</span></p>
<p class="c0"><span class="c4"></span></p>
<p class="c2"><span class="c10 c13">Cl&aacute;usula 4&ordf;. </span><span class="c10">O resgate parcial ou total do valor investido ser&aacute; feito da seguinte forma:</span></p>
<a id="t.9c98e1ca96f85d466e1147b185e242cd6985123c"></a>
<a id="t.0"></a>
<table class="c29">
    <tbody>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c24"><span class="c10">CAPITAL INVESTIDO</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c24"><span class="c10">PRAZO M&Aacute;XIMO PARA DEVOLU&Ccedil;&Atilde;O</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">At&eacute; R$ 10.000,00</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c14"><span class="c10">15 (quinze) dias ap&oacute;s a solicita&ccedil;&atilde;o</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c14"><span class="c10">De R$ 10.001,00 &agrave; R$ 30.000,00</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">20 (vinte) dias ap&oacute;s a solicita&ccedil;&atilde;o</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">De R$ 30.001,00 &agrave; R$ 50.000,00</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">40 (quarenta) dias ap&oacute;s a solicita&ccedil;&atilde;o</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">De R$ 50.001,00 &agrave; R$ 100.000,00</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">60 (sessenta) dias ap&oacute;s a solicita&ccedil;&atilde;o</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">De R$ 100.001,00 &agrave; R$ 200.000,00</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">70 (setenta) dias ap&oacute;s a solicita&ccedil;&atilde;o</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">Acima de R$ 200.001,00</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">90 (noventa) dias ap&oacute;s a solicita&ccedil;&atilde;o</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c6 c18"><span class="c4"></span></p>
<p class="c20"><span class="c10">Mediante solicita&ccedil;&atilde;o por escrito, os s&oacute;cios participantes podem retirar-se da sociedade e receber a devolu&ccedil;&atilde;o do seu capital, respeitado os prazos fixados no contrato, n&atilde;o podendo ser acumulativa as solicita&ccedil;&otilde;es. &nbsp;</span></p>
<p class="c20"><span class="c10">Exemplo: se solicitou o resgate de R$ 50.000,00 hoje com prazo de 40 dias e no dia seguinte solicitar mais R$ 50.000,00, logo o prazo respeitado ter&aacute; que ser para R$ 100.000,00, que no caso ser&atilde;o 60 dias.</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 5&ordf;. Em hip&oacute;tese alguma ser&atilde;o devolvidos o valor investido e o lucro apurado no per&iacute;odo de investimento antes dos 180 dias da contempla&ccedil;&atilde;o.</span></p>
<p class="c2"><span class="c4">III &ndash; OBRIGA&Ccedil;&Otilde;ES DO PROMITENTE CEDENTE</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 6&ordf;. Feito o pagamento total do valor de investimento, o PROMITENTE CEDENTE fica obrigado a gerenciar e fornecer informa&ccedil;&otilde;es a cerca do cons&oacute;rcio at&eacute; o resgate do valor, que se dar&aacute; 180 dias ap&oacute;s a contempla&ccedil;&atilde;o do bem.</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 7&ordf;. Compete ao PROMITENTE CEDENTE assegurar a veracidade das informa&ccedil;&otilde;es descritas no par&aacute;grafo &uacute;nico da cl&aacute;usula 1&ordf; deste contrato, por serem as bases da negocia&ccedil;&atilde;o e da fixa&ccedil;&atilde;o do pre&ccedil;o.</span></p>
<p class="c0"><span class="c4"></span></p>
<p class="c2"><span class="c4">IV &ndash; OBRIGA&Ccedil;&Otilde;ES DO CESSION&Aacute;RIO</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 8&ordf;. Compete ao PROMITENTE CESSION&Aacute;RIO notificar por escrito, com pelo menos 60 dias de anteced&ecirc;ncia sobre a retirada do valor investido, ainda que seja parcial e somente ap&oacute;s os 180 dias da contempla&ccedil;&atilde;o.</span></p>
<p class="c2"><span class="c4">&nbsp;V &ndash; DISPOSI&Ccedil;&Otilde;ES GERAIS</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 9&ordf;. O PROMITENTE CESSION&Aacute;RIO declara conhecer o funcionamento do sistema de cons&oacute;rcio, notadamente a forma de reajuste das parcelas mensais.</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 10&ordf;. Este instrumento vincula os herdeiros e sucessores das partes contratantes, apresentando as caracter&iacute;sticas da irrevogabilidade e a irretratabilidade contratual.</span></p>
<p class="c2"><span class="c4">VI &ndash; DO FORO</span></p>
<p class="c2"><span class="c4">Cl&aacute;usula 11&ordf;, Para dirimir quaisquer controv&eacute;rsias oriundas do presente CONTRATO, as partes elegem o foro Central da comarca de Santos.</span></p>
<p class="c2"><span class="c4">Por estarem, assim, justas e contratadas, as partes firmam o presente instrumento, em duas vias de igual teor e forma, juntamente com duas testemunhas.</span></p>
<p class="c0"><span class="c4"></span></p>
<p class="c2"><span class="c4">Santos, 10 de Outubro de 2018.</span></p>
<a id="t.e73a31e7a3239eb88a64c3b0a8125422879b69f8"></a>
<a id="t.1"></a>
<table class="c12">
    <tbody>
    <tr class="c5">
        <td class="c9" colspan="1" rowspan="1">
            <p class="c18 c28"><span class="c4"></span></p>
            <p class="c0"><span class="c4"></span></p>
            <p class="c8"><span class="c4">_____________________________________</span></p>
            <p class="c8"><span class="c4">INVESTE CONS&Oacute;RCIO</span></p>
            <p class="c3"><span class="c4">VANIA MORAES DA CRUZ</span></p>
        </td>
        <td class="c23" colspan="1" rowspan="1">
            <p class="c28 c18"><span class="c4"></span></p>
            <p class="c0"><span class="c4"></span></p>
            <p class="c8"><span class="c4">_____________________________________</span></p>
            <p class="c8"><span class="c4">S&Oacute;CIO PARTICIPANTE</span></p>
            <p class="c19"><span class="c4">RAPHAELLE KAROLINE DE SOUZA</span></p>
        </td>
    </tr>
    <tr class="c5">
        <td class="c9" colspan="1" rowspan="1">
            <p class="c28"><span class="c4">TESTEMUNHAS</span></p>
            <p class="c0"><span class="c4"></span></p>
            <p class="c8"><span class="c4">_____________________________________</span></p>
            <p class="c8"><span class="c4">NOME: JULIANA FUSUMA GARCIA</span></p>
            <p class="c3"><span class="c4">RG n&ordm; 46.599.688SSP/SP</span></p>
            <p class="c3"><span class="c4">CPF/MF n&ordm; 389.660.958-01</span></p>
            <p class="c0"><span class="c4"></span></p>
            <p class="c19 c18"><span class="c4"></span></p>
        </td>
        <td class="c23" colspan="1" rowspan="1">
            <p class="c8 c18"><span class="c4"></span></p>
            <p class="c0"><span class="c4"></span></p>
            <p class="c8"><span class="c4">_____________________________________</span></p>
            <p class="c16"><span class="c4">NOME: MELISSA CARLOS DA SILVA MONTEIRO</span></p>
            <p class="c3"><span class="c4">RG n&ordm; 24.571.685-3</span></p>
            <p class="c3"><span class="c4">CPF/MF n&ordm; 265.276.388-17</span></p>
            <p class="c3 c18"><span class="c4"></span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c0"><span class="c4"></span></p>
<p class="c18 c34"><span class="c4"></span></p>
<div>
    <p class="c16"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 850.93px; height: 184.93px;">
            <img alt="" src="{{asset('modules/admin/images/contract_footer.png') }}" style="width: 850.93px; height: 184.93px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
</div>
</body>

</html>