@extends('admin::layout.master')

@section('page_title', 'Arquivos Enviados')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">
                <div class="header">
                    <div class="row clearfix">

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <h2>Banco de Arquivos</h2>
                            <small>Total: {{ FileModel::count() }}</small>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 align-right pull-right">
                            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-6 pull-right">
                                @include('core::components._datatables_columns_dropdown')
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 pull-right">
                                <a href="javascript: void(0);" type="button"
                                   data-toggle="modal" data-target="#uploadModal"
                                   class="btn btn-success btn-circle-lg waves-effect waves-circle waves-float">
                                    <i class="material-icons" data-toggle="tooltip" data-placement="top"
                                       data-original-title="Upload de Arquivo">add</i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="body">

                    {{--<div class="table-responsive">--}}
                    {!!
                       $dataTable->table([
                           'class' => 'datatable files-datatable table table-hover table-bordered bg-gray',
                           'style' => 'width: 100%',
                           'data-token' => csrf_token()
                       ])
                    !!}
                    {{--</div>--}}

                </div>
            </div>

        </div>
    </div>

    {!! Form::open([ 'route' => [ 'admin.files.store', ], 'files' => true]) !!}
    <!-- Default Size -->
    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="uploadModalLabel">Escolha o arquivo</h4>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="body">

                            <div class="row">
                                <div class="col-md-12">
                                    <label id="title">Categoria</label> <br>
                                    {!! Form::select('file_category_id', FileCategory::pluck('name', 'id'), [ 'class' => 'form-control' ]) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label id="title">Selecione o arquivo</label> <br>
                                    {!! Form::file('file_upload', [ 'class' => 'form-control', 'style' => 'border: 0' ]) !!}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">UPLOAD</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <!-- Default Size -->
    <div class="modal fade" id="process-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Processando...</h4>
                </div>
                <div class="modal-body" style="min-height: 60px;">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        AGUARDE...
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Default Size -->
    <div class="modal fade" id="file-log-modal" data-url="{{ route('api.files.index') }}"
         tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Log de Processamento - Arquivo #@{{ file_id }}</h4>
                </div>
                <div class="modal-body">
                    <div class="preloader" v-show="is_loading">
                        <div class="spinner-layer pl-amber">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>

                    <div v-if="error_data.length == 0" class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" v-show="!is_loading">
                            <thead>
                                <tr>
                                    <th style="width: 10%;" scope="col">Arquivo</th>
                                    <th scope="col">Log</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="file in content">
                                    <td>@{{ file.id }}</td>
                                    <td>@{{ file.process_log }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div v-else v-show="!is_loading" class="alert alert-danger">
                        <strong>Ops!</strong> @{{ error_data.data }}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FECHAR</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@include('core::components._datatables')
@include('core::components._confirm_delete')

{!! $dataTable->scripts() !!}

<link href="{{ Module::asset('admin:app/files.css') }}" rel="stylesheet">
<script src="{{ Module::asset('admin:app/files.js') }}"></script>
@endpush