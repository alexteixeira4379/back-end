@extends('admin::layout.master')

@section('page_title', 'Arquivos Enviados')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                            <h2>
                                INFORMAÇÕES DA PLANILHA
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="body">

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>precos</th>
                                <th>identificador</th>
                                <th>data_leitura</th>
                                <th>id_pesquisa</th>
                                <th>uf</th>
                                <th>municipio</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($excel_data as $data)
                                <tr>
                                    <td>{{ $data['precos'] }}</td>
                                    <td>{{ $data['identificador'] }}</td>
                                    <td>{{ $data['data_leitura'] }}</td>
                                    <td>{{ $data['id_pesquisa'] }}</td>
                                    <td>{{ $data['uf'] }}</td>
                                    <td>{{ $data['municipio'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $excel_data->links() }}

                </div>
            </div>

        </div>
    </div>

@endsection

@push('scripts')

@include('core::components._datatables')

<link href="{{ Module::asset('admin:app/files.css') }}" rel="stylesheet">
<script src="{{ Module::asset('admin:app/files.js') }}"></script>
@endpush