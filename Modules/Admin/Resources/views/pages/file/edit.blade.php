@extends('admin::layout.master')

@section('page_title', 'Editar Arquivo ' . $file->name)

@section('content')

    {!! Form::model($file, ['route' => ['admin.files.update', $file->id]]) !!}
    {!! Form::hidden('_method', 'put') !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">
                <div class="header">
                    <h2>
                        Editar Arquivo "{{ mb_strtoupper($file->name) }}"
                        <small>Nome original: <strong>{{ $file->file_name }}</strong></small>
                    </h2>
                </div>
                <div class="body">

                    <div class="row">
                        <div class="col-md-4">
                            <label id="title">Nome*</label>
                            {!! Form::text('name', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <label id="title">Categoria*</label>
                            {!! Form::select('file_category_id', FileCategory::pluck('name', 'id'),  $file->file_category_id,
                                [ 'class' => 'form-control show-tick' ]) !!}
                        </div>
                        <div class="col-md-2">
                            <label id="title">Status*</label>
                            {!! Form::select('status', FileModel::STATUS_LABELS, $file->getOriginal('status'),
                               [ 'class' => 'form-control show-tick' ]) !!}
                        </div>
                    </div>

                </div>
            </div>

            @include('admin::shared._save_button')

        </div>
    </div>
    <!-- #END# TinyMCE -->

    {!! Form::close() !!}

@endsection

@push('scripts')
<link href="{{ Module::asset('admin:app/files.css') }}" rel="stylesheet">
<script src="{{ Module::asset('admin:app/files.js') }}"></script>
@endpush