{!! Form::open([ 'route' => [ 'admin.withdrawals.store' ], 'withdrawals' => 'create']) !!}
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Realizar Resgate</h4>
            </div>
            <div class="modal-body">

                <div class="body  form-horizontal">

                    <h3 class="align-center">
                        Vamos Lá ...
                        <i class="material-icons" style=" vertical-align: bottom; padding: 0 5px; ">
                            assessment</i>
                    </h3>

                    <p class="font-15 align-center">
                        Para fazer o resgate informe a quantidade e a conta que será depositada.
                        {{--Você pode escolher sua conta cadastrada ou escolher outra--}}
                    </p>

                    <div class="row">&ensp;</div>
                    <div class="row">&ensp;</div>

                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">VALOR: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('value', null, [ 'class' => 'form-control mask-money-real' ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">&ensp;</div>
                    <div class="row">&ensp;</div>
                    <div class="row">&ensp;</div>
                    <p class="font-bold font-17 align-center">
                        Dados da conta em que o valor será depositado
                    </p>


                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">BANCO: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> {{ \Auth::getUser()->bank_account->bank->friendly_name  }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">AGÊNCIA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> {{ \Auth::getUser()->bank_account->friendly_agency  }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">{{ \Auth::getUser()->bank_account->account_type  }}</label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> {{ \Auth::getUser()->bank_account->friendly_account_number  }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>

                    <p class="font-bold col-teal align-center">
                        Após a solicitação informamos que o prazo para realização é de até <strong>3 Dias Utéis</strong>.
                    </p>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="submit" class="btn btn-link waves-effect waves-light-green bg-green col-white">CONFIRMAR</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')
    <script type="application/javascript">

        $(document).ready(function () {

            $('.mask-money-real').inputmask( 'currency',{"autoUnmask": true,
                radixPoint:",",
                removeMaskOnSubmit: true,
                groupSeparator: ".",
                allowMinus: false,
                prefix: 'R$ ',
                digits: 2,
                digitsOptional: false,
                rightAlign: false,
                unmaskAsNumber: true,
                min:0,
            });

        });

    </script>
@endpush