{{--{!! Form::open(null,['router' => ['admin.withdrawals.update',5],'files' => true, 'withdrawals' => 'edit']) !!}--}}
{!! Form::open(['route' => ['admin.withdrawals.update', 5],'files' => true, 'withdrawals' => 'edit']) !!}
{!! Form::hidden('_method', 'put') !!}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" id="vue-form">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Editar Resgate</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div v-if="isLoader" class="body">
                    <div class="preloader">
                        <div class="spinner-layer pl-teal">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else class="body form-horizontal">


                    <h3 class="align-center">
                        Vamos lá...
                        <i class="material-icons" style=" vertical-align: bottom; padding: 0 5px; ">
                            assessment</i>
                    </h3>

                    <p class="font-15 align-center">
                        Você pode alterar o <strong>valor</strong> ou a <strong>conta</strong> que será enviada o resgate
                    </p>
                    <p class="font-bold col-red align-center">
                        Após a alteração clique em salvar para validar a mudança.
                    </p>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>

                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">VALOR: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('value', null,
                                    [
                                        'class' => 'form-control mask-money-real',
                                        'v-model' => 'withdrawal.value'
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">&ensp;Banco </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.bank.friendly_name }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">AGÊNCIA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.friendly_agency }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">@{{ withdrawal.user.bank_account.account_type }}: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.friendly_account_number }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="button" @click="formSubmit()" class="btn btn-success waves-effect">Salvar</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')

    <script type="application/javascript">

        var app = new Vue({
            el: '#vue-form',
            data: {
                isLoader: true,
                withdrawal: {
                    user:{
                        capital:{},
                        bank_account:{bank:{}},
                        account:{},
                    }

                },
            },
            methods: {

                getWithdrawal(id){

                    var that = this;
                    $.ajax({
                        url: "{{ route('api.withdrawals.show',null) }}" + "/" +id,
                        method: 'GET',
                        beforeSend: function () {
                            Vue.set(that,'isLoader', true);
                        }
                    }).done(function(response) {that.withdrawal = response.data;})
                        .fail(function(response) {console.log(response.responseJSON);})
                        .always(function(response) {Vue.set(that,'isLoader', false);});

                },

                formSubmit: function () {
                    Vue.set(this,'isLoader', true);

                    $('form[withdrawals="edit"]')
                        .attr('action', '{{ route('admin.withdrawals.update',null)  }}/' + this.withdrawal.id)
                        .submit();

                }
            },
        });
    </script>

    <style>
        .modal {
            text-align: center;
            padding: 0!important;
        }

        .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
@endpush