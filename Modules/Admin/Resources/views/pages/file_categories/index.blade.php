@extends('admin::layout.master')

@section('page_title', 'Categorias de arquivos')

@section('content')

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="card">
            <div class="header">
                <div class="row clearfix">

                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h2>Categorias</h2>
                        <small>Total: {{ \Modules\Core\Entities\FileCategory::count() }}</small>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 align-right pull-right">
                        <div class="col-lg-3 col-md-2 col-sm-2 col-xs-6 pull-right">
                            @include('core::components._datatables_columns_dropdown')
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 pull-right">
                            <a href="javascript: void(0);" type="button"
                               data-toggle="modal" data-target="#categoryModal"
                               class="btn btn-success btn-circle-lg waves-effect waves-circle waves-float">
                                <i class="material-icons" data-toggle="tooltip" data-placement="top"
                                   data-original-title="Upload de Arquivo">add</i>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="body">

                {{--<div class="table-responsive">--}}
                    {!!
                    $dataTable->table([
                    'class' => 'datatable files-datatable table table-hover table-bordered bg-gray',
                    'style' => 'width: 100%',
                    'data-token' => csrf_token()
                    ])
                    !!}
                    {{--</div>--}}

            </div>
        </div>

    </div>
</div>

{!! Form::open([ 'route' => [ 'admin.file-categories.store', ] ]) !!}
<!-- Default Size -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="categoryModalLabel">Escolha o arquivo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label id="title">Categoria</label> <br>
                        {!! Form::text('name', null, [ 'class' => 'form-control', 'required' => 'true' ]) !!}
                    </div>
                </div>

                <p>
                    Atenção caso você crie uma categorie e ela é usada, a mesma só poderar ser excluida apos todas as categorias vinculadas a ela serem desvinculadas
                </p>
            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect">Salvar</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

@include('admin::pages.file_categories._form._edit')

@endsection

@push('scripts')

@include('core::components._datatables')
@include('core::components._confirm_delete')

{!! $dataTable->scripts() !!}

@endpush