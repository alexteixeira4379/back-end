{{--{!! Form::open(null,['router' => ['admin.file-categories.update',5],'files' => true, 'file-categories' => 'edit']) !!}--}}
{!! Form::open(['route' => ['admin.file-categories.update', 5],'files' => true, 'file-categories' => 'edit']) !!}
{!! Form::hidden('_method', 'put') !!}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" id="vue-form">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Categoria</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div v-if="isLoader" class="body">
                    <div class="preloader">
                        <div class="spinner-layer pl-teal">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else class="body form-horizontal">



                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">Nome: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('name', null,
                                    [
                                        'class' => 'form-control mask-money-real',
                                        'v-model' => 'file_category.name'
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="button" @click="formSubmit()" class="btn btn-success waves-effect">Salvar</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')

    <script type="application/javascript">

        var app = new Vue({
            el: '#vue-form',
            data: {
                isLoader: true,
                file_category: {
                },
            },
            methods: {

                getCategories(id){

                    var that = this;
                    $.ajax({
                        url: "{{ route('admin.file-categories.show',null) }}" + "/" +id,
                        method: 'GET',
                        beforeSend: function () {
                            Vue.set(that,'isLoader', true);
                        }
                    }).done(function(response) {that.file_category = response.data;})
                        .fail(function(response) {console.log(response.responseJSON);})
                        .always(function(response) {Vue.set(that,'isLoader', false);});

                },

                formSubmit: function () {
                    Vue.set(this,'isLoader', true);

                    $('form[file-categories="edit"]')
                        .attr('action', '{{ route('admin.file-categories.update',null)  }}/' + this.file_category.id)
                        .submit();

                }
            },
        });
    </script>

    <style>
        .modal {
            text-align: center;
            padding: 0!important;
        }

        .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
@endpush