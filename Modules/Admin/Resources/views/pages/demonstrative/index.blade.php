@extends('admin::layout.master')

@section('page_title', 'Demonstativos de resultados')

@section('content')


    <div class="card">
        <div class="header">
            <h2>
                Demonstrativo de Resultado do Mês de Abril/2019
            </h2>
        </div>
        <div class="body" style="padding: 0 10px">
            <div class="row clearfix">
                <div class="col-md-6">
                    {!! Form::open(['route' => ['admin.demonstratives.index'], 'method' => 'GET']) !!}
                        <div class="form-group p-b-10 p-t-20">
                            <label class="p-l-15 control-label align-right">Selecione o mês</label>
                            <br>
                            <div class="col-sm-6">
                                <div class="form-line">
                                    {!! Form::select('period',  $period,$date,['class' => 'form-control except' ]) !!}

                                </div>
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" class="btn bg-blue waves-effect"><i class="material-icons">search</i> <span>Exibir</span></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">

            @include('admin::shared._profitability',['year_filter_select' => $year_filter_select])

        </div>
        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">

            <div class="card">
                <div class="header">
                    <h2>
                        Seu Capital
                    </h2>
                </div>
                <div class="body" style="padding: 0 10px">
                    <div class="row clearfix">

                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            @include('admin::shared._capital')
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            @include('admin::shared._last_capital',\Modules\Core\Entities\Capital::getLastCapital($date))
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    {!! \Modules\Core\Entities\Profitability::getPrevProfitability($date) !!}
                    {{--@include('admin::shared._prev_profitability')--}}
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    @include('admin::shared._social_capital')
                </div>
            </div>


        </div>
    </div>


    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="body">
                    @include('admin::pages.demonstrative._shared._recipe',$recipes)
                    @include('admin::pages.demonstrative._shared._expense',$expenses)
                    @include('admin::pages.demonstrative._shared._taxation',$taxations)
                </div>
            </div>
        </div>

    </div>

    <div class="row clearfix">
        <div class="col-xs-6">
            @include('admin::pages.demonstrative._shared._net_calculated',$net_calculated)
        </div>
        <div class="col-xs-6">
            @include('admin::pages.demonstrative._shared._participation_index',$part_index)
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-xs-6">
            @include('admin::pages.demonstrative._shared._profit_distribution',$profit_distribution)
        </div>
        <div class="col-xs-6">
            <div class="card">
                <div class="body">
                    <p class="text-danger font-20">
                        <b>Atenção</b>
                        <br>
                        Caso o sócio participante tenha ingressado na sociedade, feito aporte ou resgate, no decorrer do mês, divida o resultado pelos dias do mês (30) e multiplique pelo número de dias que o capital passou a fazer parte da sociedade.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="body">
                    <p class="font-12">
                        <b>DATA DO DEPÓSITO: 05-{!! $date !!}.</b>
                        <br>
                        Conforme disposto na cláusula 17ª do Contrato Social, a demonstração de resultado e o crédito da rentabilidade são realizados até o 5º dia útil do mês subsequente ao mês de apuração.
                    </p>
                </div>
            </div>
        </div>
    </div>



@endsection

@push('scripts')

    <script src="{{ Module::asset('core:plugins/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ Module::asset('core:plugins/flot-charts/jquery.flot.time.js') }}"></script>

    <script src="{{ Module::asset('core:plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

    <link href="{{ Module::asset('admin:app/dashboard.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/dashboard.js') }}"></script>

    <script type="text/javascript">


    </script>


@endpush