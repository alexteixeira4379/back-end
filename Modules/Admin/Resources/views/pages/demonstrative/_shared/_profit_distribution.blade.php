<div class="card">
    <div class="header">
        <h2>
            Distribuição de lucro

        </h2>
    </div>
    <div class="body">

        <p>
            Multiplique o capital investido pelo índice de participação.
            <br>
            <b>Exemplo:</b>
        </p>

        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15">Capital investido:</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $profit_distribution['value'] !!}</p>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15"> Índice de participação:</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $profit_distribution['participation_index'] !!}</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15"> Resultado: </h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $profit_distribution['result'] !!}</p>
            </div>
        </div>


    </div>
</div>