<div class="panel-group full-body" id="accordion_taxation" role="tablist" aria-multiselectable="true">
    <div class="panel panel-col-blue-grey">
        <div class="panel-heading" role="tab" id="headingTaxation">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapseTaxation" aria-expanded="true" aria-controls="collapseTaxation">
                    <i class="material-icons">description</i>  Impostos e taxas
                </a>
            </h4>
        </div>
        <div id="collapseTaxation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTaxation">
            <div class="panel-body bg-white">

                <table class="table">
                    <thead style="background-color: #304550">
                    <tr>
                        <th>Natureza</th>
                        <th>Tributo</th>
                        <th>Valor</th>
                        <th>Data</th>
                        <th>Data de Registro</th>
                    </tr>
                    </thead>
                    <tbody style=" background: #3045502b; color: black; ">
                    @if(! $taxations->isEmpty()) @foreach($taxations as $taxation)
                        <tr>
                            <td>{!! $taxation->kind !!}</td>
                            <td>{!! $taxation->tribute !!}</td>
                            <td>{!! $taxation->value !!}</td>
                            <td>{!! $taxation->date !!}</td>
                            <td>{!! $taxation->datetime_format !!}</td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="align-center">
                                <p class="p-b-10 p-t-10"> Não há registros </p>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
