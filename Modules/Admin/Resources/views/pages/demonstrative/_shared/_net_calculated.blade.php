<div class="card">
    <div class="header">
        <h2>
            Lucro líquido apurado no mês

        </h2>
    </div>
    <div class="body">

        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15">Receitas:</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $net_calculated['totalRecipes'] !!}</p>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15"> (-) Despesas, impostos e taxas :</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $net_calculated['totalDispenses'] !!}</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15"> Lucro = :</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $net_calculated['profit'] !!}</p>
            </div>
        </div>


    </div>
</div>