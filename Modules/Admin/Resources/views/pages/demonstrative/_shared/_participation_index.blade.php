<div class="card">
    <div class="header">
        <h2>
            Indice de participação

        </h2>
    </div>
    <div class="body">

        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15">Lucro dos sócios participantes:</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $part_index['partner_profit'] !!}</p>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15"> Capital dos sócios participantes:</h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $part_index['partner_capital'] !!}</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xs-9">
                <h5 class="font-15"> Índice: </h5>
            </div>
            <div class="col-xs-3">
                <p class="font-15">{!! $part_index['partner_profitability'] !!}</p>
            </div>
        </div>


    </div>
</div>