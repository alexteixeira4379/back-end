<div class="panel-group full-body" id="accordion_recipe" role="tablist" aria-multiselectable="true">
    <div class="panel panel-col-blue-grey">
        <div class="panel-heading" role="tab" id="headingRecipe">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapseRecipe" aria-expanded="true" aria-controls="collapseRecipe">
                    <i class="material-icons">description</i> Receitas
                </a>
            </h4>
        </div>
        <div id="collapseRecipe" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingRecipe">
            <div class="panel-body bg-white">

                <table class="table">
                    <thead style="background-color: #304550">
                    <tr>
                        <th>Natureza</th>
                        <th>Cedente/Cessionário</th>
                        <th>Valor</th>
                        <th>Data</th>
                        <th>Data de Registro</th>
                    </tr>
                    </thead>
                    <tbody style=" background: #3045502b; color: black; ">
                    @if(! $recipes->isEmpty()) @foreach($recipes as $recipe)
                        <tr>
                            <td>{!! $recipe->kind !!}</td>
                            <td>{!! $recipe->assignee_assignor !!}</td>
                            <td>{!! $recipe->value !!}</td>
                            <td>{!! $recipe->date !!}</td>
                            <td>{!! $recipe->datetime_format !!}</td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="align-center">
                                <p class="p-b-10 p-t-10"> Não há registros </p>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
