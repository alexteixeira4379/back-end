<div class="panel-group full-body" id="accordion_expense" role="tablist" aria-multiselectable="true">
    <div class="panel panel-col-blue-grey">
        <div class="panel-heading" role="tab" id="headingExpense">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapseExpense" aria-expanded="true" aria-controls="collapseExpense">
                    <i class="material-icons">description</i> Despensas
                </a>
            </h4>
        </div>
        <div id="collapseExpense" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingExpense">
            <div class="panel-body bg-white">

                <table class="table">
                    <thead style="background-color: #304550">
                    <tr>
                        <th>Natureza</th>
                        <th>Destinatário</th>
                        <th>Valor</th>
                        <th>Data</th>
                        <th>Data de Registro</th>
                    </tr>
                    </thead>
                    <tbody style=" background: #3045502b; color: black; ">
                    @if(! $expenses->isEmpty()) @foreach($expenses as $expense)
                        <tr>
                            <td>{!! $expense->kind !!}</td>
                            <td>{!! $expense->addressee !!}</td>
                            <td>{!! $expense->value !!}</td>
                            <td>{!! $expense->date !!}</td>
                            <td>{!! $expense->datetime_format !!}</td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="align-center">
                                <p class="p-b-10 p-t-10"> Não há registros </p>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
