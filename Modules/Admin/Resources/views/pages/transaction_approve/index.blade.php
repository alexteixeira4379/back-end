@extends('admin::layout.master')

@section('page_title', 'Aprovar Transações')

@section('content')

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="card">
            <div class="body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab_contribution" data-toggle="tab">
                            <i class="material-icons">add</i> SOLICITAÇÕES DE APORTES
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#tab_withdrawals" data-toggle="tab">
                            <i class="material-icons">remove</i>SOLICITAÇÕES DE RESGATES
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="tab_contribution">
                        {{--<div class="table-responsive">--}}
                        {!!
                           $contributionsDataTable->table([
                               'class' => 'datatable table table-hover table-bordered bg-gray',
                               'style' => 'width: 100%',
                               'data-token' => csrf_token(),
                               'id' => 'contributions'
                           ])
                        !!}
                        {{--</div>--}}
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_withdrawals">
                        {{--<div class="table-responsive">--}}
                        {!!
                           $withdrawalsDataTable->table([
                               'class' => 'datatable table table-hover table-bordered bg-gray',
                               'style' => 'width: 100%',
                               'data-token' => csrf_token(),
                               'id' => 'withdrawals'
                           ])
                        !!}
                        {{--</div>--}}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@include('admin::pages.transaction_approve._withdrawal_elements._edit')
@include('admin::pages.transaction_approve._contribution_elements._edit')

@endsection

@push('scripts')


@include('core::components._datatables')
@include('core::components._confirm_delete')

{!! $contributionsDataTable->scripts() !!}
{!! $withdrawalsDataTable->scripts() !!}

<link href="{{ Module::asset('admin:app/transaction-approve.css') }}" rel="stylesheet">
<link href="{{ Module::asset('core:plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">

<script src="{{ Module::asset('core:plugins/light-gallery/js/lightgallery-all.js') }}"></script>
<script src="{{ Module::asset('core:plugins/light-gallery/js/lg-thumbnail.min.js') }}"></script>
<script src="{{ Module::asset('core:plugins/light-gallery/js/lg-thumbnail.min.js') }}"></script>

<script src="{{ Module::asset('admin:app/transaction-approve.js') }}"></script>

@endpush