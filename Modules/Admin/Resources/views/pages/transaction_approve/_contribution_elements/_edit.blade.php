{!! Form::open(['route' => ['admin.contributions.update', null],'files' => true, 'contributions' => 'edit']) !!}
{!! Form::hidden('_method', 'put') !!}
<div class="modal fade" id="editModalContribution" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" id="contributionApp">
        <div v-if="_.isEmpty(response_message)" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Solicitação de aporte</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div v-if="isLoader" class="body">
                    <div class="preloader">
                        <div class="spinner-layer pl-teal">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else class="body form-horizontal">
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">SÓCIO(A): </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ contribution.user.account.name }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">VALOR DO APORTE: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; ">R$ @{{ contribution.value }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">DATA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p class="font-italic" style="line-height: 2; ">@{{ contribution.date_format }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">COMPROVANTE ENVIADO: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <div v-if="contribution.file_link" id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                    <div class="col-xs-12">
                                        <a :href="contribution.file_link" data-sub-html="Foto do comprovante">
                                            <img class="img-responsive thumbnail" :src="contribution.file_link">
                                        </a>
                                    </div>
                                </div>
                                <p v-else style="line-height: 2; " class="col-red">Nenhum comprovante enviado até o momento</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="button" @click="formSubmit(false)" class="btn btn-danger waves-effect">NEGAR</button>
                <button type="button" @click="formSubmit(true)" class="btn btn-success waves-effect">APROVAR</button>
            </div>
        </div>
        <div v-else class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Solicitação de aporte</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div class="body">
                    <div v-if="response_message.status == 'success'">
                        <h5>TUDO CERTO... TRANSAÇÃO APROVADA COM SUCESSO!</h5>
                        <div class="alert bg-green">
                            @{{response_message.message}}
                        </div>
                    </div>
                    <div v-else >
                        <h5>OPS... Algo de deu errado</h5>
                        <div class="alert bg-danger">
                            @{{response_message.message}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" @click="setPatterns()" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')

<script type="application/javascript">

    var contributionApp = new Vue({
        el: '#contributionApp',
        data: {
            isLoader: true,
            response_message: {},
            contribution: {user:{ account:{} }},
        },
        methods: {

            /*
              Set default of this application after actions
            */
            setPatterns(){

                this.response_message = {};
                this.contribution = {user:{ account:{} }};
                this.isLoader = true;

            },

            /*
             Ajax with contribution data
           */
            getContribution(id){

                this.setPatterns();

                var that = this;

                $.ajax({
                    url: "{{ route('api.contributions.show',null) }}" + "/" +id,
                    method: 'GET',
                    beforeSend: function () {
                        Vue.set(that,'isLoader', true);
                    }
                })
                /*
                   Do something when it completes successfully
                */
                .done(function(response)
                {
                    that.contribution = response.data;
                })
                /*
                   Do something when it fails
                */
                .fail(function(response)
                {
                    console.log(response.responseJSON);
                })
                /*
                   Do something ever
                */
                .always(function(response)
                    {
                        Vue.set(that,'isLoader', false);
                    }
                );

            },

            formSubmit: function (approve) {

                var that = this;

                this.response_message = {};
                this.isLoader = true;

                $.ajax({

                    url: "{{ route('api.contributions.update',null) }}" + "/" + that.contribution.id,
                    method: 'PUT',
                    dataType: 'json',
                    data: { approve: approve },

                    beforeSend: function () {
                        // Code before send here
                    }
                })
                /*
                  Do something when it completes successfully
               */
                .done(function(response)
                {
                    if(typeof response == 'string')
                        that.response_message = JSON.parse(response);
                    else
                        that.response_message = response;

                    LaravelDataTables.contributions.ajax.reload()
                })
                /*
                   Do something when it fails
                */
                .fail(function(response)
                {
                    if(typeof response.responseJSON == 'string')
                        that.response_message = JSON.parse(response.responseJSON);
                    else
                        that.response_message = response.responseJSON;
                })
                /*
                   Do something ever
                */
                .always(function(response)
                    {
                        Vue.set(that,'isLoader', false);
                    }
                );

            }
        },
        updated: function () {
            this.$nextTick(function () {

                /* Binding plugin after vue mounted structure */
                $('#aniimated-thumbnials').lightGallery({
                    thumbnail: true,
                    selector: 'a'
                });

            })
        }
    });

</script>
@endpush