{!! Form::open(['route' => ['admin.withdrawals.update', 5],'files' => true, 'withdrawals' => 'edit']) !!}
{!! Form::hidden('_method', 'put') !!}
<div class="modal fade" id="editModalWithdrawal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" id="withdrawalApp">
        <div v-if="_.isEmpty(response_message)" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">APROVAR RESGATE</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div v-if="isLoader" class="body">
                    <div class="preloader">
                        <div class="spinner-layer pl-teal">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else class="body form-horizontal">

                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">SÓCIO(A): </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.account.name }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">VALOR DO APORTE: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; ">R$ @{{ withdrawal.value }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">DATA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p class="font-italic" style="line-height: 2; ">@{{ withdrawal.date_format }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">CAPITAL: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p class="col-teal" style="line-height: 2; "> @{{ withdrawal.user.capital.balance_to_real }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>


                    <p class="font-15 align-center" style="border-top: 2px solid; padding: 11px;">
                       Dados bancarios para transferência
                    </p>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">&ensp;Banco </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.bank.friendly_name }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">AGÊNCIA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.friendly_agency }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">@{{ withdrawal.user.bank_account.account_type }}: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.friendly_account_number }} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">PROPRIETÁRIO: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> @{{ withdrawal.user.bank_account.owner_name }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">@{{ withdrawal.user.bank_account.document_type }}: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; ">
                                    @{{ getCpnjCpfMaks(withdrawal.user.bank_account.document_type,withdrawal.user.bank_account.document_number) }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                    <p class="font-15 align-center" style="border-top: 2px solid; padding: 11px;">
                        Documento da transferência
                    </p>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">COMPROVANTE: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::file('file_upload', [
                                        'class' => 'form-control',
                                        'style' => 'border: 0',
                                        'ref' =>'file',
                                        'id' =>'file',
                                        '@change'=>'handleFileUpload()',
                                    ]) !!}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="button" @click="formSubmit(false)" class="btn btn-danger waves-effect">NEGAR</button>
                <button type="button" @click="formSubmit(true)" class="btn btn-success waves-effect">APROVAR</button>
            </div>
        </div>
        <div v-else class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Solicitação de Resgate</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div class="body">

                    <div v-if="response_message.status == 'success'">
                        <h5>TUDO CERTO... TRANSAÇÃO APROVADA COM SUCESSO!</h5>
                        <div class="alert bg-green">
                            @{{response_message.message}}
                        </div>
                    </div>
                    <div v-else >
                        <h5>OPS... Algo de deu errado</h5>
                        <div class="alert bg-danger">
                            @{{response_message.message}}
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" @click="setPatterns()" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@push('scripts')

<script type="application/javascript">

    var withdrawalApp = new Vue({
        el: '#withdrawalApp',
        data: {
            response_message: {},
            file: '',
            isLoader: true,
            withdrawal: {user:{ account:{} }},

        },
        methods: {

            /*
              Set default of this application after actions
            */
            setPatterns()
            {
                this.isLoader = true;
                this.response_message = {};
                this.withdrawal = {user:{ account:{} }};

            },

            /*
               Handles a change on the file upload
             */
            handleFileUpload()
            {
                this.file = this.$refs.file.files[0];
            },

            /*
               Get input mask
             */
            getCpnjCpfMaks(type,number)
            {
                if(type == 'CPF')
                    return number
                        .replace( /(\d{3})(\d)/ , "$1.$2")
                        .replace( /(\d{3})(\d)/ , "$1.$2")
                        .replace( /(\d{3})(\d{1,2})$/ , "$1-$2");
                 else
                    return number.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
            },

            /*
               Ajax with withdrawal data
             */
            getWithdrawal(id)
            {
                this.setPatterns();

                var that = this;

                $.ajax({
                    url: "{{ route('api.withdrawals.show',null) }}" + "/" +id,
                    method: 'GET',
                    beforeSend: function () {
                        Vue.set(that,'isLoader', true);
                    }
                })
                /*
                   Do something when it completes successfully
                */
                .done(function(response)
                {
                    that.withdrawal = response.data;
                })
                /*
                   Do something when it fails
                */
                .fail(function(response)
                {
                    console.log(response.responseJSON);
                })
                /*
                   Do something ever
                */
                .always(function(response)
                {
                    Vue.set(that,'isLoader', false);
                }
                );

            },

            /*
              Submit withdrawal
            */
            formSubmit: function (approve) {
                var that = this;

                var formData = new FormData();
                formData.append('file_upload', this.file);
                formData.append('approve',approve);
                formData.append('id_request',that.withdrawal.id);

                this.isLoader = true;
                this.response_message = {};

                $.ajax({

                    url: "{{ route('api.withdrawals.store')}}",
                    method: 'POST',
                    dataType: 'text',
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,

                    beforeSend: function () {
                        // Code before send here
                    }
                })
                /*
                  Do something when it completes successfully
               */
                .done(function(response)
                {
                    if(typeof response == 'string')
                        that.response_message = JSON.parse(response);
                    else
                        that.response_message = response;

                    LaravelDataTables.withdrawals.ajax.reload()
                })
                /*
                   Do something when it fails
                */
                .fail(function(response)
                {
                    if(typeof response.responseJSON == 'string')
                        that.response_message = JSON.parse(response.responseJSON);
                    else
                        that.response_message = response.responseJSON;
                })
                /*
                   Do something ever
                */
                .always(function(response)
                    {
                        Vue.set(that,'isLoader', false);
                    }
                );

            }
        },
    });
</script>
@endpush