{{--{!! Form::open(null,['router' => ['admin.contributions.update',5],'files' => true, 'contributions' => 'edit']) !!}--}}
{!! Form::open(['route' => ['admin.contributions.update', 5],'files' => true, 'contributions' => 'edit']) !!}
{!! Form::hidden('_method', 'put') !!}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" id="vue-form">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Editar Aporte</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div v-if="isLoader" class="body">
                    <div class="preloader">
                        <div class="spinner-layer pl-teal">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else class="body form-horizontal">


                    <h3 class="align-center">
                        Falta Pouco...
                        <i class="material-icons" style=" vertical-align: bottom; padding: 0 5px; ">
                            assessment</i>
                    </h3>

                    <p class="font-15 align-center">
                        Você pode alterar o <strong>valor</strong> ou reenviar um novo <strong>comprovante</strong> para o aporte
                    </p>
                    <p class="font-bold col-red align-center">
                        Após a alteração clique em salvar para validar a mudança.
                    </p>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>

                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">VALOR: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('value', null,
                                    [
                                        'class' => 'form-control mask-money-real',
                                        'v-model' => 'contribution.value'
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">COMPROVANTE: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::file('file_upload', [
                                        'class' => 'form-control',
                                        'style' => 'border: 0',
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label align-center">DATA DE ATUALIZAÇÃO: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p class="font-italic" style="line-height: 2; ">{!! (\Carbon\Carbon::now())->format('d/m/Y') !!}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">&ensp;Banco </label>
                        <div class="col-sm-8">
                            <img src="{!!  Module::asset('admin:images/logo-itau.png') !!}" style="max-width: 56px">
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">AGÊNCIA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> XXXX </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">C\ CORRENTE: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> XXXX </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">RAZÃO SOCIAL: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> {!! \Setting::first()->company_name !!} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-4 control-label">CNPJ: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                <p style="line-height: 2; "> {!! \Setting::first()->company_cnpj !!} </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>



                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="button" @click="formSubmit()" class="btn btn-success waves-effect">Salvar</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')

    <script type="application/javascript">

        var app = new Vue({
            el: '#vue-form',
            data: {
                isLoader: true,
                contribution: {
                },
            },
            methods: {

                getContribution(id){

                    var that = this;
                    $.ajax({
                        url: "{{ route('api.contributions.show',null) }}" + "/" +id,
                        method: 'GET',
                        beforeSend: function () {
                            Vue.set(that,'isLoader', true);
                        }
                    }).done(function(response) {that.contribution = response.data;})
                        .fail(function(response) {console.log(response.responseJSON);})
                        .always(function(response) {Vue.set(that,'isLoader', false);});

                },

                formSubmit: function () {
                    Vue.set(this,'isLoader', true);

                    $('form[contributions="edit"]')
                        .attr('action', '{{ route('admin.contributions.update',null)  }}/' + this.contribution.id)
                        .submit();

                }
            },
        });
    </script>

    <style>
        .modal {
            text-align: center;
            padding: 0!important;
        }

        .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
@endpush