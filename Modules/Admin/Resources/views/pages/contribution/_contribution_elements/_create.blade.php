{!! Form::open([ 'route' => [ 'admin.contributions.store' ],'files' => true, 'contributions' => 'create']) !!}
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabel">Escolha o arquivo</h4>
            </div>
            <div class="modal-body">

                <div class="body  form-horizontal">
                    <div id="wizard_vertical">
                        <h2>Primeiro passo</h2>
                        <section>
                            <div class="row">&nbsp;</div>
                            {{--<div class="form-group">--}}
                            {{--<label for="company_name" class="col-sm-4 control-label">SÓCIO(A) PARTICIPANTE: </label>--}}
                            {{--<div class="col-sm-8">--}}
                            {{--<div class="form-line">--}}
                            {{--{!! Form::select('account_id',--}}
                            {{--\User::with('account')->get()->pluck('account.name','id')->toArray(),--}}
                            {{--null, [ 'class' => 'form-control show-tick'])--}}
                            {{--!!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="row">&nbsp;</div>
                            <div class="form-group">
                                <label for="company_name" class="col-sm-4 control-label">VALOR: </label>
                                <div class="col-sm-8">
                                    <div class="form-line">
                                        {!! Form::text('value', null, [ 'class' => 'form-control mask-money-real' ]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">&nbsp;</div>
                            <div class="form-group">
                                <label for="company_name" class="col-sm-4 control-label">DATA: </label>
                                <div class="col-sm-8">
                                    <div class="form-line">
                                        <p class="font-italic" style="line-height: 2; ">{!! (\Carbon\Carbon::now())->format('d/m/Y') !!}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="row">&nbsp;</div>
                            <div class="row">&nbsp;</div>

                        </section>

                        <h2>Segundo passo</h2>
                        <section>
                            <h3 class="align-center">
                                Falta Pouco...
                                <i class="material-icons" style=" vertical-align: bottom; padding: 0 5px; ">
                                    assessment</i>
                            </h3>

                            <p class="font-15 align-center">
                                Agora para continuar precisamos que faça o deposito em nossa conta
                            </p>
                            <p class="font-bold col-teal align-center">
                                Após o deposíto envie o comprovante para que possamos confirmar seu deposíto.
                            </p>


                            <div class="row">&nbsp;</div>
                            <div class="row">&nbsp;</div>

                            <div class="row">

                                <div class="col-md-12 col-lg-8">

                                    <div class="form-group visible-sm visible-md visible-xs">
                                        <label for="company_name" class="col-sm-4 control-label">&ensp; </label>
                                        <div class="col-sm-8">
                                            <img src="{!!  Module::asset('admin:images/logo-itau.png') !!}" style="max-width: 56px">
                                        </div>
                                    </div>

                                    <div class="row">&nbsp;</div>
                                    <div class="form-group">
                                        <label for="company_name" class="col-sm-4 control-label">AGÊNCIA: </label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <p style="line-height: 2; "> XXXX </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">&nbsp;</div>
                                    <div class="form-group">
                                        <label for="company_name" class="col-sm-4 control-label">C\ CORRENTE: </label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <p style="line-height: 2; "> XXXX </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">&nbsp;</div>
                                    <div class="form-group">
                                        <label for="company_name" class="col-sm-4 control-label">RAZÃO SOCIAL: </label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <p style="line-height: 2; "> {!! \Setting::first()->company_name !!} </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">&nbsp;</div>
                                    <div class="form-group">
                                        <label for="company_name" class="col-sm-4 control-label">CNPJ: </label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <p style="line-height: 2; "> {!! \Setting::first()->company_cnpj !!} </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">&nbsp;</div>
                                    <div class="row">&nbsp;</div>
                                    <div class="form-group">
                                        <label for="company_name" class="col-sm-4 control-label">COMPROVANTE: </label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                {!! Form::file('file_upload', [ 'class' => 'form-control', 'style' => 'border: 0' ]) !!}
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-4 align-center visible-lg">
                                    <img src="{!!  Module::asset('admin:images/logo-itau.png') !!}" style="max-width: 136px">
                                </div>

                            </div>

                        </section>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')
    <script src="{{ Module::asset('core:plugins/jquery-steps/jquery.steps.js') }}"></script>
    <script type="application/javascript">

        $(document).ready(function () {

            $("#wizard_vertical").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                stepsOrientation: 'horizontal',
                labels: {
                    cancel: "Cancelar",
                    current: "Passo atual:",
                    pagination: "Paginação",
                    finish: "Concluir",
                    next: "Próximo",
                    previous: "Voltar",
                    loading: "Loading ..."
                },
                onFinished: function (ev, current_index) {

                    $(".mask-money-real").unmask();
                    $('form[contributions="create"]').submit();

                }
            });

            $('.mask-money-real').inputmask( 'currency',{"autoUnmask": true,
                radixPoint:",",
                removeMaskOnSubmit: true,
                groupSeparator: ".",
                allowMinus: false,
                prefix: 'R$ ',
                digits: 2,
                digitsOptional: false,
                rightAlign: false,
                unmaskAsNumber: true,
                min:1000,
            });

        });

    </script>
@endpush