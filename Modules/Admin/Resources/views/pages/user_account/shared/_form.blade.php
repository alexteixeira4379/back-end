
<div class="row">
    <div class="col-md-4">
        <label id="title">CEP*</label> <br>
        {!! Form::text('zip_code', null, [ 'class' => 'form-control mask-zip-code','v-model' => 'data_cep.cep', 'v-on:change' => 'getAndSetCep()' ]) !!}
    </div>
    <div class="col-md-5">
        <label id="title">Endereço*</label>
        {!! Form::text('address', null, [ 'class' => 'form-control','v-model' => 'data_cep.logradouro' ]) !!}
    </div>
    <div class="col-md-3">
        <label id="title">Número*</label>
        {!! Form::text('number', null, [ 'class' => 'form-control mask-number' ]) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <label id="title">Bairro*</label>
        {!! Form::text('neighborhood', null, [ 'class' => 'form-control','v-model' => 'data_cep.bairro' ]) !!}
    </div>
    <div class="col-md-4">
        <label id="title">Cidade*</label>
        {!! Form::text('city', null, [ 'class' => 'form-control','v-model' => 'data_cep.localidade' ]) !!}
    </div>
    <div class="col-md-4">
        <label id="title">Estado</label>
        {!! Form::select('state', UserAccount::STATES_LABELS, null, [ 'class' => 'form-control','v-model' => 'data_cep.uf'])!!}
    </div>
</div>

<div class="row">

    <div class="col-md-4">
        <label id="title">Telefone 1*</label>
{{--        {!! Form::text('telephone_1', null, [ 'class' => 'form-control mask-telephone' ]) !!}--}}
        <the-mask :mask="['(##) ####-####', '(##) #####-####']" name="telephone_1" class="form-control" />
    </div>
    <div class="col-md-4">
        <label id="title">Telefone 2</label>
{{--        {!! Form::text('telephone_2', null, [ 'class' => 'form-control mask-telephone' ]) !!}--}}
        <the-mask :mask="['(##) ####-####', '(##) #####-####']" name="telephone_2" class="form-control" />
    </div>
</div>