@push('scripts')

    <script type="text/x-template" id="form_physics">

        <div>
            <div class="row">
                <div class="col-md-12">
                    <label id="title">Nome Completo*</label>
                    {!! Form::text('full_name', null, [ 'class' => 'form-control' ]) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label id="title">CPF*</label>
                    <the-mask :mask="'###.###.###-##'" name="cpf" class="form-control mask-cpf" />
                    {{--   {!! Form::text('cpf', null, [ 'class' => 'form-control mask-cpf' ]) !!}--}}
                </div>

                <div class="col-md-6">
                    <label id="title">RG*</label>
                    <the-mask :mask="'##.###.###-#'" name="rg" class="form-control mask-rg" />
                    {{--                    {!! Form::text('rg', null, [ 'class' => 'form-control mask-rg' ]) !!}--}}
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <label id="title">Nascimento*</label>
                    {!! Form::date('birthday', null, [ 'class' => 'form-control mask-date' ]) !!}
                </div>
                <div class="col-md-6">
                    <label id="title">Estado Civil</label>
                    {!! Form::select('marital_status', UserAccount::MARITAL_STATUS_LABELS, UserAccount::MARITAL_STATUS_NOT_MARRIED,
                         [ 'class' => 'form-control']
                         )
                     !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label id="title">Nacionalidade*</label>
                    {!! Form::text('nationality', null, [ 'class' => 'form-control' ]) !!}
                </div>

                <div class="col-md-6">
                    <label id="title">Profissão*</label>
                    {!! Form::text('profession', null, [ 'class' => 'form-control' ]) !!}
                </div>
            </div>

        </div>

    </script>


    <script>

        Vue.component('form-physics',{

            template: '#form_physics',
            props: [],
            data: function () {
                return {

                }
            },
            methods:{

            },


        });




    </script>

@endpush