
@push('scripts')
    <script type="text/x-template" id="form_legal">

        <div>
            <div class="row">
                <div class="col-md-12">
                    <label id="title">Razão Social</label>
                    {!! Form::text('company_name', null, [ 'class' => 'form-control' ]) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label id="title">CNPJ</label>
{{--                    {!! Form::text('cnpj', null, [ 'class' => 'form-control mask-cnpj' ]) !!}--}}
                    <the-mask :mask="'##.###.###/####-##'" name="cnpj" class="form-control mask-cnpj" />
                </div>

            </div>

        </div>
    </script>


    <script>

        Vue.component('form-legal',{
            template: '#form_legal',
            props: [],
            data: function () {
                return {

                }
            },
            methods:{

            },

        });

    </script>

@endpush