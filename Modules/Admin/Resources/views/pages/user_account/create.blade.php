@extends('admin::layout.blankpage')

@section('page_title', 'Cadastrar Usuário')

@section('content')

    {!! Form::open(['route' => [ 'admin.accounts.store' ]],[ 'id'=>'vue_instance' ]) !!}

    <div class="row clearfix">
        <div class="col-sm-2 col-lg-3 col-md-3"></div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 border-pin" style="max-width: 800px">

            <div class="card-title-sign mt-3 text-left">
                <a href="/" class="logo float-left">
                    <img src="{{  Module::asset('admin:images/logo-amarelo.png')  }}" height="54" alt="Investe Consórcio">
                </a>
            </div>
            <div class="card costum-card">
                <div class="header">
                    <h2 class="p-l-10">
                        ABRIR CONTA
                    </h2>
                    <p class="m-t-10 p-t-10 p-l-10" style=" border-top: 1px solid #a5a5a5">Dados Cadastrais</p>
                </div>
                <div class="body">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="alert alert-danger" id="error-validation" style="display: none">
                        <p></p>
                    </div>

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif



                    @if(!isset($code_shared) && ! old('code_shared') )
                        <div class="row">
                            <div class="col-md-6">
                                <label id="title">Como conheceu a Investe Consórcio?</label>
                                {!! Form::select('met_for', UserAccount::MET_FOR_LABELS, UserAccount::MET_FOR_INDICATION,
                                    [ 'class' => 'form-control', 'v-model' => 'met_for' ]
                                    )
                                !!}
                            </div>
                            <div class="col-md-6">

                                <label id="title">@{{ getLabel() }}</label>
                                {!! Form::text('indicated_by', null, [ 'class' => 'form-control',':placeholder' => 'getLabel()' ]) !!}
                            </div>
                        </div>
                    @else
                            {!! Form::hidden('code_shared', $code_shared ? $code_shared : old('code_shared') ) !!}
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <p class="p-l-5"><strong>Tipo de Pessoa</strong></p>

                            <input name="person_type" value="1" type="radio" id="person_type_1" checked="true" v-model="person_type">
                            <label for="person_type_1">Pessoa Física</label>

                            <input name="person_type" value="2" type="radio" id="person_type_2"  v-model="person_type">
                            <label for="person_type_2">Pessoa Jurídica</label>
                        </div>
                    </div>

                    <form-physics v-if="person_type == 1"></form-physics>
                    <form-legal v-else></form-legal>

                    @include('admin::pages.user_account.shared._form')

                    <div class="row">
                        <div class="col-md-6">
                            <p class="font-15">Reinvestir automaticamente sua rentabilidade mensal ?</p>

                            {!! Form::checkbox('automatic_application', true,null, [ 'id' => 'automatic_application' ]) !!}
                            <label for="automatic_application">Sim</label>
                        </div>
                    </div>


                </div>
            </div>

            <div class="m-b-0 card costum-card">
                <div class="header">
                    <h2 class="p-l-10">EMAIL E SENHA</h2>
                    <p class="m-t-10 p-t-10 p-l-10" style=" border-top: 1px solid #a5a5a5">Cadastre um email e senha</p>
                </div>
                <div class="body">

                    <div class="row">
                        <div class="col-md-4">
                            <label id="title">E-mail*</label>
                            {!! Form::email('user[email]', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-4">
                            <label id="title">Senha</label>
                            {!! Form::password('user[password]', [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-4">
                            <label id="title">Confirme a senha</label>
                            {!! Form::password('user[password_confirmation]', [ 'class' => 'form-control' ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 align-right">
                    {!! Form::button('<span>CONTINUAR</span> <i class="material-icons">arrow_right</i>',
                        [ 'id' => 'bt-continue', 'type' => 'submit', 'class' => 'btn btn-success btn-lg waves-effect' ]) !!}
                </div>
            </div>


        </div>
    </div>

    <div class="row">&nbsp;</div>
    <div class="row">&nbsp;</div>
    <!-- #END# TinyMCE -->

    {!! Form::close() !!}

@endsection




@push('scripts')
    <link href="{{ Module::asset('admin:app/new-account.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/new-account.js') }}"></script>

    <script src="{{ Module::asset('core:plugins/vue-the-mask/dist/vue-the-mask.js') }}"></script>


    <script src="{{ Module::asset('core:js\pages\forms\basic-form-elements.js') }}"></script>


    <script type="text/javascript">

        Vue.use(VueTheMask);

        var app = new Vue({
            el: '#app',
            data: {
                met_for: 1,
                person_type: 1,
                data_cep:{

                },

            },
            methods:{
                getLabel: function () {

                    let labels = {
                        1:'Quem ?',
                        2:'Termo Buscado ? ',
                        3:'Qual ? ',
                        4:'Qual ? ',
                    };

                    return labels[this.met_for];

                },
                getAndSetCep(){

                    var that = this;

                    Vue.set( that, 'data_cep', {
                        cep:that.data_cep.cep,
                        logradouro:"...",
                        complemento:"...",
                        bairro:"...",
                        localidade:"...",
                        uf:"...",
                        unidade:"...",
                        ibge:"...",
                        gia:"...",
                    });

                    $.getJSON("https://viacep.com.br/ws/"+this.data_cep.cep+"/json/",function (data) {
                        if (!("erro" in data)) {
                            Vue.set( that, 'data_cep', data);
                        }
                    });

                }
            },
        });

        $('input,select').attr('class', ' form-control costum-form-control');
        $('#bt-continue').on('click',App.admin.account.submitValidation);
    </script>



    @include('admin::pages.user_account._account_elements._form_legal')
    @include('admin::pages.user_account._account_elements._form_physics')
@endpush

