@extends('admin::layout.master')

@section('page_title', 'Usuários do Sistema')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">
                <div class="header">
                    <div class="row clearfix">

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <h2>Usuários</h2>
                            <small>Total: {{ User::count() }}</small>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 align-right pull-right">
                            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-6 pull-right">
                                @include('core::components._datatables_columns_dropdown')
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 pull-right">
                                <a href="{{ route('admin.users.create') }}" type="button"
                                   data-toggle="tooltip" title="Novo Usuário"
                                   class="btn btn-success btn-circle-lg waves-effect waves-circle waves-float">
                                    <i class="material-icons">add</i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="body">

                    {{--<div class="table-responsive">--}}
                    {!!
                       $dataTable->table([
                           'class' => 'datatable users-datatable table table-hover table-bordered bg-gray',
                           'style' => 'width: 100%',
                           'data-token' => csrf_token()
                       ])
                    !!}
                    {{--</div>--}}

                </div>
            </div>

        </div>
    </div>

@endsection

@push('scripts')

@include('core::components._datatables')
@include('core::components._confirm_delete')

{!! $dataTable->scripts() !!}

<link href="{{ Module::asset('admin:app/users.css') }}" rel="stylesheet">
<script src="{{ Module::asset('admin:app/users.js') }}"></script>
@endpush