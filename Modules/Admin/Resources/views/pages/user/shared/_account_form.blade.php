<div class="row form-horizontal">
    <div class="col-sm-8">

        {{-- Legal Person form --}}
        @if(Auth::getUser()->account->person_type == UserAccount::PERSON_TYPE_JURISTIC)

        <div class="form-group">
            <label for="company_name" class="col-sm-2 control-label">Razão Social</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[company_name]', null, [ 'class' => 'form-control' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="company_name" class="col-sm-2 control-label">CNPJ</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[cnpj]', null, [ 'class' => 'form-control mask-cnpj' ]) !!}
                </div>
            </div>
        </div>
        @endif

        {{-- Physical Person form --}}
        @if(Auth::getUser()->account->person_type == UserAccount::PERSON_TYPE_PHYSICAL)
            <div class="form-group">
                <label for="full_name" class="col-sm-2 control-label">Nome Completo</label>
                <div class="col-sm-8">
                    <div class="form-line">
                        {!! Form::text('account[full_name]', null, [ 'class' => 'form-control' ]) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="cpf" class="col-sm-2 control-label">CPF</label>
                <div class="col-sm-8">
                    <div class="form-line">
                        {!! Form::text('account[cpf]', null, [ 'class' => 'form-control mask-cpf' ]) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="rg" class="col-sm-2 control-label">RG</label>
                <div class="col-sm-8">
                    <div class="form-line">
                        {!! Form::text('account[rg]', null, [ 'class' => 'form-control' ]) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="birthday" class="col-sm-2 control-label">Nascimento</label>
                <div class="col-sm-8">
                    <div class="form-line">
                        {!! Form::date('account[birthday]', null, [ 'class' => 'form-control' ]) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="marital_status" class="col-sm-2 control-label">Estado Civil</label>
                <div class="col-sm-8">
                    <div class="form-line">
                        {!! Form::select('account[marital_status]', UserAccount::MARITAL_STATUS_LABELS, UserAccount::MARITAL_STATUS_NOT_MARRIED,
                            [ 'class' => 'form-control']
                            )
                        !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="profession" class="col-sm-2 control-label">Profissão</label>
                <div class="col-sm-8">
                    <div class="form-line">
                        {!! Form::text('account[profession]', null, [ 'class' => 'form-control' ]) !!}
                    </div>
                </div>
            </div>

        @endif
    </div>
</div>