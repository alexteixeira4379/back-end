<div class="row">
    <div class="col-md-8 col-lg-6 col-sm-12">
        <div class="row">
            <div class="col-md-12">
                <label id="title">Banco*</label>
                {!! Form::select('bank_account[bank_id]', Modules\Core\Entities\Bank::get()->pluck('friendly_name', 'id'),
                    (isset($bank_account) ? $bank_account->bank_id : null), [ 'class' => 'form-control show-tick' ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="form-group">
                    <label id="title">Proprietário*</label>
                    <div class="form-line">
                        {!! Form::select('bank_account[owner]',['1' => "Eu mesmo(a)", 2 => "Outra conta"],
                            1, [ 'class' => 'form-control show-tick' ]) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="form-group">
                    <label id="title">Nome do(a) proprietário(a)*</label>
                    <div class="form-line">
                        {!! Form::text('bank_account[owner_name]', null, [ 'class' => 'form-control' ]) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label id="title">Tipo de Documento*</label>
                {!! Form::select('bank_account[document_type]', Modules\Core\Entities\BankAccount::DOCUMENT_TYPE_LABELS,
                    (isset($bank_account) ? $bank_account->getOriginal('document_type') : null), [ 'class' => 'form-control show-tick' ]) !!}
            </div>
            <div class="col-md-4">

                <div class="form-group">
                    <label id="title">Número documento*</label>
                    <div class="form-line">
                        {!! Form::text('bank_account[document_number]', null,
                         [ 'class' => (isset($bank_account) ? ($bank_account->getOriginal('document_type') == Modules\Core\Entities\BankAccount::DOCUMENT_TYPE_CPF ?
                             'form-control mask-cpf' : 'form-control mask-cnpj'.' ' ) : 'form-control mask-cpf' ) ]) !!}
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <label id="title">Tipo de Conta*</label>
                {!! Form::select('bank_account[account_type]', Modules\Core\Entities\BankAccount::ACCOUNT_TYPE_LABELS,
                    (isset($bank_account) ? $bank_account->getOriginal('account_type') : null), [ 'class' => 'form-control show-tick' ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-8">
                <div class="form-group">
                    <label id="title">Agência*</label>
                    <div class="form-line">
                        {!! Form::text('bank_account[agency]', null, [ 'class' => 'form-control mask-number' ]) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4">
                <div class="form-group">
                    <label id="title">Agência Dígito*</label>
                    <div class="form-line">
                        {!! Form::text('bank_account[agency_digit]', null, [ 'class' => 'form-control mask-number' ]) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-8">
                <div class="form-group">
                    <label id="title">Conta*</label>
                    <div class="form-line">
                        {!! Form::text('bank_account[account_number]', null, [ 'class' => 'form-control mask-number' ]) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4">
                <div class="form-group">
                    <label id="title">Conta Dígito</label>
                    <div class="form-line">
                        {!! Form::text('bank_account[account_digit]', null, [ 'class' => 'form-control mask-number' ]) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <p class="font-15">Reinvestir automaticamente sua rentabilidade mensal ?</p>

                {!! Form::checkbox('account[automatic_application]', true,null, [ 'id' => 'automatic_application' ]) !!}
                <label for="automatic_application">Sim</label>
            </div>
        </div>
    </div>
</div>
