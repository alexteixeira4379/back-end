<div class="row form-horizontal" id="address_vue">
    <div class="col-sm-8">
        <div class="form-group">
            <label for="account[zip_code]" class="col-sm-2 control-label">CEP</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[zip_code]', null, [ 'class' => 'form-control mask-zip-code','v-model' => 'data_cep.cep', 'v-on:change' => 'getAndSetCep()' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="ccount[address]" class="col-sm-2 control-label">Endereço</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[address]', null, [ 'class' => 'form-control','v-model' => 'data_cep.logradouro' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="account[number]" class="col-sm-2 control-label">Número</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[number]', null, [ 'class' => 'form-control mask-number' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="account[neighborhood]" class="col-sm-2 control-label">Bairro</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[neighborhood]', null, [ 'class' => 'form-control','v-model' => 'data_cep.bairro' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="account[city]" class="col-sm-2 control-label">Cidade</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[city]', null, [ 'class' => 'form-control','v-model' => 'data_cep.localidade' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="account[state]" class="col-sm-2 control-label">Estado</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::select('account[state]', UserAccount::STATES_LABELS, null, [ 'class' => 'form-control','v-model' => 'data_cep.uf'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="account[telephone_1]" class="col-sm-2 control-label">Telefone 1</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[telephone_1]', null, [ 'class' => 'form-control mask-telephone' ]) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="account[telephone_2]" class="col-sm-2 control-label">Telefone 2</label>
            <div class="col-sm-8">
                <div class="form-line">
                    {!! Form::text('account[telephone_2]', null, [ 'class' => 'form-control mask-telephone' ]) !!}
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')

    <script type="text/javascript">

        var app = new Vue({
            el: '#address_vue',
            data: {
                data_cep:{
                    cep: "{{ isset($user) ? $user->account->zip_code:null  }}",
                    logradouro: "{{ isset($user) ? $user->account->address:null  }}",
                    bairro: "{{ isset($user) ? $user->account->neighborhood:null  }}",
                    localidade: "{{ isset($user) ? $user->account->city:null  }}",
                    uf: "{{ isset($user) ? $user->account->state:null  }}",
                },

            },
            methods:{

                getAndSetCep(){

                    var that = this;

                    Vue.set( that, 'data_cep', {
                        cep:that.data_cep.cep,
                        logradouro:"...",
                        complemento:"...",
                        bairro:"...",
                        localidade:"...",
                        uf:"...",
                        unidade:"...",
                        ibge:"...",
                        gia:"...",
                    });

                    $.getJSON("https://viacep.com.br/ws/"+this.data_cep.cep+"/json/",function (data) {
                        if (!("erro" in data)) {
                            Vue.set( that, 'data_cep', data);
                        }
                    });

                }

            },

            updated: function () {
                this.$nextTick(function () {
                    $('[name="account[state]"]').selectpicker('refresh');
                })
            }
        });

    </script>
@endpush