<div class="form-horizontal">
    <div class="col-xs-12">
        <div class="profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="https://gurayyarar.github.io/AdminBSBMaterialDesign/images/user-lg.jpg" id="display_profile_image"
                         alt="AdminBSB - Profile Image" style="width: 136px;height: 136px" />
                    <label class="user-imagem-upload">
                        Adicionar uma imagem
                        <i class="material-icons">
                            attach_file
                        </i>
                        {!! Form::file('profile_image',[ 'class'=>'hidden', 'id' => 'profile_image','accept' =>"image/x-png,image/gif,image/jpeg"]) !!}
                    </label>
                </div>
                <div class="content-area">
                    <h3>{{  Auth::getUser()->account->full_name }}</h3>
                   @if(Auth::getUser()->account->profession)
                        <p>{{  Auth::getUser()->account->profession }}</p>
                    @endif
                    <p>{{ Auth::getUser()->roles()->first()->display_name }}</p>
                </div>
            </div>
            <div class="profile-footer">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="Email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-8">
                                <div class="form-line">
                                    {!! Form::email('user[email]', isset($user) ? $user->email:null, [ 'class' => 'form-control',  'placeholder' => 'Email','autocomplete' => 'off' ]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="form-group">
                            <label for="Email" class="col-sm-2 control-label">Senha Antiga</label>
                            <div class="col-sm-8">
                                <div class="form-line">
                                    {!! Form::password('password_old', [ 'class' => 'form-control',
                                              'placeholder' => 'Deixe em branco p/ não alterar' ]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Email" class="col-sm-2 control-label">Nova senha</label>
                            <div class="col-sm-8">
                                <div class="form-line">
                                    {!! Form::password('password_new', [ 'class' => 'form-control',
                                              'placeholder' => 'Deixe em branco p/ não alterar' ]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Email" class="col-sm-2 control-label">Nova senha (confirmação)</label>
                            <div class="col-sm-8">
                                <div class="form-line">
                                    {!! Form::password('password_new2', [ 'class' => 'form-control',
                                                'placeholder' => 'Deixe em branco p/ não alterar' ]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@push('scripts')
    <script>
        document.getElementById('profile_image').onchange = function (evt) {
            var tgt = evt.target || window.event.srcElement,
                files = tgt.files;

            // FileReader support
            if (FileReader && files && files.length) {
                var fr = new FileReader();
                fr.onload = function () {
                    document.getElementById('display_profile_image').src = fr.result;
                }
                fr.readAsDataURL(files[0]);
            }

            // Not supported
            else {
                // fallback -- perhaps submit the input to an iframe and temporarily store
                // them on the server until the user's session ends.
            }
        }
    </script>
@endpush
