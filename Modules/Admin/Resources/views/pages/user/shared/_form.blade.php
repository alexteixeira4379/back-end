<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation">
            <a href="#profile_form" data-toggle="tab">
                <i class="material-icons">face</i> Perfil
            </a>
        </li>
        <li role="presentation">
            <a href="#account_form" data-toggle="tab">
                <i class="material-icons">account_circle</i> Dados Pessoais
            </a>
        </li>
        <li role="presentation">
            <a href="#address_form" data-toggle="tab">
                <i class="material-icons">edit_location</i> Endereço
            </a>
        </li>
        <li role="presentation">
            <a href="#bank_form" data-toggle="tab">
                <i class="material-icons">account_balance</i> Banco
            </a>
        </li>
    </ul>

    <div class="body">

        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Atenção!</strong> Após a alteração clique em salva para os campos serem alterados.
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="profile_form">
                <div class="row">
                    @include('admin::pages.user.shared._profile_form')
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="account_form">
                @include('admin::pages.user.shared._account_form')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="address_form">
                @include('admin::pages.user.shared._address_form')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="bank_form">
                @include('admin::pages.user.shared._bank_form')
            </div>
        </div>

    </div>
</div>
