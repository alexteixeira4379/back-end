@extends('admin::layout.master')

@section('page_title', 'Cadastrar Usuário')

@section('content')

    {!! Form::open(['route' => [ 'admin.users.store' ]]) !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">
                <div class="header">
                    <h2>
                        CADASTRAR USUÁRIO
                    </h2>
                </div>
            </div>
            @include('admin::pages.user.shared._form')


            <div class="card">
                <div class="header">
                    <h2>CADASTRO DE SENHA</h2>
                </div>
                <div class="body">

                    <div class="row">
                        <div class="col-md-4">
                            <label id="title">Senha Nova</label>
                            {!! Form::password('password', [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-4">
                            <label id="title">Senha Nova (confirmação)</label>
                            {!! Form::password('password_confirmation', [ 'class' => 'form-control' ]) !!}
                        </div>
                    </div>

                </div>
            </div>

            @include('admin::shared._save_button')

        </div>
    </div>
    <!-- #END# TinyMCE -->

    {!! Form::close() !!}

@endsection

@push('scripts')
<link href="{{ Module::asset('admin:app/users.css') }}" rel="stylesheet">
<script src="{{ Module::asset('admin:app/users.js') }}"></script>
@endpush