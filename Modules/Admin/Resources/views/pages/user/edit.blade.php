@extends('admin::layout.master')

@section('page_title', 'Editar Usuário ' . $user->name)

@section('content')

    {!! Form::model($user, ['route' => ['admin.users.update', $user->id]]) !!}
    {!! Form::hidden('_method', 'put') !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


            @include('admin::pages.user.shared._form')

            @include('admin::shared._save_button')

        </div>
    </div>
    <!-- #END# TinyMCE -->

    {!! Form::close() !!}

@endsection

@push('scripts')

    <link href="{{ Module::asset('admin:app/users.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/users.js') }}"></script>
@endpush