{!! Form::open([ 'route' => [ 'admin.recipes.store' ],'files' => true, 'recipes' => 'create']) !!}
<div class="modal fade" id="recipeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ENTRE COM OS DADOS PARA UMA NOVA RECEITA</h4>
            </div>
            <div class="modal-body">

                <div class="body  form-horizontal">

                    <div class="row">&nbsp;</div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">VALOR: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('value', null, [ 'class' => 'form-control mask-money-real' ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">DATA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::date('date', null, [ 'class' => 'form-control' ]) !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">CEDENTE/CESSIONÁRIO:  </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('assignee_assignor', null, [ 'class' => 'form-control' ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">NATUREZA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('kind', null, [ 'class' => 'form-control' ]) !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="submit" class="btn btn-success btn-lg waves-effect">ENVIAR</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')
    <script type="application/javascript">

        $(document).ready(function () {



            $('.mask-money-real').inputmask( 'currency',{"autoUnmask": true,
                radixPoint:",",
                removeMaskOnSubmit: true,
                groupSeparator: ".",
                allowMinus: false,
                prefix: 'R$ ',
                digits: 2,
                digitsOptional: false,
                rightAlign: false,
                unmaskAsNumber: true,
                min:0,
            });

        });

    </script>
@endpush