{!! Form::open(['route' => ['admin.taxations.update', null],'files' => true, 'taxations' => 'edit']) !!}
{!! Form::hidden('_method', 'put') !!}
<div class="modal fade" id="editModalTaxation" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg " role="document" id="taxationApp">
        <div v-if="_.isEmpty(response_message)" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabelTaxation">Solicitação de aporte</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div v-if="isLoader" class="body">
                    <div class="preloader">
                        <div class="spinner-layer pl-teal">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else class="body form-horizontal">


                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">VALOR: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('value', null,
                                    [
                                        'class' => 'form-control mask-money-real',
                                        'v-model' => 'taxation.value'
                                    ])
                                !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">DATA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::date('date', null, [ 'class' => 'form-control','v-model' => 'taxation.date' ]) !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">TRIBUTO:  </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('tribute', null, [ 'class' => 'form-control','v-model' => 'taxation.tribute'  ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3 control-label">NATUREZA: </label>
                        <div class="col-sm-8">
                            <div class="form-line">
                                {!! Form::text('kind', null, [ 'class' => 'form-control','v-model' => 'taxation.kind' ]) !!}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                <button type="button" @click="formSubmit()" class="btn btn-success waves-effect">SALVAR</button>
            </div>
        </div>
        <div v-else class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uploadModalLabelTaxation">Solicitação de aporte</h4>
            </div>
            <div class="modal-body align-center" >
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div class="body">
                    <div v-if="response_message.status == 'success'">
                        <h5>TUDO CERTO... OS DADOS FORAM SALVOS!</h5>
                        <div class="alert bg-green">
                            @{{response_message.message}}
                        </div>
                    </div>
                    <div v-else >
                        <h5>OPS... Algo de deu errado</h5>
                        <div class="alert bg-danger">
                            @{{response_message.message}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" @click="setPatterns()" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@push('scripts')

    <script type="application/javascript">

        var taxationApp = new Vue({
            el: '#taxationApp',
            data: {
                isLoader: true,
                response_message: {},
                taxation: {},
            },
            methods: {

                /*
                  Set default of this application after actions
                */
                setPatterns(){

                    this.response_message = {};
                    this.taxation = {};
                    this.isLoader = true;

                },

                /*
                 Ajax with taxation data
               */
                getTaxations(id){

                    this.setPatterns();

                    var that = this;

                    $.ajax({
                        url: "{{ route('api.taxations.show',null) }}" + "/" +id,
                        method: 'GET',
                        beforeSend: function () {
                            Vue.set(that,'isLoader', true);
                        }
                    })
                    /*
                       Do something when it completes successfully
                    */
                        .done(function(response)
                        {
                            that.taxation = response.data;
                        })
                        /*
                           Do something when it fails
                        */
                        .fail(function(response)
                        {
                            console.log(response.responseJSON);
                        })
                        /*
                           Do something ever
                        */
                        .always(function(response)
                            {
                                Vue.set(that,'isLoader', false);
                            }
                        );

                },

                formSubmit: function () {


                    var that = this;
                    var taxation = Object.assign({}, this.taxation);
                    this.setPatterns();

                    $.ajax({

                        url: "{{ route('api.taxations.update',null) }}" + "/" + taxation.id,
                        method: 'PUT',
                        dataType: 'json',
                        data: taxation,

                        beforeSend: function () {
                            // Code before send here
                        }
                    })
                    /*
                      Do something when it completes successfully
                   */
                        .done(function(response)
                        {
                            if(typeof response == 'string')
                                that.response_message = JSON.parse(response);
                            else
                                that.response_message = response;

                            LaravelDataTables.taxations.ajax.reload()
                        })
                        /*
                           Do something when it fails
                        */
                        .fail(function(response)
                        {
                            if(typeof response.responseJSON == 'string')
                                that.response_message = JSON.parse(response.responseJSON);
                            else
                                that.response_message = response.responseJSON;
                        })
                        /*
                           Do something ever
                        */
                        .always(function(response)
                            {
                                Vue.set(that,'isLoader', false);
                            }
                        );

                }
            },
            updated: function () {

            }
        });

    </script>
@endpush