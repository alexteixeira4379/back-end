@extends('admin::layout.master')

@section('page_title', 'Finanças')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">

                <div class="body">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab_recipes" data-toggle="tab">
                                <i class="material-icons">description</i> Receitas
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_expenses" data-toggle="tab">
                                <i class="material-icons">description</i> Despensas
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_taxations" data-toggle="tab">
                                <i class="material-icons">description</i> Impostos e taxas
                            </a>
                        </li>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 align-right pull-right" style="margin: 5px;">
                            <button
                                    type="button"
                                    class="btn btn-success waves-effect"
                                    data-toggle="modal" data-target="#recipeModal">
                                <i class="material-icons">add</i><span>Receitas</span>
                            </button>
                            <button
                                    type="button"
                                    class="btn btn-success waves-effect"
                                    data-toggle="modal" data-target="#expenseModal">
                                <i class="material-icons">add</i><span>Despensas</span>
                            </button>
                            <button
                                    type="button"
                                    class="btn btn-success waves-effect"
                                    data-toggle="modal" data-target="#taxationModal">
                                <i class="material-icons">add</i><span>Impostos e taxas</span>
                            </button>
                        </div>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tab_recipes">
                            {{--<div class="table-responsive">--}}
                            {!!
                               $recipesDataTable->table([
                                   'class' => 'datatable table table-hover table-bordered bg-gray',
                                   'style' => 'width: 100%',
                                   'data-token' => csrf_token(),
                                   'id' => 'recipes'
                               ])
                            !!}
                            {{--</div>--}}
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_expenses">
                            {{--<div class="table-responsive">--}}
                            {!!
                               $expensesDataTable->table([
                                   'class' => 'datatable table table-hover table-bordered bg-gray',
                                   'style' => 'width: 100%',
                                   'data-token' => csrf_token(),
                                   'id' => 'expenses'
                               ])
                            !!}
                            {{--</div>--}}
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_taxations">
                            {{--<div class="table-responsive">--}}
                            {!!
                               $taxationsDataTable->table([
                                   'class' => 'datatable table table-hover table-bordered bg-gray',
                                   'style' => 'width: 100%',
                                   'data-token' => csrf_token(),
                                   'id' => 'taxation'
                               ])
                            !!}
                            {{--</div>--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('admin::pages.finance._recipe_elements._create')
    @include('admin::pages.finance._recipe_elements._edit')

    @include('admin::pages.finance._expense_elements._create')
    @include('admin::pages.finance._expense_elements._edit')

    @include('admin::pages.finance._taxation_elements._create')
    @include('admin::pages.finance._taxation_elements._edit')


@endsection

@push('scripts')



    @include('core::components._datatables')
    @include('core::components._confirm_delete')

    {!! $recipesDataTable->scripts() !!}
    {!! $expensesDataTable->scripts() !!}
    {!! $taxationsDataTable->scripts() !!}

    <link href="{{ Module::asset('admin:app/finances.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/finances.js') }}"></script>

@endpush