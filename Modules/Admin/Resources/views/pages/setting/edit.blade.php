@extends('admin::layout.master')

@section('page_title', 'Configurações do Sistema')

@section('content')

    {!! Form::model($setting, ['route' => ['admin.settings.update', $setting->id]]) !!}
    {!! Form::hidden('_method', 'put') !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card">
                <div class="header">
                    <h2>
                        CONFIGURAÇÕES DO SISTEMA
                    </h2>
                </div>
                <div class="body">

                    <div class="row">
                        <div class="col-md-3">
                            <label id="title">Título do Sistema*</label>
                            {!! Form::text('site_title', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-3">
                            <label id="title">Texto de Rodapé</label>
                            {!! Form::text('footer_text', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <label id="title">Nome da Empresa</label>
                            {!! Form::text('company_name', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-3">
                            <label id="title">CNPJ</label>
                            {!! Form::text('company_cnpj', null, [ 'class' => 'form-control mask-number' ]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <label id="title">Rentabilidade Mínima</label>
                            <div style="display:flex">
                                {!! Form::text('min_profitability', null, [ 'class' => 'mask-profitability form-control' ]) !!}
                                <p class="align-center font-17" style="min-width: 112px;line-height: 2;letter-spacing: 2px;"> \ 100 = %</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label id="title">Tipo de rentabilidade</label>

                            <div class="form-line">
                                {!! Form::select('profitability_type', Setting::PROFITABILITY_STATUS_LABELS, null,
                                    [ 'class' => 'form-control']
                                    )
                                !!}
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label id="title">Dia de fechamento</label>
                        <div class="form-group">
                            <div class="form-line">
                                {!! Form::selectRange('day_operation',  1, 31,null,[ 'class' => 'form-control' ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <h2>
                        CONFIGURAÇÕES DO SMTP
                    </h2>
                </div>
                <div class="body">

                    <div class="row">
                        <div class="col-md-3">
                            <label id="title">Host do SMTP*</label>
                            {!! Form::text('smtp_host', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-3">
                            <label id="title">Username*</label>
                            {!! Form::text('smtp_username', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <label id="title">Senha do SMTP*</label>
                            {!! Form::text('smtp_password', null, [ 'class' => 'form-control' ]) !!}
                        </div>
                        <div class="col-md-3">
                            <label id="title">Porta do SMTP*</label>
                            {!! Form::text('smtp_port', null, [ 'class' => 'form-control mask-number' ]) !!}
                        </div>
                        <div class="col-md-3">
                            <label id="title">Tipo de Criptografia*</label>
                            {!! Form::select('smtp_encryption_type', AppHelper::SMTP_ENCRYPTION_TYPES, null,
                                [ 'class' => 'form-control show-tick' ]) !!}
                        </div>
                    </div>

                </div>
            </div>

            <div class="card">
                <div class="header">
                    <h2>
                        IDENTIDADE VISUAL
                    </h2>
                </div>
                <div class="body">

                    <div class="row">
                        <div class="col-md-2">
                            <label id="title">Cor Principal*</label>
                            {!! Form::color('primary_color', null, [ 'class' => 'form-control' ]) !!} <br>
                            Atual: <div style="width: 30px; height: 30px; background-color: {{ ConfigHelper::get('primary_color') }};"></div>
                        </div>
                        <div class="col-md-2">
                            <label id="title">Cor Secundária*</label>
                            {!! Form::color('secondary_color', null, [ 'class' => 'form-control' ]) !!} <br>
                            Atual: <div style="width: 30px; height: 30px; background-color: {{ ConfigHelper::get('secondary_color') }};"></div>
                        </div>
                    </div>

                </div>
            </div>

            @include('admin::shared._save_button')

        </div>
    </div>

    {!! Form::close() !!}

@endsection

@push('scripts')
    <link href="{{ Module::asset('admin:app/settings.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/settings.js') }}"></script>

    <script type="application/javascript">

        $(document).ready(function () {

            $('.mask-profitability').inputmask( '9.99');

        });

    </script>
@endpush

