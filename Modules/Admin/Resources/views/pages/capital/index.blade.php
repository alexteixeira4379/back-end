@extends('admin::layout.master')

@section('page_title', 'Meu Capital')

@section('content')

    <div class="row clearfix">

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <div class="icon bg-green">
                    <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                </div>
                <div class="content">
                    <div class="text">Saldo</div>
                    <div class="number">{{ $capital->balance_to_real }}</div>
                </div>
            </div>
        </div>

    </div>


    <div class="row clearfix">
        <div class="col-xs-12">

            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-3 col-sm-2 col-xs-12">

                            <div class="form-group">
                                <label for="company_name" class="col-sm-3 control-label">Filtrar Data (Mês/Ano)</label>
                                <div class="col-sm-5">
                                    <div class="form-line">
                                        {!! Form::text('date_filter', null, [
                                        'class' => 'form-control to filters',
                                        'autocomplete'=> 'off',
                                        'placeholder'=> today()->format('m-Y'),
                                        ]) !!}
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    {!!
                     $dataTable->table([
                         'class' => 'datatable table table-bordered bg-gray',
                         'style' => 'width: 100%',
                         'data-token' => csrf_token()
                     ])
                    !!}

                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <link href="{{ Module::asset('core:plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" rel="stylesheet">
    <script src="{{ Module::asset('core:vendor/bootstrap/js/bootstrap.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ Module::asset('core:plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>



    @include('core::components._datatables')
    @include('core::components._confirm_delete')

    {!! $dataTable->scripts() !!}


    <script src="{{ Module::asset('core:plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>



    <link href="{{ Module::asset('admin:app/capital.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/capital.js') }}"></script>


    <script type="text/javascript">


        $(function () {
            initCharts();
        });

        //Charts
        function initCharts() {
            //Chart Bar

            //Chart Line
            $('.chart.chart-line').sparkline(undefined, {
                type: 'line',
                width: '60px',
                height: '45px',
                lineColor: '#fff',
                lineWidth: 1.3,
                fillColor: 'rgba(0,0,0,0)',
                spotColor: 'rgba(255,255,255,0.40)',
                maxSpotColor: 'rgba(255,255,255,0.40)',
                minSpotColor: 'rgba(255,255,255,0.40)',
                spotRadius: 3,
                highlightSpotColor: '#fff'
            });
        }


        var FromEndDate = new Date();


        $('.to').datepicker({
            autoclose: true,
            minViewMode: 1,
            format: 'mm/yyyy',
        }).on('changeDate', function(selected) {

            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('.from').datepicker('setEndDate', FromEndDate);

            LaravelDataTables.dataTableBuilder.draw();
        });







        // $(function () {
        //     $(".sparkline").each(function () {
        //         var $this = $(this);
        //         $this.sparkline('html', $this.data());
        //     });
        //
        //
        // });


    </script>

@endpush