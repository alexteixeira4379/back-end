@extends('admin::layout.master')

@section('page_title', 'Dashboard')

@section('content')


    <div class="row clearfix">
        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">

            @include('admin::shared._profitability')

        </div>
        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">

            <div class="card">
                <div class="header">
                    <h2>
                        Seu Capital

                        @if( Permission::canDo( Permission::ACTION_LIST, Permission::CAPITAL) )
                            <a class="text-primary pull-right" href="{{ route('admin.capital.index') }}"
                               style="font-size: 14px;display: flex;align-items: center;text-decoration: none;">
                                <i class="material-icons">search</i>Ver extrato
                            </a>
                        @endif

                    </h2>

                </div>
                <div class="body" style="padding: 0 10px">
                    <div class="row clearfix">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            @include('admin::shared._capital')
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            @include('admin::shared._last_capital',\Modules\Core\Entities\Capital::getLastCapital())
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    {!! \Modules\Core\Entities\Profitability::getPrevProfitability() !!}
                    {{--@include('admin::shared._prev_profitability')--}}
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    @include('admin::shared._social_capital')
                </div>
            </div>


        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">

            @include('admin::shared._movement', $movement )

        </div>
        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">
            @include('admin::shared._indication')
        </div>
    </div>




@endsection

@push('scripts')

    <script src="{{ Module::asset('core:plugins/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ Module::asset('core:plugins/flot-charts/jquery.flot.time.js') }}"></script>

    <script src="{{ Module::asset('core:plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

    <link href="{{ Module::asset('admin:app/dashboard.css') }}" rel="stylesheet">
    <script src="{{ Module::asset('admin:app/dashboard.js') }}"></script>

    <script type="text/javascript">


    </script>


@endpush