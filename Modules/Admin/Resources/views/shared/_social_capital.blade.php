<div class="card">
    <div class="header">
        <h2>
            Capital Social

        </h2>
    </div>
    <div class="body">
        <p class="text font-15">{!! \Modules\Core\Helpers\DateHelper::getColumnDateMonthYear(\Carbon\Carbon::now()->subMonth(1)) !!}:
            <strong class="number"> {!! \Modules\Core\Helpers\CapitalHelper::getShareCapital(\Carbon\Carbon::now()->subMonth(1)) !!}</strong></p>
        <p class="text font-15">Atual <strong class="number">{!! \Modules\Core\Helpers\CapitalHelper::getCurrentShareCapital() !!}</strong></p>
        <p>&ensp;</p>

    </div>
</div>