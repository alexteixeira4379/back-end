<div class="row">
    <div class="col-md-6">
        {!! Form::button('<span>SALVAR</span> <i class="material-icons">save</i>',
            [ 'type' => 'submit', 'class' => 'btn btn-success btn-lg waves-effect' ]) !!}
    </div>

    @if(\Request::url() !== \URL::previous())
    <div class="col-md-6">
        <a href="{{ \URL::previous() }}" class="btn btn-default btn-sm waves-effect pull-right">
            <i class="material-icons">arrow_back</i>
            <span>VOLTAR</span>
        </a>
    </div>
    @endif
</div>

<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>