<div id="movement">
    <div class="card">
        <div class="header" style="background-color: #f9f9f9"><h2>Movimentações</h2></div>
        <div class="body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Data</th>
                    <th>Aporte</th>
                    <th>Resgate</th>
                </tr>
                </thead>
                <tbody>

                @if(! $movement['hasMovement'] )
                    <tr><td colspan="3">Não há movimentações deate mês </td></tr>

                @else @foreach(\Modules\Core\Helpers\TransactionsHelper::getTransactionExtract( $movement['query'] ) as $column)

                    <tr>
                        <td>{!! \Modules\Core\Helpers\TransactionsHelper::getColumnDate($column) !!}</td>
                        <td>R$ {!! number_format( (clone $column->transactions)->where('typeable_type','=',\Modules\Core\Entities\Withdrawal::class)
                            ->pluck('value')
                            ->sum(), 2, ',', '.') !!}
                        </td>
                        <td>R$ {!! number_format( (clone $column->transactions)->where('typeable_type','=',\Modules\Core\Entities\Contribution::class)
                            ->pluck('value')
                            ->sum(), 2, ',', '.') !!}
                        </td>
                        <td></td>
                    </tr>
                @endforeach

                @endif

                </tbody>
            </table>
        </div>
    </div>

</div>


