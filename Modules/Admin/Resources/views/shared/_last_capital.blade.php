<div id="last-capital">
    <div class="info-box" style="box-shadow: none;    margin: 0;    height: initial;">
        <div class="icon bg-white" style="max-height: 70px">
            <span class="chart chart-line last-capital">9,4,6,5,6,4,7,3</span>
        </div>
        <div class="content">
            <div class="text">Fechamento em {!! $label!!}</div>
            <div class="number">{!! $value !!}</div>
        </div>
    </div>
</div>


@push('scripts')

<script type="application/javascript">

    $(document).ready(function () {
        $('.last-capital').sparkline(undefined, {
            type: 'line',
            width: '60px',
            height: '45px',
            lineColor: 'teal',
            lineWidth: 1.3,
            fillColor: 'rgba(0,0,0,0)',
            spotColor: 'rgba(255,255,255,0.40)',
            maxSpotColor: 'green',
            minSpotColor: 'rgba(255,255,255,0.40)',
            spotRadius: 3,
            highlightSpotColor: 'black'
        });
    });


</script>
@endpush

