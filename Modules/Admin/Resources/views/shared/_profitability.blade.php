<div class="card" id="profitabilityApp">
    <div class="header">
        <div class="row">
            <div class="col-md-6">
                Grafico de Rentabilidade
            </div>
            @if( !isset($year_filter_select) )
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-1 control-label align-right">Rent. Em:</label>
                        <div class="col-sm-11">
                            <div class="form-line">
                                {!! Form::selectRange('year_filter',  2017,
                                    \Carbon\Carbon::now()->year,null,
                                    ['class' => 'form-control except','v-model' => 'year_filter', '@change'=>'getData' ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
    <div v-if="_.isEmpty(response_message)" class="body">
        @if( isset($year_filter_select) )
            <div class="row p-b-20 p-t-20">&ensp;</div>
        @endif

        {{--<div v-show="isLoader" class="body">--}}
            {{--<div class="preloader">--}}
                {{--<div class="spinner-layer pl-teal">--}}
                    {{--<div class="circle-clipper left">--}}
                        {{--<div class="circle"></div>--}}
                    {{--</div>--}}
                    {{--<div class="circle-clipper right">--}}
                        {{--<div class="circle"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div v-show="!isLoader">--}}
            <div class="rentabilidades chart chart-sm chart-active" style="height:230px" id="graph_prof"></div>
            <div class="row">&ensp;</div>
            <div class="row" style=" padding: 6px; ">&ensp;</div>
            <div class="row visible-md" style=" padding: 10px; ">&ensp;</div>
            <div class="row visible-sm" style=" padding: 10px; ">&ensp;</div>
        {{--</div>--}}

    </div>
    <div v-else class="body">
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="body">
            <h5>OPS... Algo de deu errado</h5>
            <div class="alert bg-danger">
                @{{response_message.message}}
            </div>
        </div>
    </div>
</div>




@push('scripts')

<script type="application/javascript">

    // $('[name="year_filter"]')[0].dispatchEvent(new Event("change"))


    var profitabilityApp = new Vue({
        el: '#profitabilityApp',
        data: {
            isLoader: true,
            response_message: {},
            year_filter: {{ isset($year_filter_select) ? $year_filter_select:\Carbon\Carbon::now()->year}},
            data_profitability: [ ],
        },
        methods: {
            /*
              Set default of this application after actions
            */
            setPatterns(){

                this.response_message = {};
                this.isLoader = true;
                this.data_profitability = [ ];

            },
            initCharts(){

                var ticks = [];

                var dataFlot = [
                    {
                        data:[],
                        color: "green"
                    },
                    {
                        data:[],
                        color: "gray"
                    }

                ];

                $.each(this.data_profitability, function(i, obj) {
                    //use obj.id and obj.name here, for example:
                    //alert(obj.name);

                    let microtimestamp = (new Date(obj.created_at)).getTime();

                    var o = [];
                    o.push(microtimestamp);
                    o.push(obj.profitability);

                    var o2 = [];
                    o2.push(microtimestamp);
                    o2.push(obj.min_profitability);

                    dataFlot[0].label="";
                    dataFlot[1].label="Mínimo Garantido";
                    ticks.push(microtimestamp);

                    dataFlot[0].data.push(o);
                    dataFlot[1].data.push(o2);
                });

                var monthNames = ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"];

                let graph = $.plot('#graph_prof', dataFlot, {
                    responsive: true,
                    maintainAspectRatio: false,
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2
                        },
                        points: {
                            show: true
                        },
                        shadowSize: 0
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        borderColor: 'rgba(0,0,0,0.1)',
                        borderWidth: 1,
                        labelMargin: 15,
                        backgroundColor: 'transparent'
                    },
                    xaxis: {
                        mode: "time",
                        timeformat: "%b",
                        color: 'rgba(0,0,0,0)',
                        ticks: ticks,
                        monthNames: monthNames
                    },
                    legend: {
                        show: false
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: function (label, xval, yval) {
                            yval = yval * 1;
                            var cls = "text-chart-rent-aplicada ";
                            var comp = $.plot.formatDate(new Date(xval), '%b ',monthNames);
                            if(label!=""){
                                cls = "text-chart-rent-minima ";
                                var content = '<p class="'+cls+'mb-0 text-white">'+"%s "+ yval.toFixed(4).replace(".",",")+"%"+"</p>";
                                comp = ""
                            }else{
                                var content = '<p class="'+cls+'mb-0 text-white">'+comp +yval.toFixed(4).replace(".",",")+"%"+"</p>";
                            }

                            return content;
                        }
                    }
                });

            },
            /*
             Ajax with contribution data
           */
            getData(){

                // this.setPatterns();

                var that = this;
                var year_filter = this.year_filter;

                $.ajax({
                    dataType: "json",
                    url: "{{ route('api.profitability.show',null) }}" + "/" +year_filter,
                    method: 'GET',
                    beforeSend: function () {
                        /*
                           Handle something
                        */
                    }
                })
                /*
                   Do something when it completes successfully
                */
                    .done(function(response)
                    {
                        that.data_profitability = response.data;
                        that.initCharts();
                    })
                    /*
                       Do something when it fails
                    */
                    .fail(function(response)
                    {
                        if(typeof response.responseJSON == 'string')
                            that.response_message = JSON.parse(response.responseJSON);
                        else
                            that.response_message = response.responseJSON;
                    })
                    /*
                       Do something ever
                    */
                    .complete(function(response)
                    {

                    });


            },

        },
        updated: function () {
            this.$nextTick(function () {
            })
        },
        mounted() {
            this.getData();
        }
    });

</script>
@endpush

