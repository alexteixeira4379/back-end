<div id="movimentações">
    <div class="card">
        <div class="header" style="background-color: #f9f9f9"><h2>Minhas Indicações (MD)</h2></div>
        <div class="body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th> Nome/Razão Social</th>
                    <th>Resultado (0,5%)</th>
                    <th>Extrato</th>
                </tr>
                </thead>
                <tbody>

                @if( \Auth::getUser()->indications()->get()->isEmpty() )
                    <tr>
                        <td colspan="3" class="align-center">
                            <p class="p-b-10 p-t-10"> Não há indicações </p>
                        </td>
                    </tr>
                @endif

                @foreach(\Auth::getUser()->indications()->get() as $column)
                    <tr>
                        <td>{!! $column->name !!}</td>
                        <td>R$ {!! '00' !!}
                        </td>
                        <td>R$ {!! '00' !!}
                        </td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>


