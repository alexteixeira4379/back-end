<div class="card">
    <div class="header">
        <h2>
          Resultado de {!! $date_label !!}
        </h2>
    </div>
    <div class="body">
        <p class="text font-15">Rentabilidade: <strong class="number">{!! $profitability !!}%</strong></p>
        <p class="text font-15">Resultado sobre meu capital: : <strong class="number">{!! $about_my_capital !!}</strong></p>
        <p class="text font-15">Valor Recebido:  <strong class="number">{!! $amount_received !!}</strong></p>

    </div>
</div>