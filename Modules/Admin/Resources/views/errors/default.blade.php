@include('admin::layout.header', [ 'extra_class' => 'five-zero-zero' ])

<div class="five-zero-zero-container">
    <div class="error-code">{{ isset($error_code) ? $error_code : 500 }}</div>
    <div class="error-message">
        Internal Server Error <br>
        <span class="font-16">Detalhes:
            <span class="col-red">
                {{ isset($error) ? $error : 'Sem maiores detalhes' }}
            </span>
        </span>
    </div>
    <div class="button-place">
        <a href="{{ route('admin.dashboard.index') }}" class="btn btn-default btn-lg waves-effect">
            <span>TENTAR NOVAMENTE</span> <i class="material-icons">refresh</i>
        </a>
    </div>
</div>

@include('admin::layout.footer')