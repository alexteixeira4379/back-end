<?php

namespace Modules\Admin\DataTables;

use Modules\Core\Entities\File;
use Modules\Core\Entities\FileCategory;
use Modules\Core\Entities\Permission;
use Modules\Core\Helpers\DataTableHelper;
use Yajra\DataTables\Services\DataTable;


/**
 * Class FileCategoriesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class FileCategoriesDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
            ->editColumn( 'name', function($data) { return $this->getColumnName($data); } )
            ->editColumn( 'id', function($data) { return $this->getColumnId($data); } )
            ->rawColumns([ 'action']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        return \FileCategory::select();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [ 'data' => 'id', 'name' => 'id', 'title' => 'ID', 'width' => '5%' ],
            [ 'data' => 'name', 'name' => 'name', 'title' => 'Nome' ],
            [ 'data' => 'action', 'name' => 'action', 'title' => 'Ação', 'width' => '12%' ]
        ];
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'paging' => true,
            'searching' => true,
            'responsive' => true,
            'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
            'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
            'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
            'dom' => DataTableHelper::DOM,
            'columnDefs' => [
                [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
            ],
            'buttons' => [ 'copy', 'csv', 'excel', 'print' ], // ['export', 'pdf', 'print'],
            'order' => [ 0, 'desc' ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Arquivos_' . date('d-m-Y__H-m');
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnName( $data )
    {
        return str_limit($data->name, 30);
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnId( $data )
    {
        return $data->id;
    }

    /**
     * Customize the column
     *
     * @param \FileCategory $data
     *
     * @return string
     */
    private function getColumnAction( \FileCategory $data )
    {
        $buttons = DataTableHelper::getActions([

            DataTableHelper::ACTION_EDIT => [
                'button' => '<i class="material-icons ">edit</i>',
                'route' => '#',
                'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::FILE_CATEGORY), // or false
                'extra_attrs' => [
                    'onclick' => "app.getCategories({$data['id']})",
                    'data-toggle' => 'modal',
                    'data-target' => '#editModal',
                ]
            ],
            DataTableHelper::ACTION_DESTROY => [
                'button' => '<i class="material-icons bt-destroy ">delete</i>',
                'route' => route( 'admin.file-categories.destroy', $data['id'] ),
                'permission' => \Permission::canDo(\Permission::ACTION_DESTROY, \Permission::FILE), // or false
            ],
        ], false);


        return implode("\n", $buttons);
    }

}
