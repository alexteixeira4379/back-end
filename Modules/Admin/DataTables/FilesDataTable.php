<?php

namespace Modules\Admin\DataTables;

use Modules\Core\Entities\File;
use Modules\Core\Entities\Permission;
use Modules\Core\Helpers\DataTableHelper;
use Yajra\DataTables\Services\DataTable;


/**
 * Class FilesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class FilesDataTable extends DataTable
{

	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables($query)
			->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
			->editColumn( 'name', function($data) { return $this->getColumnName($data); } )
			->editColumn( 'file_name', function($data) { return $this->getColumnFileName($data); } )
			->editColumn( 'date_process', function($data) { return $this->getColumnDateProcess($data); } )
			->editColumn( 'status', function($data) { return $this->getColumnStatus($data); } )
			->editColumn( 'file_type', function($data) { return $this->getColumnFileType($data); } )
			->rawColumns([ 'action', 'date_process', 'status', ]);
	}

	/**
	 * Get the query object to be processed by dataTables.
	 *
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
	 */
    public function query()
    {
	    return File::with('category')->select();
    }

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
	        [ 'data' => 'id', 'name' => 'id', 'title' => 'ID', 'width' => '5%' ],
	        [ 'data' => 'name', 'name' => 'name', 'title' => 'Nome' ],
	        [ 'data' => 'file_name', 'name' => 'file_name', 'title' => 'Nome (arquivo)' ],
	        [ 'data' => 'size', 'name' => 'size', 'title' => 'Tamanho' ],
	        [ 'data' => 'status', 'name' => 'status', 'title' => 'Status' ],
	        [ 'data' => 'created_at', 'name' => 'created_at', 'title' => 'Upload' ],
	        [ 'data' => 'date_process', 'name' => 'date_process', 'title' => 'Processamento' ],
	        [ 'data' => 'category.name', 'name' => 'category.name', 'title' => 'Categoria' ],
//	        [ 'data' => 'file_type', 'name' => 'file_type', 'title' => 'Tipo' ],
	        [ 'data' => 'action', 'name' => 'action', 'title' => 'Ação', 'width' => '12%' ]
        ];
    }

	/**
	 * Get default builder parameters.
	 *
	 * @return array
	 */
	protected function getBuilderParameters()
	{
		return [
			'paging' => true,
			'searching' => true,
			'responsive' => true,
			'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
			'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
			'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
			'dom' => DataTableHelper::DOM,
			'columnDefs' => [
		        [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
		    ],
			'buttons' => [ 'copy', 'csv', 'excel', 'print' ], // ['export', 'pdf', 'print'],
			'order' => [ 0, 'desc' ],
		];
	}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Arquivos_' . date('d-m-Y__H-m');
    }

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnName( $data )
	{
		return str_limit($data->name, 30);
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnFileName( $data )
	{
		return str_limit($data->file_name, 30);
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnDateProcess( $data )
	{
		if($data->is_sheet)
		{
			if(is_null($data->date_process))
				return '<span style="font-style: italic">nunca</span>';

			return $data->date_process;
		}

		return '<span style="font-style: italic">N/A</span>';
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnStatus( $data )
	{
		if($data->is_sheet)
		{
			switch ($data->getOriginal('status') )
			{
				case File::FILE_UNPROCESSED:
					return "<span class='badge bg-light-blue col-white'>{$data->status}</span>";

				case File::FILE_PROCESSING:
					return "<span class='badge bg-orange col-white'>{$data->status}</span>";

				case File::FILE_SCHEDULED:
					return "<span class='badge bg-amber col-white'>{$data->status}</span>";

				case File::FILE_PROCESSED:
					return "<span class='badge bg-green col-white'>{$data->status}</span>";

				case File::FILE_ERROR:
					return "<span class='badge bg-red col-white ico-file-log' data-id='{$data->id}'>{$data->status}</span>";
			}
		}

		return "<span class='badge bg-grey col-white'>{$data->status}</span>";
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnFileType( $data )
	{
		return str_limit($data->file_type, 15);
	}

	/**
	 * Customize the column
	 *
	 * @param File $data
	 *
	 * @return string
	 */
	private function getColumnAction( File $data )
	{
		$buttons = DataTableHelper::getActions([
			DataTableHelper::ACTION_VIEW => [
				'button' => '<i class="material-icons ">pageview</i>',
				'route' => route( 'admin.files.show', $data['id'] ),
				'permission' => Permission::canDo(Permission::ACTION_SHOW, Permission::FILE), // or false
			],

			DataTableHelper::ACTION_EDIT => [
				'button' => '<i class="material-icons ">edit</i>',
				'route' => route( 'admin.files.edit', $data['id'] ),
				'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::FILE), // or false
			],

			DataTableHelper::ACTION_DESTROY => [
				'button' => '<i class="material-icons bt-destroy ">delete</i>',
				'route' => route( 'admin.files.destroy', $data['id'] ),
				'permission' => Permission::canDo(Permission::ACTION_DESTROY, Permission::FILE), // or false
			],
		], false);

		if($data->getOriginal('status') == File::FILE_SCHEDULED
		   || $data->getOriginal('status') == File::FILE_PROCESSING)
			unset( $buttons[ 3 ] );

		if($data->is_sheet && ($data->getOriginal('status') == File::FILE_UNPROCESSED ||
		                       $data->getOriginal('status') == File::FILE_ERROR) )
		{
			$buttons[] = html_entity_decode(link_to(
				route('admin.files.process', $data->id),
				'<i class="material-icons ico-process ">play_circle_filled</i>',
				[ 'class' => "btn btn-success btn-circle  waves-effect waves-circle waves-float" ]
			));
		}

		return implode("\n", $buttons);
	}

}
