<?php

namespace Modules\Admin\DataTables;

use Auth;
use Contribution;
use Modules\Admin\Helpers\DataTableBuilderHelper;
use Modules\Admin\Http\Controllers\ContributionController;
use Modules\Admin\Http\Controllers\TransactionApproveController;
use Modules\Core\Entities\Role;
use Modules\Core\Helpers\DataTableHelper;
use Permission;
use Yajra\DataTables\Services\DataTable;


/**
 * Class FilesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class ContributionsDataTable extends DataTable
{

    public $queryFilters = [];
    public $controllerRequest = ContributionController::class;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
            ->editColumn( 'user.account.person_type', function($data) { return $this->getColumnName($data); } )
            ->editColumn( 'type', function($data){ return $this->getColumnType($data); } )
            ->editColumn( 'value', function($data){ return $this->getColumnValue($data); } )
            ->editColumn( 'date', function($data) { return $this->getColumnDate($data); } )
            ->editColumn( 'status', function($data) { return $this->getColumnStatus($data); } )

            ->rawColumns([ 'action','status','value','user.account.person_type','type' ]);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        /** @var \Modules\Core\Entities\Contribution $contributionQuery */
        $contributionQuery = Contribution::with('user','user.account')->where($this->queryFilters);

        if(Auth::getUser()->hasRole(Role::PROFILE_USER)){
            $contributionQuery->whereUserId(Auth::user()->id);
        }

        return $contributionQuery->select();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'data' => ['datatable_id' => Contribution::class]
            ])
            ->parameters($this->getBuilderParameters());
    }

    /** Set variable $queryFilters
     * @param $queryFilters
     *
     * @return $this
     */
    public function setQueryFilters($queryFilters){

        if(is_array($queryFilters)){
            $this->queryFilters = $queryFilters;
        }
        else
            $this->queryFilters = [$queryFilters];

        return $this;
    }

    /** Set variable $queryFilters
     * @param $controllerRequest
     * @throws \Throwable
     * @return $this
     */
    public function setControllerRequest($controllerRequest){

        $only_classes = [
          TransactionApproveController::class,
          ContributionController::class,
        ];

        if(in_array($controllerRequest,$only_classes)){
            $this->controllerRequest = $controllerRequest;
        }
        else
            throw new \Exception('Class not allowed');

        return $this;

    }


    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function getHtmlBuilder()
    {
        $builder = $this->html();
        if ($this->htmlCallback) {
            call_user_func($this->htmlCallback, $builder);
        }

        return $builder;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [ 'data' => 'user.account.person_type', 'name' => 'user.account.person_type', 'title' => 'Socio(a)' ],
            [ 'data' => 'value', 'name' => 'value', 'title' => 'Valor' ],
            [ 'data' => 'date', 'name' => 'date', 'title' => 'Data Aporte' ],
            [ 'data' => 'type', 'name' => 'type', 'title' => 'Tipo' ],
            [ 'data' => 'status', 'name' => 'status', 'title' => 'Status', 'width' => '8%' ],
            [ 'data' => 'action', 'name' => 'action', 'title' => 'Ações', 'width' => '10%', ],

        ];
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'paging' => true,
            'searching' => true,
            'responsive' => true,
            'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
            'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
            'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
            'dom' => DataTableHelper::DOM,
            'columnDefs' => [
                [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
            ],
            'buttons' =>
                $this->controllerRequest == TransactionApproveController::class ?
                    []:[ 'copy', 'csv', 'excel', 'print' ], // ['export', 'pdf', 'print'],
            'order' => [ 0, 'desc' ],
        ];
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnValue( $data )
    {
        return "<p class='col-teal'>{$data->value}</p>";
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnType( $data )
    {
        return "<p class='col-green small'>{$data->friendlyType}</p>";
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnName( $data )
    {
        return "<strong class='font-bold'>{$data->user->account->name}</strong>";
    }


    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnDate( $data )
    {
        return $data->date_format;
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnStatus( $data )
    {
            switch ($data->getOriginal('status') )
            {
                case Contribution::CONTRIBUTION_PENDING:
                    return "<strong class='col-amber'>{$data->status}</strong>";

                case Contribution::CONTRIBUTION_SCHEDULED:
                    return "<strong class='col-blue-grey'>{$data->status}</strong>";

                case Contribution::CONTRIBUTION_NOT_APPROVED:
                    return "<strong class='col-red'>{$data->status}</strong>";

                case Contribution::CONTRIBUTION_APPROVED:
                    return "<strong class='col-green'>{$data->status}</strong>";
            }

        return "<strong class='col-grey'>{$data->status}</strong>";
    }

    /**
     * Customize the column
     *
     * @param Contribution $data
     *
     * @return string
     */
    private function getColumnAction( Contribution $data )
    {

        if($this->controllerRequest == TransactionApproveController::class)
        {
            $buttons = DataTableHelper::getActions([

                DataTableHelper::ACTION_EDIT => [
                    'button' => '<i class="material-icons ">pageview</i>',
                    'route' => '#',
                    'title'  => 'Ver Solicitação',
                    'class'  => 'btn-success',
                    'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::CONTRIBUTION), // or false
                    'extra_attrs' => [
                        'onclick' => "contributionApp.getContribution({$data['id']})",
                        'data-toggle' => 'modal',
                        'data-target' => '#editModalContribution',
                    ]
                ],

            ], false);


        }
        else
        {
            $buttons = DataTableHelper::getActions([

                DataTableHelper::ACTION_VIEW => [
                    'button' => '<i class="material-icons ">picture_as_pdf</i>',
                    'route' => \Module::asset('admin:pdfs/contrato.pdf'),
                    'permission' => true, // or false
                    'extra_attrs' => [
                        'target' => '_blank'
                    ]
                ],

                DataTableHelper::ACTION_EDIT => [
                    'button' => '<i class="material-icons ">edit</i>',
                    'route' => '#',
                    'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::CONTRIBUTION), // or false
                    'extra_attrs' => [
                        'onclick' => "app.getContribution({$data['id']})",
                        'data-toggle' => 'modal',
                        'data-target' => '#editModal',
                    ]
                ],

                DataTableHelper::ACTION_DESTROY => [
                    'button' => '<i class="material-icons bt-destroy ">delete</i>',
                    'route' => route( 'admin.contributions.destroy', $data['id'] ),
                    'permission' => Permission::canDo(Permission::ACTION_DESTROY, Permission::CONTRIBUTION), // or false
                ],
            ], false);


        }


        return implode("\n", $buttons);
    }


}
