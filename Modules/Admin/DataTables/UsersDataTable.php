<?php

namespace Modules\Admin\DataTables;

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\User;
use Modules\Core\Helpers\DataTableHelper;
use Yajra\DataTables\Services\DataTable;


/**
 * Class UsersDataTable
 *
 * @package Modules\Admin\DataTables
 */
class UsersDataTable extends DataTable
{

	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables($query)
			->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
			->editColumn( 'account.name', function($data) { return $data->account->name; } )
			->editColumn( 'account.cpf', function($data) { return $data->account->document; } )
			->editColumn( 'account.telephone_1', function($data) { return $data->account->telephone_1; } )
			->editColumn( 'account.telephone_2', function($data) { return $data->account->telephone_2; } )
            ->editColumn( 'roles.display_name', function($data) { return $this->getColumnRoleName($data); } )
            ->rawColumns([ 'action', ]);
	}

	/**
	 * Get the query object to be processed by dataTables.
	 *
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
	 */
    public function query()
    {
	    return User::with(['roles','account'])->select();
    }

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
	        [ 'data' => 'id', 'name' => 'id', 'title' => 'ID', 'width' => '5%' ],
	        [ 'data' => 'account.name', 'name' => 'name', 'title' => 'Nome' ],
	        [ 'data' => 'email', 'name' => 'email', 'title' => 'E-mail' ],
	        [ 'data' => 'account.cpf', 'name' => 'cpf', 'title' => 'CPF' ],
	        [ 'data' => 'account.telephone_1', 'name' => 'telephone_1', 'title' => 'Telefone 1' ],
	        [ 'data' => 'account.telephone_2', 'name' => 'telephone_2', 'title' => 'Telefone 2' ],
	        [ 'data' => 'roles.display_name', 'name' => 'roles.display_name', 'title' => 'Perfil' ],
	        [ 'data' => 'created_at', 'name' => 'created_at', 'title' => 'Cadastro', 'width' => '10%', ],
	        [ 'data' => 'action', 'name' => 'action', 'title' => 'Ação', 'width' => '12%' ]
        ];
    }

	/**
	 * Get default builder parameters.
	 *
	 * @return array
	 */
	protected function getBuilderParameters()
	{
		return [
			'paging' => true,
			'searching' => true,
			'responsive' => true,
			'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
			'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
			'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
			'dom' => DataTableHelper::DOM,
			'buttons' => [ 'copy', 'csv', 'excel', 'print' ], // ['export', 'pdf', 'print'],
			'order' => [ 0, 'desc' ],
		];
	}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Usuarios_' . date('d-m-Y__H-m');
    }

	/**
	 * Customize the column
	 *
	 * @param User $data
	 *
	 * @return string
	 */
	private function getColumnRoleName( User $data )
	{
		return $data->roles->pluck('display_name')->implode(', ');
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnAction( User $data )
	{
		return DataTableHelper::getActions([
			//DataTableHelper::ACTION_VIEW => [
			//	'button' => '<i class="material-icons ">pageview</i>',
			//	'route' => route( 'admin.users.show', $data['id'] ),
			//	'permission' => true,
			//],

			DataTableHelper::ACTION_EDIT => [
				'button' => '<i class="material-icons ">edit</i>',
				'route' => route( 'admin.users.edit', $data['id'] ),
				'permission' => true,
			],

			DataTableHelper::ACTION_DESTROY => [
				'button' => '<i class="material-icons bt-destroy ">delete</i>',
				'route' => route( 'admin.users.destroy', $data['id'] ),
				'permission' => true,
			],
		]);
	}

}
