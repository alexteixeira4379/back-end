<?php

namespace Modules\Admin\DataTables;

use Modules\Core\Entities\Recipe;
use Modules\Core\Helpers\DataTableHelper;
use Permission;
use Yajra\DataTables\Services\DataTable;


/**
 * Class FilesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class RecipesDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
            ->editColumn( 'kind', function($data){ return $this->getColumnKind($data); } )
            ->editColumn( 'assignee_assignor', function($data){ return $this->getColumnAssigneeAssignor($data); } )
            ->editColumn( 'value', function($data){ return $this->getColumnValue($data); } )
            ->editColumn( 'date', function($data) { return $this->getColumnDate($data); } )
            ->editColumn( 'datetime', function($data) { return $this->getColumnDateTime($data); } )

            ->rawColumns([ 'action','value']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        return Recipe::select();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'data' => ['datatable_id' => Recipe::class]
            ])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function getHtmlBuilder()
    {
        $builder = $this->html();
        if ($this->htmlCallback) {
            call_user_func($this->htmlCallback, $builder);
        }

        return $builder;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [ 'data' => 'kind', 'name' => 'kind', 'title' => 'Natureza' ],
            [ 'data' => 'assignee_assignor', 'name' => 'assignee_assignor', 'title' => 'Cedente/Cessionário' ],
            [ 'data' => 'value', 'name' => 'value', 'title' => 'Valor' ],
            [ 'data' => 'date', 'name' => 'date', 'title' => 'Data' ],
            [ 'data' => 'datetime', 'name' => 'datetime', 'title' => 'Data de Registro' ],
            [ 'data' => 'action', 'name' => 'action', 'title' => 'Ações', 'width' => '10%', ],

        ];
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'paging' => true,
            'searching' => true,
            'responsive' => true,
            'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
            'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
            'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
            'dom' => DataTableHelper::DOM,
            'columnDefs' => [
                [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
            ],
            'buttons' => [ 'copy', 'csv', 'excel', 'print' ], // ['export', 'pdf', 'print'],
            'order' => [ 0, 'desc' ],
        ];
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnKind( $data )
    {
        return $data->kind;
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnAssigneeAssignor( $data )
    {
        return $data->assignee_assignor;
    }


    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnValue( $data )
    {
        return "<strong>{$data->value}</strong>";
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnDate( $data )
    {
        return $data->date;
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnDateTime( $data )
    {
        return $data->datetime_format;
    }

    /**
     * Customize the column
     *
     * @param Recipe $data
     *
     * @return string
     */
    private function getColumnAction( Recipe $data )
    {

        $buttons = DataTableHelper::getActions([

            DataTableHelper::ACTION_EDIT => [
                'button' => '<i class="material-icons ">edit</i>',
                'route' => '#',
                'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::RECIPE), // or false
                'extra_attrs' => [
                    'onclick' => "recipeApp.getRecipes({$data['id']})",
                    'data-toggle' => 'modal',
                    'data-target' => '#editModalRecipe',
                ]
            ],

            DataTableHelper::ACTION_DESTROY => [
                'button' => '<i class="material-icons bt-destroy ">delete</i>',
                'route' => route( 'admin.recipes.destroy', $data['id'] ),
                'permission' => Permission::canDo(Permission::ACTION_DESTROY, Permission::RECIPE), // or false
            ],
        ], false);



        return implode("\n", $buttons);
    }


}
