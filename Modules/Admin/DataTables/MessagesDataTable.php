<?php

namespace Modules\Admin\DataTables;

use Modules\Core\Entities\User;
use Modules\Core\Helpers\DataTableHelper;
use Yajra\DataTables\Services\DataTable;


/**
 * Class MessagesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class MessagesDataTable extends DataTable
{

	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables($query)
			->addColumn( 'id', function($data) { return $this->getColumnId($data); } )
			->addColumn( 'sender', function($data) { return $this->getColumnSender($data); } )
			->addColumn( 'recipient', function($data) { return $this->getColumnRecipient($data); } )
			->addColumn( 'title', function($data) { return $this->getColumnTitle($data); } )
			->addColumn( 'message', function($data) { return $this->getColumnMessage($data); } )
			->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
            ->rawColumns([ 'id', 'action' ]);
	}

	/**
	 * Get the query object to be processed by dataTables.
	 *
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
	 */
    public function query()
    {
	    return  \Auth::getUser()->threads()->first()->messages;
    }

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
	        [ 'data' => 'id', 'name' => 'id', 'title' => 'ID', 'width' => '5%' ],
	        [ 'data' => 'sender', 'name' => 'sender', 'title' => 'Remetente', 'width' => '5%' ],
	        [ 'data' => 'recipient', 'name' => 'recipient', 'title' => 'Destinatário', 'width' => '5%' ],
	        [ 'data' => 'title', 'name' => 'title', 'title' => 'Titulo', 'width' => '5%' ],
	        [ 'data' => 'message', 'name' => 'message', 'title' => 'Menssagem', 'width' => '5%' ],
	        [ 'data' => 'action', 'name' => 'action', 'title' => 'Ações', 'width' => '5%' ],
        ];
    }

	/**
	 * Get default builder parameters.
	 *
	 * @return array
	 */
	protected function getBuilderParameters()
	{
		return [
			'paging' => true,
			'searching' => true,
			'responsive' => true,
			'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
			'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
			'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
			'dom' => DataTableHelper::DOM,
			'columnDefs' => [
		        [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
		    ],
			'buttons' => [ ],
			'order' => [ 0, 'desc' ],
		];
	}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Arquivos_' . date('d-m-Y__H-m');
    }

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnId( $data )
	{
		return $data->id;
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnSender( $data )
	{
		return $data->sender->account->name;
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnRecipient( $data )
	{

	    $recip = $data->recipients;
        $recip = $recip->pluck('user.account.name');

	    return $recip->implode(", \n");
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnTitle( $data )
	{
		return $data->thread->title;
	}

	/**
	 * Customize the column
	 *
	 * @param $data
	 *
	 * @return string
	 */
	private function getColumnMessage( $data )
	{
		return str_limit($data->body, 30);
	}

	/**
	 * Customize the column
	 *
	 * @param File $data
	 *
	 * @return string
	 */
	private function getColumnAction( $data )
	{

        $buttons = DataTableHelper::getActions([

            DataTableHelper::ACTION_VIEW => [
                'button' => '<i class="material-icons ">pageview</i>',
                'route' => '#',
                'permission' => true, // or false
                'extra_attrs' => [
                    'target' => '_blank'
                ]
            ],

        ], false);




        return implode("\n", $buttons);

    }

}
