<?php

namespace Modules\Admin\DataTables;

use Auth;
use Modules\Admin\Http\Controllers\TransactionApproveController;
use Modules\Admin\Http\Controllers\WithdrawalController;
use Modules\Core\Entities\Withdrawal;
use Modules\Core\Helpers\DataTableHelper;
use Permission;
use Role;
use Yajra\DataTables\Services\DataTable;


/**
 * Class FilesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class WithdrawalsDataTable extends DataTable
{

    public $queryFilters = [];
    public $controllerRequest = Withdrawal::class;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn( 'action', function($data) { return $this->getColumnAction($data); } )
            ->addColumn( 'user.account.person_type', function($data) { return $this->getColumnName($data); } )
            ->addColumn( 'value', function($data) { return $this->getColumnValue($data); } )
            ->editColumn( 'type', function($data){ return $this->getColumnType($data); } )
            ->addColumn( 'date', function($data) { return $this->getColumnDate($data); } )
            ->addColumn( 'status', function($data) { return $this->getColumnStatus($data); } )

            ->rawColumns([ 'action','status','value','user.account.person_type','type' ]);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        /** @var \Modules\Core\Entities\Withdrawal $withdrawalQuery */
        $withdrawalQuery = Withdrawal::with('user','user.account')->where($this->queryFilters);

        if(Auth::getUser()->hasRole(Role::PROFILE_USER)){
            $withdrawalQuery->whereUserId(Auth::user()->id);
        }

        return $withdrawalQuery->select();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'data' => ['datatable_id' => Withdrawal::class]
            ])
            ->parameters($this->getBuilderParameters());
    }

    /** Set variable $queryFilters
     * @param $queryFilters
     *
     * @return $this
     */
    public function setQueryFilters($queryFilters){

        if(is_array($queryFilters)){
            $this->queryFilters = $queryFilters;
        }
        else
            $this->queryFilters = [$queryFilters];

        return $this;
    }

    /** Set variable $queryFilters
     * @param $controllerRequest
     * @throws \Throwable
     * @return $this
     */
    public function setControllerRequest($controllerRequest){

        $only_classes = [
            TransactionApproveController::class,
            WithdrawalController::class,
        ];

        if(in_array($controllerRequest,$only_classes)){
            $this->controllerRequest = $controllerRequest;
        }
        else
            throw new \Exception('Class not allowed');

        return $this;

    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function getHtmlBuilder()
    {
        $builder = $this->html();
        if ($this->htmlCallback) {
            call_user_func($this->htmlCallback, $builder);
        }

        return $builder;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [ 'data' => 'user.account.person_type', 'name' => 'user.account.person_type', 'title' => 'Socio(a)' ],
            [ 'data' => 'value', 'name' => 'value', 'title' => 'Valor' ],
            [ 'data' => 'date', 'name' => 'date', 'title' => 'Data Resgate' ],
            [ 'data' => 'type', 'name' => 'type', 'title' => 'Tipo' ],
            [ 'data' => 'status', 'name' => 'status', 'title' => 'Status', 'width' => '8%' ],
            [ 'data' => 'action', 'name' => 'action', 'title' => 'Ações', 'width' => '10%', ],

        ];
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'paging' => true,
            'searching' => true,
            'responsive' => true,
            'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
            'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
            'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
            'dom' => DataTableHelper::DOM,
            'columnDefs' => [
                [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
            ],
            'buttons' =>
                $this->controllerRequest == TransactionApproveController::class ?
                    []:[ 'copy', 'csv', 'excel', 'print' ], // ['export', 'pdf', 'print'],
            'order' => [ 0, 'desc' ],
        ];
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnValue( $data )
    {
        return "<p class='col-teal'>{$data->value}</p>";
    }


    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnType( $data )
    {
        return "<p class='col-green small'>{$data->friendlyType}</p>";
    }


    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnName( $data )
    {
        return "<strong>{$data->user->account->name}</strong>";
    }


    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnDate( $data )
    {
        return $data->date_format;
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnStatus( $data )
    {
            switch ($data->getOriginal('status') )
            {
                case Withdrawal::WITHDRAWAL_PENDING:
                    return "<p class='col-amber'>{$data->status}</p>";

                case Withdrawal::WITHDRAWAL_SCHEDULED:
                    return "<p class='col-blue-grey'>{$data->status}</p>";

                case Withdrawal::WITHDRAWAL_NOT_APPROVED:
                    return "<p class='col-red'>{$data->status}</p>";

                case Withdrawal::WITHDRAWAL_APPROVED:
                    return "<p class='col-green'>{$data->status}</p>";
            }

        return "<p class='col-grey'>{$data->status}</p>";
    }

    /**
     * Customize the column
     *
     * @param Withdrawal $data
     *
     * @return string
     */
    private function getColumnAction( Withdrawal $data )
    {
        if($this->controllerRequest == TransactionApproveController::class)
        {
            $buttons = DataTableHelper::getActions([

                DataTableHelper::ACTION_EDIT => [
                    'button' => '<i class="material-icons ">pageview</i>',
                    'route' => '#',
                    'title'  => 'Ver Solicitação',
                    'class'  => 'btn-success',
                    'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::WITHDRAWAL), // or false
                    'extra_attrs' => [
                        'onclick' => "withdrawalApp.getWithdrawal({$data['id']})",
                        'data-toggle' => 'modal',
                        'data-target' => '#editModalWithdrawal',
                    ]
                ],

            ], false);

        }
        else
        {
            $buttons = DataTableHelper::getActions([

                DataTableHelper::ACTION_EDIT => [
                    'button' => '<i class="material-icons ">edit</i>',
                    'route' => '#',
                    'permission' => Permission::canDo(Permission::ACTION_EDIT, Permission::WITHDRAWAL), // or false
                    'extra_attrs' => [
                        'onclick' => "app.getWithdrawal({$data['id']})",
                        'data-toggle' => 'modal',
                        'data-target' => '#editModal',
                    ]
                ],

                DataTableHelper::ACTION_DESTROY => [
                    'button' => '<i class="material-icons bt-destroy ">delete</i>',
                    'route' => route( 'admin.withdrawals.destroy', $data['id'] ),
                    'permission' => Permission::canDo(Permission::ACTION_DESTROY, Permission::WITHDRAWAL), // or false
                ],
            ], false);
        }


        return implode("\n", $buttons);
    }


}
