<?php

namespace Modules\Admin\DataTables;

use Illuminate\Database\Eloquent\Collection;
use Modules\Core\Entities\File;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\Transaction;
use Modules\Core\Helpers\DataTableHelper;
use Modules\Core\Helpers\TransactionsHelper;
use PhpParser\Node\Expr\Array_;
use Role;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;


/**
 * Class FilesDataTable
 *
 * @package Modules\Admin\DataTables
 */
class ExtractTransactionsDataTable extends DataTable
{

    protected $printPreview = 'admin::datatables.print';

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn( 'date', function($data) { return $this->getColumnDate($data); } )
            ->editColumn( 'withdrawal', function($data) { return $this->getColumnWithdrawal($data); } )
            ->editColumn( 'contribution', function($data) { return $this->getColumnContribution($data); } )
            ->addColumn( 'total_extract', function($data) { return $this->getTotalExtract($data); } )
            ->rawColumns([ 'total_extract','contribution','withdrawal']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $data = \Auth::getUser()->hasRole(Role::PROFILE_USER) ?
            \Auth::getUser()->transactions:
            \User::with('transactions')->get()->pluck('transactions')->collapse();


        return TransactionsHelper::getTransactionExtract( $data );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->ajax([ 'data' => 'function(data) { data.filters = CoreModule.datatables.getFilters(); }', ])
            ->columns($this->getColumns())
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [ 'data' => 'date', 'name' => 'date', 'title' => 'Data (Dia/Mes/Ano)' ],
            [ 'data' => 'withdrawal', 'name' => 'withdrawal', 'title' => 'Resgate' ],
            [ 'data' => 'contribution', 'name' => 'contribution', 'title' => 'Aporte' ],
            [ 'data' => 'total_extract', 'name' => 'total_extract', 'title' => 'Saldo' ],
        ];
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'paging' => false,
            'pageLength' => 31,
            'searching' => false,
            'responsive' => true,
            'drawCallback' => DataTableHelper::CALLBACK_DRAW_CALLBACK,
            'rowCallback' => DataTableHelper::CALLBACK_ROW_CALLBACK,
            'initComplete' => DataTableHelper::CALLBACK_INIT_CALLBACK,
            'dom' => DataTableHelper::DOM,
            'columnDefs' => [
                [ 'responsivePriority' => 1, 'targets' => count($this->getColumns()) - 1 ],
            ],
            'buttons' => ['pdf'], // [ 'copy', 'csv', 'excel', 'print' ,'export', 'print'],
            'order' => [ 0, 'desc' ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Fechamento_' . date('d-m-Y__H-m');
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnDate( $data )
    {
        $M = [ 1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro",];

        $datatime = Carbon::parse($data->date->format('Y-m-d'))->format('d m Y');

        $datatime = explode(' ',$datatime);

        $datatime[1] = ' de ' . $M[(int)$datatime[1]] . ' de ';

        return implode(' ',$datatime);
    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnContribution( $data )
    {
        /** @var Collection $transactions */
        $transactions = $data->transactions;

        $total_extract = $transactions
                        ->where('typeable_type','=',\Modules\Core\Entities\Contribution::class)
                        ->pluck('value')
                        ->sum();

        $total_extract = 'R$ '.number_format($total_extract, 2, ',', '.');

        $i = '';
        if($this->request()->action != 'pdf')
            $i = '<i class=\'material-icons col-red m-r-20\'>trending_up</i>';

        return "$i<strong> $total_extract</strong>";

    }

    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getColumnWithdrawal( $data )
    {
        /** @var Collection $transactions */
        $transactions = $data->transactions;

        $total_extract = $transactions
            ->where('typeable_type','=',\Modules\Core\Entities\Withdrawal::class)
            ->pluck('value')
            ->sum();

        $total_extract = 'R$ '.number_format($total_extract, 2, ',', '.');

        $i = '';
        if($this->request()->action != 'pdf')
            $i = '<i class=\'material-icons col-red m-r-20\'>trending_down</i>';

        return "$i<strong> $total_extract</strong>";
    }



    /**
     * Customize the column
     *
     * @param $data
     *
     * @return string
     */
    private function getTotalExtract( $data )
    {
        $total_extract = 0;

        foreach ( $data->transactions->toArray() as $transaction){

            $transaction = new Transaction($transaction);
            switch ($transaction->typeable_type){
                case \Modules\Core\Entities\Contribution::class:
                    $total_extract = TransactionsHelper::increaseBalance($total_extract,$transaction->value);
                    break;
                case  \Modules\Core\Entities\Withdrawal::class:
                    $total_extract = TransactionsHelper::decreaseBalance($total_extract,$transaction->value);

            }

        }

        $total_extract = 'R$ '.number_format($total_extract, 2, ',', '.');

        return "<p class='col-teal'>{$total_extract}</p>";
    }



}
