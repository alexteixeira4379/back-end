<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Finances
Breadcrumbs::register('admin.finances.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Financeiro', route('admin.finances.index'), ['icon' => 'attach_money']);
});
