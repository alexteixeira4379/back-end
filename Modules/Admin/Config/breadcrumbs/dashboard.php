<?php

use DaveJamesMiller\Breadcrumbs\Generator;

// Dashboard
Breadcrumbs::register('admin.dashboard.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->push('Home', route('admin.dashboard.index'), ['icon' => 'dashboard']);
});

// Dashboard > Create Resource
Breadcrumbs::register('admin.dashboard.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Novo Dashboard', route('admin.dashboard.create'), ['icon' => 'fiber_new']);
});

// Dashboard > Show [Resource Name]
Breadcrumbs::register('admin.dashboard.show', function(Generator $breadcrumbs, $dashboard_id)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Dashboard', route('admin.dashboard.show', $dashboard_id), ['icon' => '']);
});

// Dashboard > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.dashboard.edit', function(Generator $breadcrumbs, $dashboard_id)
{
	$breadcrumbs->parent('admin.dashboard.show', $dashboard_id);
	$breadcrumbs->push('Editar', route('admin.dashboard.edit', $dashboard_id), ['icon' => 'edit']);
});