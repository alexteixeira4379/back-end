<?php

use DaveJamesMiller\Breadcrumbs\Generator;

// Settings
Breadcrumbs::register('admin.settings.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Configurações', route('admin.settings.index'), ['icon' => 'settings']);
});

// Settings > Create Resource
Breadcrumbs::register('admin.settings.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.settings.index');
	$breadcrumbs->push('Nova Configuração', route('admin.settings.create'), ['icon' => 'fiber_new']);
});

// Settings > Show [Resource Name]
Breadcrumbs::register('admin.settings.show', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.settings.index');

	$breadcrumbs->push(AppHelper::getFriendlyId( $id ), null, ['icon' => 'folder_open']);
});

// Settings > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.settings.edit', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.settings.index');

	$breadcrumbs->push(
		"Editar " . AppHelper::getFriendlyId( $id ),
		route('admin.settings.edit', $id),
		['icon' => 'edit']
	);
});