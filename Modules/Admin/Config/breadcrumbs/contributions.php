<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Contributions
Breadcrumbs::register('admin.contributions.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Aportes (Aumento de capital)', route('admin.contributions.index'), ['icon' => 'add']);
});

// Contributions > Create Resource
Breadcrumbs::register('admin.contributions.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.contributions.index');
	$breadcrumbs->push('Novo Arquivo', route('admin.contributions.create'), ['icon' => 'fiber_new']);
});

// Contributions > Show [Resource Name]
Breadcrumbs::register('admin.contributions.show', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.contributions.index');
	$file = File::find($id);

	$breadcrumbs->push(mb_strtoupper( $file->name ) , null, ['icon' => 'folder_open']);
});

// Contributions > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.contributions.edit', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.contributions.index');

	$file = File::find($id);

	$breadcrumbs->push(
		"Editar {$file->name} ($file->file_name)",
		route('admin.contributions.edit', $id),
		['icon' => 'edit']
	);
});