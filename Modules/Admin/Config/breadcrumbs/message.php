<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Contributions
Breadcrumbs::register('admin.message.index', function(Generator $breadcrumbs)
{
    $breadcrumbs->parent('admin.dashboard.index');
    $breadcrumbs->push('Menssagens', route('admin.message.index'), ['icon' => 'account_balance']);
});

// Contributions > Create Resource
Breadcrumbs::register('admin.message.create', function(Generator $breadcrumbs)
{
    $breadcrumbs->parent('admin.message.index');
    $breadcrumbs->push('Nova menssagem', route('admin.message.create'), ['icon' => 'fiber_new']);
});

// Contributions > Show [Resource Name]
Breadcrumbs::register('admin.message.show', function(Generator $breadcrumbs, $id)
{
    $breadcrumbs->parent('admin.message.index');
    $file = File::find($id);

    $breadcrumbs->push(mb_strtoupper( $file->name ) , null, ['icon' => 'folder_open']);
});

// Contributions > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.message.edit', function(Generator $breadcrumbs, $id)
{
    $breadcrumbs->parent('admin.message.index');

    $file = File::find($id);

    $breadcrumbs->push(
        "Editar {$file->name} ($file->file_name)",
        route('admin.message.edit', $id),
        ['icon' => 'edit']
    );
});