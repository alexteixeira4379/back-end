<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Contributions
Breadcrumbs::register('admin.capital.index', function(Generator $breadcrumbs)
{
    $breadcrumbs->parent('admin.dashboard.index');
    $breadcrumbs->push('Capital (Extrato)', route('admin.capital.index'), ['icon' => 'account_balance']);
});

// Contributions > Create Resource
Breadcrumbs::register('admin.capital.create', function(Generator $breadcrumbs)
{
    $breadcrumbs->parent('admin.capital.index');
    $breadcrumbs->push('Novo Arquivo', route('admin.capital.create'), ['icon' => 'fiber_new']);
});

// Contributions > Show [Resource Name]
Breadcrumbs::register('admin.capital.show', function(Generator $breadcrumbs, $id)
{
    $breadcrumbs->parent('admin.capital.index');
    $file = File::find($id);

    $breadcrumbs->push(mb_strtoupper( $file->name ) , null, ['icon' => 'folder_open']);
});

// Contributions > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.capital.edit', function(Generator $breadcrumbs, $id)
{
    $breadcrumbs->parent('admin.capital.index');

    $file = File::find($id);

    $breadcrumbs->push(
        "Editar {$file->name} ($file->file_name)",
        route('admin.capital.edit', $id),
        ['icon' => 'edit']
    );
});