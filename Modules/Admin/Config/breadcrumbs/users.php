<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\User;

// Users
Breadcrumbs::register('admin.users.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Usuários do Sistema', route('admin.users.index'), ['icon' => 'accessibility']);
});

// Users > Create Resource
Breadcrumbs::register('admin.users.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.users.index');
	$breadcrumbs->push('Novo Usuário', route('admin.users.create'), ['icon' => 'fiber_new']);
});

// Users > Show [Resource Name]
Breadcrumbs::register('admin.users.show', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.users.index');
	$user = User::find($id);

	$breadcrumbs->push(mb_strtoupper( $user->name ) , null, ['icon' => 'folder_open']);
});

// Users > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.users.edit', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.users.index');

	$user = User::find($id);

	$breadcrumbs->push(
		"Editar " . mb_strtoupper( $user->name ),
		route('admin.users.edit', $id),
		['icon' => 'edit']
	);
});