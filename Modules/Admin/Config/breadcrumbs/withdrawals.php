<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Contributions
Breadcrumbs::register('admin.withdrawals.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Resgates', route('admin.withdrawals.index'), ['icon' => 'remove']);
});

// Contributions > Create Resource
Breadcrumbs::register('admin.withdrawals.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.withdrawals.index');
	$breadcrumbs->push('Novo Arquivo', route('admin.withdrawals.create'), ['icon' => 'fiber_new']);
});

// Contributions > Show [Resource Name]
Breadcrumbs::register('admin.withdrawals.show', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.withdrawals.index');
	$file = File::find($id);

	$breadcrumbs->push(mb_strtoupper( $file->name ) , null, ['icon' => 'folder_open']);
});

// Contributions > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.withdrawals.edit', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.withdrawals.index');

	$file = File::find($id);

	$breadcrumbs->push(
		"Editar {$file->name} ($file->file_name)",
		route('admin.withdrawals.edit', $id),
		['icon' => 'edit']
	);
});