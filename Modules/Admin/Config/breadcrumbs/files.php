<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Files
Breadcrumbs::register('admin.files.index', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.dashboard.index');
	$breadcrumbs->push('Banco de Arquivos', route('admin.files.index'), ['icon' => 'attach_file']);
});

// Files > Create Resource
Breadcrumbs::register('admin.files.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('admin.files.index');
	$breadcrumbs->push('Novo Arquivo', route('admin.files.create'), ['icon' => 'fiber_new']);
});

// Files > Show [Resource Name]
Breadcrumbs::register('admin.files.show', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.files.index');
	$file = File::find($id);

	$breadcrumbs->push(mb_strtoupper( $file->name ) , null, ['icon' => 'folder_open']);
});

// Files > [Resource Name] > Edit Resource
Breadcrumbs::register('admin.files.edit', function(Generator $breadcrumbs, $id)
{
	$breadcrumbs->parent('admin.files.index');

	$file = File::find($id);

	$breadcrumbs->push(
		"Editar {$file->name} ($file->file_name)",
		route('admin.files.edit', $id),
		['icon' => 'edit']
	);
});