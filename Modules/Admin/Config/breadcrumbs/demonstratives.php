<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use Modules\Core\Entities\File;

// Contributions
Breadcrumbs::register('admin.demonstratives.index', function(Generator $breadcrumbs)
{
    $breadcrumbs->parent('admin.dashboard.index');
    $breadcrumbs->push('Demonstrativo de Resultados', route('admin.demonstratives.index'), ['icon' => 'insert_chart']);
});
