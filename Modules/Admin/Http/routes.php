<?php

Route::get('/', function () {
	return redirect(route('admin.dashboard.index'));
});

Route::group(['middleware' => [ 'web', 'auth','roles' ], 'as' => 'admin.',
              'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
	// -- Dashboard
	Route::resource('/dashboard', 'DashboardController');

	// -- Capital
	Route::resource('/capital', 'CapitalController');

	// -- Message
	Route::resource('/message', 'MessageController');

    // -- Demonstratives
    Route::resource('/demonstratives', 'DemonstrativeController');

    // -- Finances
    Route::resource('/finances', 'FinanceController');
    // -- Recipes
    Route::resource('/recipes', 'RecipeController');
    // -- Recipes
    Route::resource('/expenses', 'ExpenseController');
    // -- Taxations
    Route::resource('/taxations', 'TaxationController');

	// -- Users
	Route::resource('/users', 'UserController');
	Route::get('/users/{user_id}/login', 'UserController@login')->name('users.login');

	// -- Files
	Route::resource('/files', 'FileController');
	Route::get('/files/{file}/process', 'FileController@process')->name('files.process');

    // -- File Categories
    Route::resource('/file-categories', 'FileCategoryController');

	// -- Settings
	Route::resource('/settings', 'SettingController');

    // -- contributions
    Route::resource('/contributions', 'ContributionController');

     // -- withdrawals
    Route::resource('/withdrawals', 'WithdrawalController');

    // -- Transaction Approve
    Route::resource('/transaction-approve', 'TransactionApproveController');

});


// -- PUBLIC
Route::group(['middleware' => [ 'web'], 'as' => 'admin.','namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    // -- UserAccount
    Route::resource('/accounts', 'UserAccountController');
    Route::get('/accounts/create/{user_shared?}', 'UserAccountController@create')->name('user.indicate_link');

    // -- Notification
    Route::post('/notify', function()
    {
        return Modules\Core\Helpers\RequestHelper::doApiRequest(function(){
            Auth::user()->unreadNotifications->markAsRead();
        });
    })->name('notifiable.mark_as_read');

});


