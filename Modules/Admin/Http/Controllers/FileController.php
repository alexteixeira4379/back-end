<?php

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\DataTables\FilesDataTable;
use Modules\Admin\Helpers\ExcelProcessHelper\ExcelProcess;
use Modules\Core\Entities\File;
use Modules\Core\Entities\FileCategory;
use Modules\Core\Helpers\DBHelper;
use Modules\Core\Helpers\Excel\ConverterHelper;
use Modules\Core\Helpers\Excel\ExcelHelper;
use Modules\Core\Helpers\Excel\ExtractorHelper;
use Modules\Core\Helpers\FileHelper;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Helpers\ResponseHelper;
use Modules\Core\Jobs\ProcessExcelFile;

class FileController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @param FilesDataTable $dataTable
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function index(Request $request, FilesDataTable $dataTable)
	{
		return $dataTable->render('admin::pages.file.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|RequestHelper
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function store(Request $request)
	{
		$options = [
			RequestHelper::REDIRECT_SUCCESS => route('admin.files.index'),
			RequestHelper::REDIRECT_ERROR => route('admin.files.index'),
			RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(), [
				'file_upload'             => 'required|file',
				'file_category_id'        => 'required|integer',
			], [
				'file_upload.required'      => 'Erro: O arquivo não foi selecionado',
				'file_upload.file'          => 'Erro: Houve um problema no envio do arquivo',
				'file_category_id.required' => 'Erro: A categoria é obrigatória',
			])
		];

		return RequestHelper::doRequest(function() use($request)
		{

		}, $options);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
	 * @throws \Exception
	 */
	public function show($id)
	{
		if(!file_exists(FileHelper::getPath($id)))
			return ResponseHelper::getResponse(route('admin.files.index'), 'Erro: Arquivo não encontrado', 404);

		// -- file
		$file = File::find($id);

		if($file->is_sheet)
			return redirect(route('medias.thumb', [ 'id' => $id, 'width' => 9999, 'height' => 9999]));

		// -- begin
		$extractor = new ExtractorHelper( FileHelper::getPath($id), function( ExtractorHelper $extractor )
		{
			//-- set desired columns
			$extractor->setColumns(['precos', 'identificador', 'data_leitura', 'id_pesquisa', 'uf', 'municipio']);

			// -- limit the columns to...
			$extractor->setMaxColumns( 15 );

			// -- extract the data
			$extractor->extract();
		});

		$excel_data = $extractor->getPaginated();

		return view('admin::pages.file.show', compact('excel_data', 'file'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$file = File::find($id);
		return view('admin::pages.file.edit', compact('file'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
	 */
	public function update(Request $request, $id)
	{
		$options = [
			RequestHelper::REDIRECT_SUCCESS => route('admin.files.index'),
			RequestHelper::REDIRECT_ERROR => route('admin.files.edit', $id),
			RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(), [
				'name'                  => 'required',
				'file_category_id'      => 'required',
				'status'                => 'required',
			], [
				'name.required'             => 'Erro: O nome é obrigatório',
				'file_category_id.required' => 'Erro: A categoria é obrigatória',
				'status.required'           => 'Erro: O status é obrigatório',
			])
		];

		return RequestHelper::doRequest(function() use($request, $id)
		{
			\DB::transaction(function () use($id)
			{
				\Request::merge([ 'id' => $id ]);
				DBHelper::update( File::class, \Request::toArray() );
			});

		}, $options);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
	 */
	public function destroy($id)
	{
		return RequestHelper::doRequest(function() use($id)
		{
			\DB::transaction(function() use($id)
			{
				File::destroy( $id );
				\File::delete(FileHelper::getPath($id));
			});
		});
	}

	/**
	 * Process specified spreadsheet.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
	 * @throws \Exception
	 */
	public function process($id)
	{
		if(!file_exists(FileHelper::getPath($id)))
			return ResponseHelper::getResponse(route('admin.files.index'), 'Erro: Arquivo não encontrado', 404);

		return RequestHelper::doRequest(function() use($id)
		{
			// -- if image
			if(($file = File::find($id)) && $file->category->name == FileCategory::FILE_CATEGORY_INCIDENT)
			{
				RequestHelper::$redirect_success_url = route('medias.thumb', [
					'id' => $file->id, 'width' => 9999, 'height' => 9999
				]);

				return true;
			}

			// -- if not image
			// -- size
			$file_size = round(\File::size(FileHelper::getPath($id)) / 1024 / 1024); // in Mb

			// -----------------
			// -- SCHEDULE JOB
			$delay = Carbon::now()->addMinutes(1);

			// -- schedule
			RequestHelper::$custom_message = "O processamento foi agendado para {$delay->format('d/m/Y H:i:s')} e 
												estará disponível alguns momentos após, continue acompanhando esta tela.";

			// -- dispatch
			ProcessExcelFile::dispatch( $file )->delay( $delay );

			$file->status = File::FILE_SCHEDULED;
			$file->save();
		            // -----------------

            // or

            // -- run now
            //(new ExcelProcess($file))->process(false);

		});
	}

}
