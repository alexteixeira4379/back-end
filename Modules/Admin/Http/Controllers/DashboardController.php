<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Knp\Snappy\Pdf;

class DashboardController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

        if(\Auth::getUser()->hasRole(\Role::PROFILE_USER))
        {
            $movement['hasMovement'] = \Auth::getUser()->capital->balance == 0 ? false: true;
            $movement['query'] = \Auth::getUser();
        }
        else
        {
            $movement['hasMovement'] = true;
            $movement['query'] = \Modules\Core\Entities\Transaction::query();
        }

        return view('admin::pages.dashboard.index',[ 'movement' => $movement ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
    \Profitability::whereMont * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
    	    $dd = 1;//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
