<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\DataTables\FileCategoriesDataTable;
use Modules\Core\Entities\FileCategory;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Helpers\ResponseHelper;

class FileCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param FileCategoriesDataTable $dataTable
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request, FileCategoriesDataTable $dataTable)
    {

        return $dataTable->render('admin::pages.file_categories.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param array $extra_options
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
    public function store(Request $request,  array $extra_options = [ 'rules' => [], 'messages' => [] ])
    {

        $options = [
            RequestHelper::REDIRECT_SUCCESS => route('admin.file-categories.index'),
            RequestHelper::REDIRECT_ERROR => route('admin.file-categories.index'),
            RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(),

                //Rules
                array_merge(
                    [
                        'name'                  => 'required',
                    ],$extra_options['rules']
                ),
                //Messages
                array_merge(
                    [

                        'name.required'        => 'Error: O campo "NOME" não foi informado',

                    ],$extra_options['messages']
                ))
        ];

        return RequestHelper::doRequest(function() use($request)
        {

            $category = \DBHelper::update(\FileCategory::class,$request->only(['name','id']));

        }, $options);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            if(!$id)
                throw new \Exception('Erro: Desculpe, parâmetros inválidos', 1);

            /** @var $data \FileCategory */
            $data = \FileCategory::whereId($id)->get();

            if($data->isEmpty())
                throw new \Exception('Erro: Recurso não encontrado', 1);

            $data_response = $data->first()->toArray();

            return response()->json([
                'status' => 'success',
                'data' => $data_response,
            ]);
        }
        catch (\Exception $ex)
        {
            $msg = $ex->getCode() == 1 ? $ex->getMessage() : 'Erro: Desculpe, ocorreu um erro desconhecido';

            return response()->json([
                'status' => 'error',
                'data' => $msg,
            ], 500);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /** @var \FileCategory $plan */
        $category = \FileCategory::find($id);

        if($category){

            $request->merge(['id' => $id]);

            return $this->store($request);
        }
        else
            return ResponseHelper::getResponse(
                route('admin.file-categories.index'), 'Erro: Não foi possivel editar os dados', 1);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RequestHelper|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return RequestHelper::doRequest(function() use($id)
        {

            \DB::transaction(function() use($id)
            {
                \FileCategory::destroy($id);

            });
        });
    }
}