<?php

namespace Modules\Admin\Http\Controllers;

use DBHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\DataTables\ContributionsDataTable;
use Contribution;
use Modules\Core\Entities\Role;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Helpers\ResponseHelper;


class ContributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ContributionsDataTable $dataTable
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request, ContributionsDataTable $dataTable)
    {

        return $dataTable
                ->render('admin::pages.contribution.index',[
                    'count_contribution' => $dataTable->query()->count()
                ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param array $extra_options
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
	public function store(Request $request,  array $extra_options = [ 'rules' => [], 'messages' => [] ])
	{

        $options = [
            RequestHelper::REDIRECT_SUCCESS => route('admin.contributions.index'),
            RequestHelper::REDIRECT_ERROR => route('admin.contributions.index'),
            RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(),
            //Rules
            array_merge(
                [
                    'value'                     => 'required|numeric',
                    'file_upload'               => 'required|mimes:jpeg,jpg,png,gif|max:10000',
                ],$extra_options['rules']
            ),
            //Messages
            array_merge(
                [
                    'value.required'         => 'Erro: O valor do aporte não foi informado',
                    'value.numeric'          => 'Erro: O campo para o valor aceita somente numeros',

                    'file_upload.required'      => 'Erro: O arquivo não foi selecionado',
                    'file_upload.file'          => 'Erro: Houve um problema no envio do arquivo',
                    'file_upload.mimes'         => 'Erro: A imagem não está em um formato valido',
                ],$extra_options['messages']
            ))
        ];

        return RequestHelper::doRequest(function() use($request)
        {

            \DB::transaction(function() use($request)
            {

                $date = (\Carbon\Carbon::now())->toDateString();
                $data_merge = ['date' =>  $date,'user_id' => \Auth::getUser()->id];

                if($request->hasFile('file_upload')){

                    if(($file_name = $request->get('file_name')) == null ){

                        $_id_name = kebab_case("receipt-{$date}-" . uniqid() );
                        $_extension = $request->file('file_upload')->extension();

                        $file_name =  "{$_id_name}.{$_extension}";

                    }

                    $_path = \Auth::getUser()->path . DIRECTORY_SEPARATOR . 'contributions_receipt';
                    $request->file('file_upload')->storeAs($_path,$file_name);

                    $data_merge['file_name'] = $file_name;

                }

                /** @var \Modules\Core\Entities\Contribution $contribution*/
                $contribution = DBHelper::update(
                    Contribution::class,$request->merge($data_merge)->only(Contribution::acceptedFields()));

            });


        }, $options);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}


    /**
     *  * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     * @throws \Throwable
     */
    public function update(Request $request, $id)
	{
        /** @var Contribution $contribution */
	    if(\Auth::getUser()->hasRole(Role::PROFILE_USER))
            $contribution = \Auth::getUser()->contributions()->find($id);
	    else
            $contribution = Contribution::find($id);

	    if($contribution){

            $request->merge(['id' => $id,'file_name' => $contribution->file_name]);

            $extra_options = [
                'rules' => [
                    'file_upload'               => 'mimes:jpeg,jpg,png,gif|max:10000',
                ],
                'messages' => [
                    'file_upload.file'          => 'Erro: Houve um problema no envio do arquivo',
                    'file_upload.mimes'         => 'Erro: A imagem não está em um formato valido',
                ]
            ];

            return $this->store($request,$extra_options);
        }
        else
            return ResponseHelper::getResponse(
                route('admin.contributions.index'), 'Erro: Não foi possivel atualizar os dados', 1);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
    public function destroy($id)
	{
        return RequestHelper::doRequest(function() use($id)
        {

            \DB::transaction(function() use($id)
            {
                if(\Auth::getUser()->hasRole(Role::PROFILE_USER)){
                    \Auth::getUser()->contributions()->find($id)->delete();
                }
                else{
                    Contribution::destroy($id);
                }
            });
        });
	}
}
