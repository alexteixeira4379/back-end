<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\DataTables\MessagesDataTable;
use Modules\Core\Entities\User;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param MessagesDataTable $dataTable
     * @return Response
     */
    public function index( MessagesDataTable $dataTable )
    {

        $message = \Messenger::from(\Auth::getUser())->to(User::whereId(2)->get()->first())->message('Hey!')->send();

        $thread = $message->thread()->first();

        $thread->title = "cuz";

        $thread->save();

        return $dataTable->render('admin::pages.message.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
