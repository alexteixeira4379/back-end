<?php

namespace Modules\Admin\Http\Controllers;

use DBHelper;
use FileHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Core\Entities\Recipe;
use Modules\Core\Entities\Role;
use Modules\Core\Helpers\RequestHelper;


class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  redirect()->route('admin.finances.index');
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     * @throws \Exception
     */
	public function store(Request $request)
	{


	    //*Optional parameter
        $extra_options = [ 'rules' => [], 'messages' => [] ];

        $options = [
            RequestHelper::REDIRECT_SUCCESS => route('admin.finances.index'),
            RequestHelper::REDIRECT_ERROR => route('admin.finances.index'),
            RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(),

            //Rules
            array_merge(
                [
                    'value'                     => 'required|numeric|min:0',
                    'kind'                      => 'required|max:255',
                    'assignee_assignor'         => 'required|max:255',

                    //'file_upload'               => 'required|mimes:jpeg,jpg,png,gif|max:10000',
                ],$extra_options['rules']
            ),

            //Messages
            array_merge(
                [
                    'value.required'         => 'Erro: O valor do aporte não foi informado',
                    'value.numeric'          => 'Erro: O campo para o valor aceita somente numeros',
                    'value.min'              => 'Erro: Valor não é inválido para resgate ',
                    'kind.required'          => 'Erro: O campo "NATUREZA" não foi informado',
                    'kind.digits_between'    => 'Erro: O campo "NATUREZA" não está dentro dos padrões de carecteres aceitos',
                    'assignee_assignor.required'          => 'Erro: O campo "CEDENTE/CESSIONÁRIO:" não foi informado',
                    'assignee_assignor.digits_between'    => 'Erro: O campo "CEDENTE/CESSIONÁRIO:" não está dentro dos padrões de carecteres aceitos',

                    //'file_upload.required'      => 'Erro: O arquivo não foi selecionado',
                    //'file_upload.file'          => 'Erro: Houve um problema no envio do arquivo',
                    //'file_upload.mimes'         => 'Erro: A imagem não está em um formato valido',
                ],$extra_options['messages']
            ))
        ];

        return RequestHelper::doRequest(function() use($request)
        {

            \DB::transaction(function() use($request)
            {

                $recipe = DBHelper::update(
                    Recipe::class,$request->only(\Schema::getColumnListing((new Recipe)->getTable())));

            });


        }, $options);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}


    /**
     *  * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     * @throws \Throwable
     */
    public function update(Request $request, $id)
	{

	}

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
    public function destroy($id)
	{
        return RequestHelper::doRequest(function() use($id)
        {

            \DB::transaction(function() use($id)
            {
                Recipe::destroy($id);
            });
        });
	}
}
