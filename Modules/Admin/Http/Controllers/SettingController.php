<?php

namespace Modules\Admin\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Admin\DataTables\ModulesDataTable;
use Modules\Core\Entities\Setting;
use Modules\Core\Helpers\DBHelper;
use Modules\Core\Helpers\Modules\Extensions\GoToRule;
use Modules\Core\Helpers\Modules\Rules\Action;
use Modules\Core\Helpers\Modules\Rules\Condition;
use Modules\Core\Helpers\RequestHelper;
use Response;

class SettingController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$setting = Setting::firstOrFail();
		return view('admin::pages.setting.edit', compact('setting'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
	 */
	public function store(Request $request)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
	 */
	public function update(Request $request, $id)
	{

		$options = [
			RequestHelper::REDIRECT_SUCCESS => route('admin.settings.index'),
			RequestHelper::REDIRECT_ERROR => route('admin.settings.index'),
			RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(), [
				'site_title'                => 'required',
				'smtp_host'                 => 'required',
				'smtp_username'             => 'required',
				'smtp_password'             => 'required',
				'smtp_port'                 => 'required|integer',
                'min_profitability'         => 'required|numeric|min:0',
			], [
				'site_title.required'                => 'Erro: o campo Titulo é obrigatório!',
				'smtp_host.required'                 => 'Erro: o campo SMTO Host é obrigatório!',
                'min_profitability.required'         => 'Erro: A "Rentabilidade Mínima" não foi informada',
                'min_profitability.numeric'          => 'Erro: O campo "Rentabilidade Mínima" aceita somente numeros',
                'min_profitability.min'              => 'Erro: "Rentabilidade Mínima" inválida',
				'smtp_password.required'             => 'Erro: o campo SMTP Senha é obrigatório!',
				'smtp_username.required'             => 'Erro: o campo SMTP Username é obrigatório!',
				'smtp_port.required'                 => 'Erro: o campo SMTP Porta é obrigatório!',
				'smtp_port.integer'                  => 'Erro: o campo SMTP Porta é inválido!',
				'pickup_days_delay.required'         => 'Erro: o campo Prazo de Retirada é obrigatório!',
			])
		];

		return RequestHelper::doRequest(function()
		{
			DB::transaction(function()
			{
				\Request::merge( [ 'id' => 1 ] );
				DBHelper::update( Setting::class, \Request::all() );
			});

		}, $options);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|Response|RequestHelper
	 */
	public function destroy($id)
	{
		//
	}

}
