<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Carbon\CarbonPeriod;
use Modules\Core\Entities\Expense;
use Modules\Core\Entities\Profitability;
use Modules\Core\Entities\Recipe;
use Modules\Core\Entities\Taxation;
use Modules\Core\Helpers\DemonstrativeHelper;
use Permission;

class DemonstrativeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
     * @param  int  $date
     *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

        $date_selected = \Illuminate\Support\Facades\Input::get('period');

        if(! $date_selected ) $date_selected = \Carbon\Carbon::now()->format('m-Y');

        $year_filter_select =  explode('-', $date_selected)[1];
        $month_filter_select = explode('-', $date_selected)[0];

        $dates = CarbonPeriod::create('2019-01-01', '1 month', \Carbon\Carbon::now()->format('Y-m-d'));

        $dates = (new \Illuminate\Database\Eloquent\Collection($dates))
            ->sortByDesc(function($col)
            {
                return $col->format("Y-m");

            })->toArray();

        foreach ($dates as $date)
            $period[$date->format("m-Y")] = $date->format("m-Y");


        $recipes = Recipe::whereYear('created_at',$year_filter_select) ->whereMonth('created_at', $month_filter_select)->get();
        $expenses = Expense::whereYear('created_at',$year_filter_select) ->whereMonth('created_at', $month_filter_select)->get();
        $taxations = Taxation::whereYear('created_at',$year_filter_select) ->whereMonth('created_at', $month_filter_select)->get();


		return view('admin::pages.demonstrative.index',
            [
                'date' => $date_selected,
                'year_filter_select' =>  $year_filter_select,
                'recipes' =>  $recipes,
                'expenses' =>  $expenses,
                'taxations' =>  $taxations,
                'net_calculated' => DemonstrativeHelper::getCalculatedNet($recipes, $expenses, $taxations),
                'part_index' => DemonstrativeHelper::getParticipationIndex($date_selected),
                'profit_distribution' => DemonstrativeHelper::getProfitDistribution($date_selected)

            ])->with('period', $period);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
