<?php

namespace Modules\Admin\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Admin\DataTables\UsersDataTable;
use Modules\Core\Entities\User;
use Modules\Core\Helpers\AppHelper;
use Modules\Core\Helpers\DBHelper;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Helpers\ResponseHelper;
use Response;
use Role;

class UserController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @param UsersDataTable $dataTable
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function index(Request $request, UsersDataTable $dataTable)
	{
		return $dataTable->render('admin::pages.user.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
        return view('admin::pages.user.create');

    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param array $extra_options
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
	 */
	public function store(Request $request, array $extra_options = [ 'rules' => [], 'messages' => [] ])
	{
		// -- set id
		$id = array_key_exists('id', $extra_options) ? $extra_options['id'] : false;

		// -- if creating
		$extra_options['rules'] = $id ? [ ] : [ 'password' => 'required|confirmed' ];
		$extra_options['messages'] = $id ? [ ] : [
			'password.required' => 'A senha é obrigatória',
			'password.confirmed' => 'A senha não coincide com a confirmação'
		];

		// -- remove all from CPF
		\Request::merge([ 'cpf' => AppHelper::onlyNumbers(\Request::get('cpf')) ]);

		$options = [
			RequestHelper::REDIRECT_SUCCESS => route('admin.users.index'),
			RequestHelper::REDIRECT_ERROR => ($id ? route('admin.users.edit', $id) : route('admin.users.create')),
			RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(), array_merge($extra_options['rules'], [
				'name' => 'required',
				'email' => 'required|unique:users,email' . (isset($id) && $id ? ",{$id}" : ''),
				'cpf' => 'required|unique:users,cpf' . (isset($id) && $id ? ",{$id}" : ''),
				'birthday' => 'required|date_format:"d/m/Y"',
				'city' => 'required',
				'state' => 'required',
				'address' => 'required',
				'number' => 'required',
				//'district' => 'required',
				'zip_code' => 'required',
				'telephone_1' => 'required',
			]), array_merge($extra_options['messages'], [
				'name.required' => 'O nome é obrigatório!',
				'email.required' => 'O e-mail é obrigatório!',
				'email.unique' => 'Desculpe: Este e-mail já está cadastrado!',
				'cpf.required' => 'O CPF é obrigatório!',
				'cpf.unique' =>  'Desculpe: Este CPF já está cadastrado!',
				'birthday.required' => 'O nascimento é obrigatório!',
				'birthday.date_format' => 'O formato da data de nascimento é inválido (dd/mm/YYYY)',

				'city.required' => 'A cidade é obrigatória!',
				'state.required' => 'O estado é obrigatório!',
				'address.required' => 'O endereço é obrigatório!',
				'number.required' => 'O número da residencia é inválido!',
				//'district.required' => 'O bairro é obrigatório!',
				'zip_code.required' => 'O CEP é obrigatório!',
				'telephone_1.required' => 'O telefone é obrigatório!',
			]))
		];

		return RequestHelper::doRequest(function() use($request, $id)
		{
			DB::transaction(function() use($request, $id)
			{
				if($id)
					$request->merge(['id' => $id]);

				// -- if changing password
				AppHelper::changePassword($request, $id);

				$user = DBHelper::update( User::class, $request->toArray() );

				// -- attach
				$user->detachRoles($user->roles);
				$user->attachRole(\Request::get('role_id'));

				// -- if profile is supplier and did not already created
				if( $user->hasRole( \Role::PROFILE_SUPPLIER ) && !$user->supplier )
					$user->supplier()->create([ 'person_type'   => \Supplier::SUPPLIER_TYPE_NATURAL_PERSON, ]);

				// -- if profile is supplier, set url
				if( $user->hasRole( \Role::PROFILE_SUPPLIER ) )
				{
					RequestHelper::$redirect_success_url = route(
						'admin.suppliers.edit',
						\Supplier::whereUserId( $user->id )->first()->id
					);
				}

			});
		}, $options);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
	 */
	public function edit($id)
	{
        if(\Auth::getUser()->hasRole(\Role::PROFILE_USER))
            return ResponseHelper::getResponse(
                route('admin.dashboard.index'), 'Erro: Não foi completar esta ação', 1);

		$user = User::find($id);
		return view('admin::pages.user.edit', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
	 */
	public function update(Request $request, $id)
	{
		return $this->store($request, [ 'id' => $id ]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|Response|RequestHelper
	 */
	public function destroy($id)
	{
		if(!User::canDestroy($id))
			return redirect()->back()->withErrors(['Desculpe, você não está autorizado a excluir este usuário.']);

		return RequestHelper::doRequest(function() use($id)
		{
			DB::transaction(function() use($id)
			{
				User::destroy( $id );
			});
		});
	}

	public function login($user_id){


        foreach(\Auth::getUser()->all_users_members as $members){
            if($members->id == $user_id)
            {
                \Auth::loginUsingId($user_id);
            }
        }

        return redirect(route('admin.dashboard.index'));
    }
}
