<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\DataTables\RecipesDataTable;
use Modules\Admin\DataTables\TaxationsDataTable;
use Modules\Admin\DataTables\ExpensesDataTable;
use Modules\Core\Entities\Expense;
use Modules\Core\Entities\Recipe;
use Modules\Core\Entities\Taxation;

class FinanceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ExpensesDataTable $expensesDataTable
     * @param RecipesDataTable $recipesDataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Request $request, RecipesDataTable $recipesDataTable, ExpensesDataTable $expensesDataTable, TaxationsDataTable $taxationsDataTable)
    {

        $is_expenses = $expensesDataTable->request()->input('datatable_id') == Expense::class;
        $is_recipes = $recipesDataTable->request()->input('datatable_id') == Recipe::class;
        $is_taxations = $taxationsDataTable->request()->input('datatable_id') == Taxation::class;

        if ($expensesDataTable->request()->ajax() && $expensesDataTable->request()->wantsJson() && $is_expenses) {
            return app()->call([$expensesDataTable, 'ajax']);
        }

        if ($recipesDataTable->request()->ajax() && $recipesDataTable->request()->wantsJson() && $is_recipes) {
            return app()->call([$recipesDataTable, 'ajax']);
        }

        if ($taxationsDataTable->request()->ajax() && $taxationsDataTable->request()->wantsJson() && $is_taxations) {
            return app()->call([$taxationsDataTable, 'ajax']);
        }

        $expensesDataTable = $expensesDataTable->getHtmlBuilder();
        $recipesDataTable = $recipesDataTable->getHtmlBuilder();
        $taxationsDataTable = $taxationsDataTable ->getHtmlBuilder();

        $recipesDataTable->scripts();

        return view('admin::pages.finance.index',
            compact('expensesDataTable', 'recipesDataTable','taxationsDataTable'));


    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
    	    $dd = 1;//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
