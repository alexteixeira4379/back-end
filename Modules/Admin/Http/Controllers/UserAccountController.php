<?php

namespace Modules\Admin\Http\Controllers;

use AppHelper;
use DB;
use DBHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Core\Helpers\RequestHelper;
use Role;
use UserAccount;
use User;

class UserAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     */
    public function index(Request $request)
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     * @param null $code_shared
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function create($code_shared = null)
	{

        return view('admin::pages.user_account.create')->with('code_shared',$code_shared );
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param array $extra_options
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|RequestHelper
     */
    public function store(Request $request, array $extra_options = [ 'rules' => [], 'messages' => [] ])
    {
        
        $birthday = $request->get('birthday');
        if($birthday)
        {
            $birthday = explode('-', $birthday);
            $birthday = implode('/', [$birthday[2],$birthday[1],$birthday[0]]);
            $request->merge(['birthday' => $birthday]);
        }

        // -- set id
        $id = array_key_exists('id', $extra_options) ? $extra_options['id'] : false;

        // -- if creating
        $extra_options['rules'] = $id ? [ ] : [
            'user.email' => 'required|unique:users,email',
            'user.password' => 'required|confirmed'
        ];
        $extra_options['messages'] = $id ? [ ] : [
            'user.email.required' => 'O e-mail é obrigatório!',
            'user.password.required' => 'A senha é obrigatória',
            'user.password.confirmed' => 'A senha não coincide com a confirmação'
        ];

        $options = [
            RequestHelper::REDIRECT_SUCCESS => route('admin.dashboard.index'),
            RequestHelper::REDIRECT_ERROR => ($id ? route('admin.accounts.edit', $id) : route('admin.accounts.create')),
            RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(), array_merge($extra_options['rules'], [

//                'met_for' => 'required',
//                'indicated_by' => 'required',

                // -- required if legal person
                'cnpj' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_JURISTIC . '|unique:user_accounts,cnpj',
                'company_name' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_JURISTIC,

                // -- required if physics person
                'full_name' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL,
                'cpf' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL . '|unique:user_accounts,cpf',
                'rg' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL . '|unique:user_accounts,rg',
                'birthday' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL . '|date_format:"d/m/Y"',
                'marital_status' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL,
                'nationality' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL,
                'profession' => 'required_if:person_type,==,' . UserAccount::PERSON_TYPE_PHYSICAL,

                // -- Contacts fields
                'zip_code' => 'required',
                'address' => 'required',
                'number' => 'required',
                'neighborhood' => 'required',
                'city' => 'required',
                'state' => 'required',
                'telephone_1' => 'required',

            ]), array_merge($extra_options['messages'], [


                'user.email.required' => 'O Email é obrigatório',
                'user.email.unique' => 'O Email Fornecido já está em uso',

                'user.required'  => 'A senha é obrigatória!',
                'user.confirmed' => 'As confirmadas não coincidem!',

                // -- legal person messages
                'company_name.required_if' => 'A razão social é obrigatória!',
                'cnpj.required_if' => 'O CNPJ é obrigatório!',
                'cnpj.unique' =>  'Desculpe: Este CNPJ já está cadastrado!',

                // -- physics person messages
                'full_name.required_if' => 'O nome é obrigatório!',
                'cpf.required_if' => 'O CPF é obrigatório!',
                'cpf.unique' =>  'Desculpe: Este CPF já está cadastrado!',
                'birthday.required_if' => 'O nascimento é obrigatório!',
                'birthday.date_format' => 'O formato da data de nascimento é inválido (dd/mm/YYYY)',
                'marital_status.required_if' => 'O estado civil é obrigatório!',
                'nationality.required_if' => 'A nacionalidade é obrigatória!',
                'profession.required_if' => 'A Profissão é obrigatória!',

                // -- Contacts messages
                'zip_code.required' => 'O CEP é obrigatório!',
                'address.required' => 'O endereço é obrigatório!',
                'number.required' => 'O número da residencia é inválido!',
                'district.neighborhood' => 'O bairro é obrigatório!',
                'city.required' => 'A cidade é obrigatória!',
                'state.required' => 'O estado é obrigatório!',
                'telephone_1.required' => 'O telefone é obrigatório!',
            ]))
        ];

        return RequestHelper::doRequest(function() use($request, $id)
        {
            DB::transaction(function() use($request, $id)
            {
                if($id)
                    $request->merge(['id' => $id]);
                else
                {
                    $user = DBHelper::update( User::class, $request->get('user') );

                    // -- attach
                    $user->attachRole( Role::whereName( Role::PROFILE_USER )->first() );

                    // -- Create Account
                    $request->merge(['user_id' => $user->id ]);
                }

                if($request->has('code_shared')){
                    $user_indicated = User::whereCodeShared($request->get('code_shared'))->get()->first();
                    $request->merge(['indicated_user_id' => $user_indicated->id]);

                }
                
                $user_account = DBHelper::update( UserAccount::class, $request->except(['code_shared']) );


            });
        }, $options);
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		//
	}
}
