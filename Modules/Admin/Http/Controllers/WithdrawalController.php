<?php

namespace Modules\Admin\Http\Controllers;

use DBHelper;
use FileHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\DataTables\WithdrawalsDataTable;
use Modules\Core\Entities\Withdrawal;
use Modules\Core\Entities\Role;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Helpers\ResponseHelper;


class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param WithdrawalsDataTable $dataTable
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request, WithdrawalsDataTable $dataTable)
    {

        return $dataTable
                ->render('admin::pages.withdrawal.index',[
                    'count_withdrawal' => $dataTable->query()->count()
                ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param array $extra_options
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     * @throws \Exception
     */
	public function store(Request $request,  array $extra_options = [ 'rules' => [], 'messages' => [] ])
	{

        $options = [
            RequestHelper::REDIRECT_SUCCESS => route('admin.withdrawals.index'),
            RequestHelper::REDIRECT_ERROR => route('admin.withdrawals.index'),
            RequestHelper::VALIDATOR => \Validator::make(\Request::toArray(),
            //Rules
            array_merge(
                [
                    'value'                     => 'required|numeric|min:0',
//                    'file_upload'               => 'required|mimes:jpeg,jpg,png,gif|max:10000',
                ],$extra_options['rules']
            ),
            //Messages
            array_merge(
                [
                    'value.required'         => 'Erro: O valor do aporte não foi informado',
                    'value.numeric'          => 'Erro: O campo para o valor aceita somente numeros',
                    'value.min'              => 'Erro: Valor não é inválido para resgate ',
//
//                    'file_upload.required'      => 'Erro: O arquivo não foi selecionado',
//                    'file_upload.file'          => 'Erro: Houve um problema no envio do arquivo',
//                    'file_upload.mimes'         => 'Erro: A imagem não está em um formato valido',
                ],$extra_options['messages']
            ))
        ];

        return RequestHelper::doRequest(function() use($request)
        {

            $value_cast = floatval($request->input('value'));

            $request->merge(['date' =>  (\Carbon\Carbon::now())->toDateString(),'user_id' => \Auth::getUser()->id]);

            if($value_cast > \Auth::getUser()->capital->balance)
                throw new \Exception('Erro: você não tem esse valor para resgate', 1);

            \DB::transaction(function() use($request)
            {

                $withdrawal = DBHelper::update(
                    Withdrawal::class,$request->only(Withdrawal::acceptedFields()));

            });


        }, $options);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}


    /**
     *  * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     * @throws \Throwable
     */
    public function update(Request $request, $id)
	{
        /** @var Withdrawal $withdrawal */
	    if(\Auth::getUser()->hasRole(Role::PROFILE_USER))
            $withdrawal = \Auth::getUser()->withdrawals()->find($id);
	    else
            $withdrawal = Withdrawal::find($id);

	    if($withdrawal){

            $request->merge(['id' => $id,'file_name' => $withdrawal->file_name]);

            $extra_options = [
                'rules' => [
                    'file_upload'               => 'mimes:jpeg,jpg,png,gif|max:10000',
                ],
                'messages' => [
                    'file_upload.file'          => 'Erro: Houve um problema no envio do arquivo',
                    'file_upload.mimes'         => 'Erro: A imagem não está em um formato valido',
                ]
            ];

            return $this->store($request,$extra_options);
        }
        else
            return ResponseHelper::getResponse(
                route('admin.withdrawals.index'), 'Erro: Não foi possivel atualizar os dados', 1);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
    public function destroy($id)
	{
        return RequestHelper::doRequest(function() use($id)
        {

            \DB::transaction(function() use($id)
            {
                if(\Auth::getUser()->hasRole(Role::PROFILE_USER)){
                    \Auth::getUser()->withdrawals()->find($id)->delete();
                }
                else{
                    Withdrawal::destroy($id);
                }
            });
        });
	}
}
