<?php

namespace Modules\Admin\Http\Controllers;

use DBHelper;
use FileHelper;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\DataTables\ContributionsDataTable;
use Contribution;
use Modules\Admin\DataTables\WithdrawalsDataTable;
use Modules\Core\Entities\Role;
use Modules\Core\Entities\Withdrawal;
use Modules\Core\Helpers\RequestHelper;
use Modules\Core\Helpers\ResponseHelper;


class TransactionApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ContributionsDataTable $contributionsDataTable
     * @param WithdrawalsDataTable $withdrawalsDataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Request $request, WithdrawalsDataTable $withdrawalsDataTable, ContributionsDataTable $contributionsDataTable)
    {

        $is_contributions = $contributionsDataTable->request()->input('datatable_id') == Contribution::class;
        $is_withdrawals = $withdrawalsDataTable->request()->input('datatable_id') == Withdrawal::class;

        $contributionsDataTable
            ->setQueryFilters(['status' => Contribution::CONTRIBUTION_PENDING])
            ->setControllerRequest(TransactionApproveController::class);

        $withdrawalsDataTable
            ->setQueryFilters(['status' => Withdrawal::WITHDRAWAL_PENDING])
            ->setControllerRequest(TransactionApproveController::class);


        if ($contributionsDataTable->request()->ajax() && $contributionsDataTable->request()->wantsJson() && $is_contributions) {
            return app()->call([$contributionsDataTable, 'ajax']);
        }

        if ($withdrawalsDataTable->request()->ajax() && $withdrawalsDataTable->request()->wantsJson() && $is_withdrawals) {
            return app()->call([$withdrawalsDataTable, 'ajax']);
        }

        $contributionsDataTable = $contributionsDataTable->getHtmlBuilder();
        $withdrawalsDataTable = $withdrawalsDataTable->getHtmlBuilder();

        $withdrawalsDataTable->scripts();

        return view('admin::pages.transaction_approve.index',
            compact('contributionsDataTable', 'withdrawalsDataTable'));


    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param array $extra_options
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
	public function store(Request $request,  array $extra_options = [ 'rules' => [], 'messages' => [] ])
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}


    /**
     *  * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     * @throws \Throwable
     */
    public function update(Request $request, $id)
	{

	}

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|RequestHelper
     */
    public function destroy($id)
	{
        return RequestHelper::doRequest(function() use($id)
        {

            \DB::transaction(function() use($id)
            {
                if(\Auth::getUser()->hasRole(Role::PROFILE_USER)){
                    \Auth::getUser()->contributions()->find($id)->delete();
                }
                else{
                    Contribution::destroy($id);
                }
            });
        });
	}
}
