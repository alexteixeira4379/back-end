var App = App || {};
App.admin = App.admin || {};

App.admin.supplier_bank_accounts = (function () {
    "use strict";

    // -- local properties
    var document_type_txt = '[name="document_type"]',
        document_number_txt = '[name="document_number"]',
        balance_txt = '.balance';


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind something
        bindDocumentType();
    }

    /**
     * Change the document type
     */
    function bindDocumentType( )
    {
        $(document_type_txt).on('change', function (ev) {
            $(document_number_txt).removeClass('mask-cpf').removeClass('mask-cnpj');

            if($(this).val() === 'cpf')
                $(document_number_txt).addClass('mask-cpf');
            else
                $(document_number_txt).addClass('mask-cnpj');
        });
    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function() {
    App.admin.supplier_bank_accounts.init(); // is initialized by other places
});