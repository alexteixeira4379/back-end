var App = App || {};
App.admin = App.admin || {};

App.admin.account = (function () {
    "use strict";

    // -- local properties

    var borderDefaultCss = '1px solid rgba(151, 149, 149, 0.35) !important',
        borderRedCss = '1px solid red !important';


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind something
        doSome();
    }

    /**
     * Change the qty values
     */
    function doSome( )
    {


    }

    function cpfValidate( cpf ) {

        cpf = cpf.replace(/[^\d]+/g,'');

        if(cpf === '') return false;

        if (cpf.length !== 11     || cpf === "00000000000" || cpf === "11111111111" || cpf === "22222222222" ||
            cpf === "33333333333" || cpf === "44444444444" || cpf === "55555555555" || cpf === "66666666666" ||
            cpf === "77777777777" || cpf === "88888888888" || cpf === "99999999999") return false;

        let add = 0;
        for (let i=0; i < 9; i ++) add += parseInt(cpf.charAt(i)) * (10 - i);

        let rev = 11 - (add % 11);
        if (rev === 10 || rev === 11) rev = 0;

        if (rev !== parseInt(cpf.charAt(9))) return false;

        add = 0;
        for (let i = 0; i < 10; i ++) add += parseInt(cpf.charAt(i)) * (11 - i);

        rev = 11 - (add % 11);
        if (rev === 10 || rev === 11) rev = 0;

        if (rev !== parseInt(cpf.charAt(10)))  return false;

        return true;
    }


    function setInputsBorder(inputs,value){

        $(inputs).each( (index,input ) => $(input).attr('style',`border: ${ value }`) );

    }

    function submitValidation(e) {

        e.preventDefault();

        let inputs = $( 'input,select,textarea' ), message = "", displayError = $('#error-validation');
        var requires_inputs = [
            'met_for','indicated_by','person_type','full_name','cpf','rg','birthday','marital_status',
            'nationality','profession','zip_code','address','number','neighborhood','city','state',
            'telephone_1','telephone_2','user[email]','user[password]',
            'user[password_confirmation]','company_name','cnpj'
            ],
            inputs_empty = [], cpf,rg,cnpj;


        displayError.hide();
        displayError.find('p').empty();

        setInputsBorder(inputs,borderDefaultCss);

        inputs.each(function (index,input)
        {

            if(requires_inputs.indexOf( input.name ) >= 0 ){

                if( input.name === 'cpf' ) cpf = input;
                if(  input.name === 'cnpj'  ) cnpj = input;
                if(  input.name === 'rg'  ) rg = input;

                if( input.value.length === 0 ) inputs_empty.push(input);
            }

        });


        if(! cpfValidate(cpf.value) && inputs_empty.indexOf('cpf') === -1 )
        {
            message += "* - O CPF não é válido<br>";
            inputs_empty.push(cpf)
        }


        if( inputs_empty.length > 0 )
        {

            message += "* - Corrija os campos em vemelho eles são inválidos";
            setInputsBorder(inputs_empty,borderRedCss);

            displayError.find('p').append(message);
            displayError.show();

            return false;


        }


        $('#app form').submit();

        return true

    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init,
        submitValidation: submitValidation
    }

}());

$(document).ready(function(){
    App.admin.users.init(); // is initialized by other places
});