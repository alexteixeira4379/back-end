var App = App || {};
App.admin = App.admin || {};

App.admin.files = (function () {
    "use strict";

    // -- local properties
    var ico_process_txt = '.ico-process',
        ico_file_log_txt = '.ico-file-log',

        // -- modals
        process_modal_txt = '#process-modal',
        file_log_modal = '#file-log-modal';


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind process modal
        bindProcessModal();

        // -- bind process modal
        bindFileLogModal();
    }

    /**
     * Bind process modal
     */
    function bindProcessModal( )
    {
        $(document).on('click', ico_process_txt, function (ev)
        {
            // -- prevent default
            ev.preventDefault();

            // -- keep loading
            $(process_modal_txt).on('shown.bs.modal', function() {
                window.location = $(ev.target).parent('a').attr('href');
            });

            // -- show modal
            $(process_modal_txt).modal({
                show: true,
                keyboard: false,
                backdrop: 'static'
            });
        });
    }

    /**
     * Bind file log modal
     */
    function bindFileLogModal()
    {
        let file_log = new Vue({
            el: file_log_modal + ' .modal-content',
            data: {
                is_loading: true,
                content: [ ],
                file_id: 0,
                error_data: [ ],
            }
        });

        $(document).on('click', ico_file_log_txt, function (ev)
        {
            // -- set vue data
            file_log.file_id = $(ev.target).data('id');

            // -- show modal
            $(file_log_modal).modal('show');

            // -- call ajax
            let url = {
                path: $(file_log_modal).data('url') + '/',
                id: $(ev.target).data('id'),
            };

            $.ajax({
                url: url['path'] + url['id'],
                method: 'GET',
                beforeSend: function () { file_log.is_loading = true; }
            }).done(function(response) { file_log.content = response.data; })
                .fail(function(response) { file_log.error_data = response.responseJSON; })
                .always(function(response) { file_log.is_loading = false; });
        });
    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function() {
    App.admin.files.init(); // is initialized by other places
});