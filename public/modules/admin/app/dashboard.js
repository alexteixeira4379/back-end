var App = App || {};
App.admin = App.admin || {};

App.admin.dashboard = (function () {
    "use strict";

    // -- local properties
    // var


    /**
     * Setup function
     */
    function setup()
    {
        // bind something
    }

    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function(){
    App.admin.dashboard.init(); // is initialized by other places
});