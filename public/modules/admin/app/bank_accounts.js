var App = App || {};
App.admin = App.admin || {};

App.admin.bank_accounts = (function () {
    "use strict";

    // -- local properties
    var document_type_txt = '[name="document_type"]',
        document_number_txt = '[name="document_number"]',
        balance_txt = '.balance',

        main_table_txt = '.bank-accounts-datatable';

    /**
     * Setup function
     */
    function setup()
    {
        // -- bind something
        bindDocumentType();

        // -- bind count to
        bindBalance();

        // sum all the value column
        CoreModule.datatables.bindValueSum(main_table_txt, 1, 0)
    }

    /**
     * Change the document type
     */
    function bindDocumentType( )
    {
        $(document_type_txt).on('change', function (ev) {
            $(document_number_txt).removeClass('mask-cpf').removeClass('mask-cnpj');

            if($(this).val() === 'cpf')
                $(document_number_txt).addClass('mask-cpf');
            else
                $(document_number_txt).addClass('mask-cnpj');
        });
    }

    /**
     * bind Balance
     */
    function bindBalance()
    {
        // if ($.isFunction('countTo'))
        // {
            $(balance_txt).countTo({
                formatter: function (value, options) {
                    return "R$ " + value.toFixed(2);
                },
            });
        // }
    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function() {
    App.admin.bank_accounts.init(); // is initialized by other places
});