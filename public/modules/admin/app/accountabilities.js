var App = App || {};
App.admin = App.admin || {};

App.admin.accountabilities = (function () {
    "use strict";

    // -- local properties
    var main_table_txt = '.accountabilities-datatable';


    /**
     * Setup function
     */
    function setup()
    {
        // sum all the value column
        CoreModule.datatables.bindValueSum(main_table_txt, 1, 0)
    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function() {
    App.admin.accountabilities.init(); // is initialized by other places
});