var App = App || {};
App.admin = App.admin || {};

App.admin.general = (function () {
    "use strict";

    // -- local properties
    var count_to_txt = '.count-to';


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind bindCountTo
        bindCountTo();
    }

    /**
     * Change the qty values
     */
    function bindCountTo( )
    {
        $(count_to_txt).countTo();
    }

    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function(){
    App.admin.general.init(); // is initialized by other places
});