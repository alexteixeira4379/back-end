var App = App || {};
App.admin = App.admin || {};

App.admin.advance_request = (function () {
    "use strict";

    // -- local properties
    var company_id_txt = '[name="company_id"]:not(.filters)',

        main_table_txt = '.advance-requests-datatable';


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind search company
        bindSearchProjects();

        // sum all the value column
        CoreModule.datatables.bindValueSum(main_table_txt, 1, 0)
    }

    /**
     * Bind search company
     */
    function bindSearchProjects( )
    {
        if($(company_id_txt).length > 0)
        {
            window.vue = new Vue({
                el: '#vue-form',
                data: {
                    is_loading: false,
                    project_id: null,
                    projects: [ ],
                },
                methods: {
                    searchProjects: function (company_id) {
                        this.is_loading = true;

                        // -- get question with all its answers
                        axios.get( $(this.$refs.company_id).data('url') + '?filters[company_id]=' + company_id)
                            .then(function (response) { vue.projects = response.data.data; })
                            .then(function () { vue.is_loading = false; });
                    },
                },
                mounted: function () {
                    $(this.$refs.company_id).change(function (ev) {
                        if( $(this).val().trim() === "" )
                            vue.projects = [ ];
                        else
                            vue.searchProjects( $(this).val() );
                    });
                    CoreModule.general.bindBootstrapSelect('select');
                },
                updated: function () {
                    $('select').selectpicker('refresh');
                },
            });
        }
    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function() {
    App.admin.advance_request.init(); // is initialized by other places
});