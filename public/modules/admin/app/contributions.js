var App = App || {};
App.admin = App.admin || {};

App.admin.files = (function () {
    "use strict";


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind process modal
        bindProcessModal();

        // -- bind process modal
        bindFileLogModal();
    }

    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function() {
});