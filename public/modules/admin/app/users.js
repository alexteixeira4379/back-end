var App = App || {};
App.admin = App.admin || {};

App.admin.users = (function () {
    "use strict";

    // -- local properties
    // var


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind something
        doSome();
    }

    /**
     * Change the qty values
     */
    function doSome( )
    {

    }


    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init
    }

}());

$(document).ready(function(){
    App.admin.users.init(); // is initialized by other places
});