var CoreModule = CoreModule || {};

CoreModule.general = (function () {
    "use strict";

    // -- local properties
    var bt_destroy_txt = '.bt-destroy',

        bt_notifiable = '#notifiable',

        // -- masks
        mask_number_txt = '.mask-number',
        mask_money_txt = '.mask-money',
        mask_date_txt = '.mask-date',
        mask_datetime_txt = '.mask-datetime',
        mask_telephone_txt = '.mask-telephone',
        mask_zip_code_txt = '.mask-zip-code',
        mask_email_txt = '.mask-email',
        mask_cpf_txt = '.mask-cpf',
        mask_cnpj_txt = '.mask-cnpj',

        thumbnials_txt = '.aniimated-thumbnials, .app-gallery',

        datepicker_txt = '.datepicker',
        datetime_picker_txt = '.datetime-picker',
        count_to_txt = '.count-to';


    /**
     * Setup function
     */
    function setup()
    {
        // -- bind bootstrap select
        bindBootstrapSelect();

        // -- bind confirm delete dialog
        bindConfirmDeleteDialog();

        // -- bind masks
        bindMasks();

        // -- bind bindCountTo
        bindCountTo();

        // -- bindGalleries
        bindGalleries();

        // -- bind datepickers
        bindDatePicker();

        // -- bind date time picker
        bindDateTimePicker();

        // -- bind tooltips
        bindToolTips();

        // -- bind editors
        bindEditor();

        // -- bind notification
        bindNotification();
    }

    /**
     * Bind bootstrap select
     *
     * @param selector {string}
     */
    function bindBootstrapSelect(selector)
    {
        selector = typeof selector === 'undefined' ? 'select:not(.except)' : selector;

        $.fn.selectpicker.defaults = {
            noneSelectedText: 'Nada selecionado',
            noneResultsText: 'Nada encontrado contendo',
            countSelectedText: 'Selecionado {0} de {1}',
            maxOptionsText: ['Limite excedido (máx. {n} {var})', 'Limite do grupo excedido (máx. {n} {var})', ['itens', 'item']],
            multipleSeparator: ', ',
            selectAllText: 'Selecionar Todos',
            deselectAllText: 'Desmarcar Todos'
        };

        if ($.fn.selectpicker) {
            $(selector).selectpicker({
                liveSearch: true,
            });
        }
    }

    /**
     * Bind confirm delete dialog
     */
    function bindConfirmDeleteDialog()
    {
        $( document ).on('click', bt_destroy_txt, function(ev)
        {
            ev.preventDefault();
            var target = $(ev.target).is('span') || $(ev.target).is('i') ? $(ev.target).parent() : $(ev.target);

            confirmDialog(function()
            {
                $.ajax({
                    url: target.attr('href'),
                    type: 'DELETE',
                    success: function( data ) {
                        if ( data.status === 'success' )
                        {
                            swal({
                                title: 'Tudo certo!',
                                text: data.message,
                                type: "success"
                            }, function(){
                                window.location.reload();
                            });
                        }
                    },
                    error: function( data ) {
                        swal("Ops", "Desculpe, ocorreu um erro ao tentar excluir o recurso: \n\n=> Motivo: " +
                            data.responseJSON.message, "error");
                        console.log(data.responseJSON);
                    }
                });
            });

            ev.preventDefault();
        });
    }

    /**
     * Bind notification
     */
    function bindNotification()
    {
        $( document ).on('click', bt_notifiable, function(ev)
        {
            ev.preventDefault();
            var target = $(ev.target).is('span') || $(ev.target).is('i') ? $(ev.target).parent() : $(ev.target);

            $.ajax({
                url: target.attr('href'),
                type: 'POST',
                success: function( data ) {},
                error: function( data ) {
                    console.log(data.responseJSON);
                }
            });

            ev.preventDefault();
        });
    }


    /**
     * Confirm delete modal
     */
    function confirmDialog(callback, title, text, type, close_on_confirm)
    {
        callback = callback ? callback : function() { };
        close_on_confirm = close_on_confirm ? close_on_confirm : true;

        title = title ? title : "Você tem certeza que deseja excluir este registro?";
        text = text ? text : "Esta ação não pode ser desfeita.";
        type = type ? type : "warning";

        swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Não",
            confirmButtonText: "Sim",
            closeOnConfirm: close_on_confirm
        }, callback);
    }

    /**
     * Bind masks
     */
    function bindMasks()
    {
        $( mask_number_txt ).mask('000000000000000');
        $( mask_money_txt ).mask('000000000000000.00', {reverse: true});
        $( mask_date_txt ).mask('00/00/0000');
        $( mask_datetime_txt ).mask('00/00/0000 00:00:00');
        $( mask_zip_code_txt ).mask('00000-000');
        $( mask_email_txt ).inputmask({ alias: "email" });

        $(mask_cpf_txt).mask('000.000.000-00', {reverse: true});
        $(mask_cnpj_txt).mask('00.000.000/0000-00', {reverse: true});

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $( mask_telephone_txt ).mask(SPMaskBehavior, spOptions);
    }

    /**
     * Change the qty values
     */
    function bindCountTo( )
    {
        if ($.isFunction('countTo'))
            $(count_to_txt).countTo();
    }

    /**
     * Bind galleries
     */
    function bindGalleries()
    {
        $(thumbnials_txt).lightGallery({
            thumbnail: true,
            selector: 'a'
        });
    }

    /**
     * Bind date picker
     */
    function bindDatePicker()
    {
        $( datepicker_txt ).bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            clearButton: true,
            nowButton: true,
            switchOnClick: true,
            weekStart: 1,
            time: false,
            cancelText: 'Cancelar',
            okText: 'Escolher',
            clearText: 'Limpar',
            nowText: 'Hoje'
        });
    }

    /**
     * Bind date picker
     */
    function bindDateTimePicker()
    {
        $( datetime_picker_txt ).bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY HH:mm:ss',
            clearButton: true,
            nowButton: true,
            switchOnClick: true,
            weekStart: 1,
            time: true,
            cancelText: 'Cancelar',
            okText: 'Escolher',
            clearText: 'Limpar',
            nowText: 'Hoje'
        });
    }


    /**
     * Bind tooltips
     */
    function bindToolTips()
    {
        $('[data-toggle="tooltip"]').tooltip();
    }

    /**
     * Bind editors
     */
    function bindEditor()
    {
        if (typeof tinymce !== 'undefined')
        {
            var toolbar1 = '',
                toolbar2 = '';

            // -- toolbar 1
            toolbar1 += 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify |';
            toolbar1 += 'bullist numlist outdent indent | link image media';

            // -- toolbar 2
            toolbar2 += 'print preview | forecolor backcolor emoticons';

            // -- valid style
            var extended_valid_elements = [
                'img[class|src|border=0|alt|title|width|height|align|name]',
                'span[style]',
                'p[style]',
                'iframe[src|frameborder|style|scrolling|class|width|height|name|align]'
            ];

            //TinyMCE
            tinymce.init({
                selector: "textarea.editor",
                theme: "modern",
                height: 300,
                valid_elements: "p,br,b,i,strong,em,video,span",
                extended_valid_elements : extended_valid_elements.join(','),
                valid_styles: {
                    '*': 'color,border,font-size,background-color',
                    'div': 'width,height',
                },
                valid_classes: {
                    '*': 'form-control', // Global classes
                    //'a': 'class4 class5' // Link specific classes
                },
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                content_style: ".mce-content-body {font-size:14px;font-family:Roboto,Arial,Tahoma,sans-serif;}",
                toolbar1: toolbar1,
                toolbar2: toolbar2,
                image_advtab: true,
                language: 'pt_BR',
                media_live_embeds: true, // allow preview
            });
            tinymce.suffix = ".min";
            // tinyMCE.baseURL = '../../plugins/tinymce';
        }
    }

    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init,
        bindToolTips: bindToolTips,
        bindBootstrapSelect: bindBootstrapSelect,
        bindGalleries: bindGalleries,
    }

}());

$(document).ready(function(){
    CoreModule.general.init(); // is initialized by other places
});