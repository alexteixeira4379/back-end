var CoreModule = CoreModule || {};

CoreModule.datatables = (function () {
    "use strict";

    // -- local properties
    var options = { },
        div_datatables_buttons_place_txt = '.datatables-buttons-place',
        div_limit_place_txt = '.div-limit-place',
        div_filter_place_txt = '.div-filter-place',
        div_dt_buttons_txt = '.dt-buttons',

        // -- datatable
        callback = null,
        column_switch_txt = '.column-switch',
        datatable_columns = '.datatable-columns',
        datatables = null;

    /**
     * Setup function
     */
    function setup()
    {
        // -- set default options
        setDefaultOptions();
    }

    /**
     * Callback when a ROW is created (TR)
     *
     * @param row
     * @param data
     * @param index
     */
    function rowCallback( row, data, index )
    {
        //
    }

    /**
     * Callback when a CELL is created (TD)
     *
     * @param td
     * @param cellData
     * @param rowData
     * @param row
     * @param col
     */
    function createdCell(td, cellData, rowData, row, col)
    {
        //
    }

    /**
     * Callback fired when DataTables has been fully initialised
     *
     * @param settings
     * @param json
     */
    function initCallback(settings, json)
    {
        // -- set instance
        datatables = this;

        if(callback)
            callback( getDataTableInstance() );

        // -- switch between columns
        switchDataTablesColumns();

        // -- bind filter actions
        bindFilters();

        // -- move filter to the rigth place
        // $(div_filter_place_txt).html($(div_filter_txt));
        // $(div_filter_txt).show();

        // -- remove datatable footer
        // $('tfoot').remove();
    }

    /**
     * Callback every time datatable is draw
     *
     * @param datatables_settings
     */
    function drawCallback(datatables_settings)
    {
        // -- mount XEditable settings
        //mountXEditable(datatables_settings);
        CoreModule.general.bindGalleries();

        // -- bind tooltips
        CoreModule.general.bindToolTips();
    }

    /**
     * Mount XEditable settings
     *
     * @param datatables_settings
     */
    function mountXEditable(datatables_settings)
    {
        // -- defaults
        //$.fn.editable.defaults.url = '';

        /**
         * Mount params to be sent together defaults in XEditable
         * (Params already contain `name`, `value` and `pk`)
         *
         * @param params
         * @returns {{}}
         */
        var params = function(params)
        {
            var data = {};

            data['id'] = params.pk;
            data[params.name] = params.value;
            data['_token'] = $(this).parents('.datatable').data('token');
            data['_method'] = 'PUT'; // -- for laravel
            data['_response_type'] = 'http'; // -- json OR http
            data['_is_xeditable'] = true; // -- json OR http

            return data;
        };

        $('.xeditable').editable({
            params: params
        });
    }

    /**
     * Set default options
     */
    function setDefaultOptions()
    {

    }

    /**
     * Get all current filter values
     *
     * @return Object
     */
    function getFilters()
    {
        var filters = { };

        $.each($('select.filters, input.filters'), function (i, el)
        {
            el = $(el);
            var el_name = el.attr('name');

            // -- key + value
            filters[el_name] = (el.is('select') ? el.find('option:selected').val() : el.val() );
        });

        return filters;
    }

    /**
     * Bind filters
     */
    function bindFilters()
    {
        $(document).on('keypress', 'input[type=text].filters', function (ev)
        {
            if(ev.keyCode == 13)
            {
                scrollDownDataTable();

                if(getDataTableInstance())
                    getDataTableInstance().draw();
            }
        });

        $(document).on('change', 'select.filters, input[type=checkbox].filters, input[type=radio].filters', function (ev)
        {
            scrollDownDataTable();

            if(getDataTableInstance())
                getDataTableInstance().draw();
        });
    }

    /**
     * Scroll down datatable
     */
    function scrollDownDataTable()
    {
        document.querySelector('.datatable').scrollIntoView({
            behavior: 'smooth'
        });
    }

    /**
     * Switch betweeen columns
     */
    function switchDataTablesColumns()
    {
        if(getDataTableInstance() && $(datatable_columns).length > 0)
        {
            getDataTableInstance().columns().every( function ()
            {
                var column = this,
                    header = $( column.header() ),
                    is_column_visible = column.visible(),
                    icon = '<i class="material-icons" style="visibility: '+(is_column_visible ? 'visible' : 'hidden')+'">check_circle</i>';

                $(datatable_columns).append(
                    '<li><a href="javascript:void(0);" class="column-switch" data-index="'+column.index()+'">'+header.text()+' '+icon+'</a></li>'
                );
            });

            // -- on click on table item on the list
            $(column_switch_txt).on('click', function (ev)
            {
                var index = $(this).data('index'),
                    column = getDataTableInstance().column( index ),
                    is_column_visible = column.visible();

                column.visible( !is_column_visible );
                $(this).find('i').css('visibility', (!is_column_visible ? 'visible' : 'hidden') );
            });
        }
    }

    /**
     * Get DataTable instance
     *
     * @returns {boolean|DataTable}
     */
    function getDataTableInstance()
    {
        if(!datatables)
            return false;

        return datatables.api();
    }

    /**
     *
     * @param _callback
     */
    function setInitCallback(_callback)
    {
        callback = _callback;
    }

    /**
     * Sum all the value column
     *
     * @param main_table_selector
     * @param column_total
     * @param column_label
     */
    function bindValueSum( main_table_selector, column_total, column_label )
    {
        jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
            return this.flatten().reduce( function ( a, b ) {
                if ( typeof a === 'string' ) {
                    a = a.replace(/[^\d.-]/g, '') * 1;
                }
                if ( typeof b === 'string' ) {
                    b = b.replace(/[^\d.-]/g, '').replace(/--/g, '') * 1;
                }

                return a + b;
            }, 0 );
        } );

        $(function() {
            var table = $( main_table_selector ).DataTable();

            $( main_table_selector ).on( 'draw.dt', function () {
                var tablesum = table.column(1).data().sum();

                var _class = tablesum >= 0 ? 'bg-green' : 'bg-red';
                var label = $('<span class="badge '+_class+'" style="font-size: 15px;">R$ '+tablesum.toFixed(2)+'</span>');

                if(typeof(column_label) !== 'undefined')
                    $( table.column(column_label).footer() ).html( 'Total' );

                $( table.column(column_total).footer() ).html( label );
            } );
        });
    }

    /**
     * Constructor
     */
    function init() {
        setup();
    }

    // -- set public methods
    return {
        init: init,
        options: options,
        initCallback: initCallback,
        drawCallback: drawCallback,
        rowCallback: rowCallback,
        createdCell: createdCell,
        getFilters: getFilters,
        getDataTableInstance: getDataTableInstance,
        setInitCallback: setInitCallback,
        scrollDownDataTable: scrollDownDataTable,
        bindValueSum: bindValueSum,
    }

}());

$(document).ready(function(){
    //CoreModule.datatables.init(); // is initialized by other places
});