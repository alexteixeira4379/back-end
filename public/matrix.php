<?php

function minhaMatriz($count)
{
	$matrix = [ ];

	for ($i=0; $i < $count; $i++)
	{
		$line = [ ];

		for ($j=0; $j < $count; $j++)
			$line[] = $j == 0 || $i == $j ? 1 : 0;

		$matrix[] = $line;
	}

	foreach ( $matrix as $columns )
		echo implode(" ", $columns) . "\n<br>";

}

minhaMatriz(5);