<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models
    |--------------------------------------------------------------------------
    */

    'models' => [
        'message' => \Modules\Core\Entities\Message::class, //default -> Gerardojbaez\Messenger\Models\Message::class,
        'thread' => \Modules\Core\Entities\MessageThread::class, //default -> Gerardojbaez\Messenger\Models\MessageThread::class,
        'participant' => \Modules\Core\Entities\MessageThreadParticipant::class // default -> Gerardojbaez\Messenger\Models\MessageThreadParticipant::class,
    ],

];
